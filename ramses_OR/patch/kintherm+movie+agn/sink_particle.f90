!################################################################
!################################################################
!################################################################
!################################################################
subroutine create_sink
  use amr_commons
  use pm_commons
  use hydro_commons
  implicit none
#ifndef WITHOUTMPI
  include 'mpif.h'
#endif
  !----------------------------------------------------------------------------
  ! Description: This subroutine create sink particle in cells where a density 
  ! threshold has been crossed. It also removes from the gas the corresponding 
  ! particle mass. On exit, all fluid variables in the cell are modified.
  ! This routine is called only once per coarse step by routine amr_step.
  ! Romain Teyssier, October 7th, 2007
  !----------------------------------------------------------------------------
  ! local constants                                                                
  integer::ilevel,ivar,info,icpu,igrid,npartbound,isink
  real(dp)::scale_nH,scale_T2,scale_l,scale_d,scale_t,scale_v,scale_m

  if(verbose)write(*,*)' Entering create_sink', myid

  ! Conversion factor from user units to cgs units
  call units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)
  scale_m=scale_d*scale_l**3

  ! Set level_agn.  This really only needs to be done at the 
  ! beginning of the simulation.
  if(level_agn == 0) level_agn = nlevelmax

  ! Remove particles to finer levels
  do ilevel=levelmin,nlevelmax
     call kill_tree_fine(ilevel)
     call virtual_tree_fine(ilevel)
  end do

  if (clumpfind .eqv. .false.)then
     ! Create new sink particles
     ! and gather particle from the grid

     call make_sink(nlevelmax)
     do ilevel=nlevelmax-1,1,-1
        if(ilevel>=levelmin)call make_sink(ilevel)
        call merge_tree_fine(ilevel)
     end do
  end if

  ! Remove particle clouds around old sinks
  call kill_cloud(1)

!!!JMG
  if(reposition_sink) then
     call move_sink_to_center(1)
  endif
!!!JMG

  if (clumpfind)then
     call remove_parts_brute_force

     ! DO NOT USE FLAG2 BETWEEN CLUMP_FINDER AND MAKE_SINK
     call clump_finder(.false.)
     call make_sink_from_clump(nlevelmax)
     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 
     call create_part_from_sink
  else
  ! Merge sink using FOF                                                           
     call merge_sink(1)
  end if

  ! Create new particle clouds
  call create_cloud(1)

  ! Scatter particle to the grid
  do ilevel=1,nlevelmax
     call make_tree_fine(ilevel)
     call kill_tree_fine(ilevel)
     call virtual_tree_fine(ilevel)
  end do

  ! Update hydro quantities for split cells
  if(hydro)then
     do ilevel=nlevelmax,levelmin,-1
        call upload_fine(ilevel)
        do ivar=1,nvar
           call make_virtual_fine_dp(uold(1,ivar),ilevel)
        end do
        ! Update boundaries 
        if(simple_boundary)call make_boundary_hydro(ilevel)
     end do
  end if

  ! Compute Bondi parameters and gather particle
  do ilevel=nlevelmax,levelmin,-1
     if(bondi)call bondi_hoyle(ilevel)
     call merge_tree_fine(ilevel)
  end do

  if(reset_accreted_mass_on_startup > 0) then
     write(*,*) "------Resetting accreted BH mass for startup...", delta_mass(1)
     do isink=1,nsink
        delta_mass(isink) = 0.0
     enddo
     reset_accreted_mass_on_startup = reset_accreted_mass_on_startup - 1
  endif

  if(debug2) WRITE(*,*) myid, "Computing accretion rates..."
  call compute_accretion_rate(levelmin)

  if(do_measure_gas_mass) call measure_gas_mass
  if(do_measure_part_mass) call measure_particle_mass(0)

  if(agn)call agn_feedback

!!! JMG
  if(zoom_sink > 0) then
     do ilevel = 1, nlevelmax
        ! only for levels with a zoom refinement
        if(r_refine(ilevel) > 0.0) then
           x_refine(ilevel) = xsink(zoom_sink, 1)
           y_refine(ilevel) = xsink(zoom_sink, 2)
           z_refine(ilevel) = xsink(zoom_sink, 3)
        endif
     enddo
  endif
!!!  call measure_outflow

end subroutine create_sink
!################################################################
!################################################################
!################################################################
!################################################################
!################################################################
!################################################################
!################################################################
!################################################################
subroutine create_part_from_sink
  use amr_commons
  use pm_commons
  use hydro_commons
  use cooling_module, ONLY: XH=>X, rhoc, mH 
  implicit none
#ifndef WITHOUTMPI
  include 'mpif.h'
#endif
  !----------------------------------------------------------------------
  ! Description: This subroutine create true RAMSES particles from the list
  ! of sink particles global arrays and store them at level 1.
  !----------------------------------------------------------------------
  ! local constants
  real(dp),dimension(1:twotondim,1:3)::xc
  integer ::ncache,nnew,ivar,ngrid,icpu,index_sink,index_sink_tot,icloud
  integer ::igrid,ix,iy,iz,ind,i,j,n,iskip,isink,inew,nx_loc
  integer ::ii,jj,kk,ind_cloud,ncloud,indp
  integer ::ntot,ntot_all,info
  logical ::ok_free

  real(dp)::scale_nH,scale_T2,scale_l,scale_d,scale_t,scale_v,scale_m
  real(dp)::d,x,y,z,u,v,w,e,temp,zg,factG
  real(dp)::dxx,dyy,dzz,drr
  real(dp)::rsink_max2
  real(dp)::velc,uc,vc,wc,l_jeans,d_jeans,d_thres,d_sink
  real(dp)::birth_epoch,xx,yy,zz,rr
  real(dp),dimension(1:3)::skip_loc
  real(dp)::dx,dx_loc,scale,vol_loc,dx_min,vol_min
  real(dp)::bx1,bx2,by1,by2,bz1,bz2

  real(dp),dimension(1:nvector,1:ndim),save::xs

  integer ,dimension(1:nvector),save::ind_grid,ind_cell,cc
  integer ,dimension(1:nvector),save::ind_grid_new,ind_cell_new,ind_part
  integer ,dimension(1:nvector),save::ind_part_cloud,ind_grid_cloud
  logical ,dimension(1:nvector),save::ok,ok_new=.true.,ok_true=.true.
  integer ,dimension(1:ncpu)::ntot_sink_cpu,ntot_sink_all
 
 
  if(numbtot(1,1)==0) return
  if(.not. hydro)return
  if(ndim.ne.3)return

  if(verbose)write(*,*)' Entering create_part_from_sink'

#if NDIM==3

  ntot=0
  ! Loop over sinks (why not use vector sweep here?)
  do isink=1,nsink
     xs(1,1:ndim)=xsink(isink,1:ndim)
     call cmp_cpumap(xs,cc,1)
     if(cc(1).eq.myid)ntot=ntot+1
  end do

  !---------------------------------
  ! Check for free particle memory
  !--------------------------------
  ok_free=(numbp_free-ntot)>=0
#ifndef WITHOUTMPI
  call MPI_ALLREDUCE(numbp_free,numbp_free_tot,1,MPI_INTEGER,MPI_MIN,MPI_COMM_WORLD,info)
#endif
#ifdef WITHOUTMPI
  numbp_free_tot=numbp_free
#endif
   if(.not. ok_free)then
      write(*,*)'No more free memory for particles'
      write(*,*)'New sink particles',ntot
      write(*,*)'Increase npartmax'
#ifndef WITHOUTMPI
      call MPI_ABORT(MPI_COMM_WORLD,1,info)
#endif
#ifdef WITHOUTMPI
      stop
#endif
   end if
   
  !---------------------------------
  ! Compute global sink statistics
  !---------------------------------
#ifndef WITHOUTMPI
  call MPI_ALLREDUCE(ntot,ntot_all,1,MPI_INTEGER,MPI_SUM,MPI_COMM_WORLD,info)
#endif
#ifdef WITHOUTMPI
  ntot_all=ntot
#endif
#ifndef WITHOUTMPI
  ntot_sink_cpu=0; ntot_sink_all=0
  ntot_sink_cpu(myid)=ntot
  call MPI_ALLREDUCE(ntot_sink_cpu,ntot_sink_all,ncpu,MPI_INTEGER,MPI_SUM,MPI_COMM_WORLD,info)
  ntot_sink_cpu(1)=ntot_sink_all(1)
  do icpu=2,ncpu
     ntot_sink_cpu(icpu)=ntot_sink_cpu(icpu-1)+ntot_sink_all(icpu)
  end do
#endif
  if(myid==1)then
     if(ntot_all.gt.0)then
        write(*,'(" Particle to be created= ",I6," Tot =",I8)')&
             & ntot_all,nsink
     endif
  end if

  ! Starting identity number
  if(myid==1)then
     index_sink=nsink-ntot_all
  else
     index_sink=nsink-ntot_all+ntot_sink_cpu(myid-1)
  end if

  ! Level 1 linked list
  do icpu=1,ncpu
     if(numbl(icpu,1)>0)then
        ind_grid(1)=headl(icpu,1)
     endif
  end do

  !sort the sink according to mass
  if(nsink>0)then
     do i=1,nsink
        xmsink(i)=msink(i)
     end do
     call quick_sort(xmsink(1),idsink_sort(1),nsink)
  endif

  ! Loop over sinks
  do i=nsink,1,-1
     isink=idsink_sort(i)
     xs(1,1:ndim)=xsink(isink,1:ndim)
     call cmp_cpumap(xs,cc,1)

     ! Create new particles
     if(cc(1).eq.myid)then
           index_sink=index_sink+1
           ! Update linked list
           call remove_free(ind_part,1)
           call add_list(ind_part,ind_grid,ok_true,1)
           indp=ind_part(1)
           tp(indp)=tsink(isink)     ! Birth epoch
           mp(indp)=msink(isink)     ! Mass
           if(msink_dynamical > 0.0) mp(indp) = msink_dynamical
           if (new_born_all(isink)==1)then
              levelp(indp)=-1
           else
              levelp(indp)=nlevelmax    ! Level WARNING THIS SHOULD BE FIXED USING lsink
           endif
           idp(indp)=-index_sink     ! Identity
           xp(indp,1)=xsink(isink,1) ! Position
           xp(indp,2)=xsink(isink,2)
           xp(indp,3)=xsink(isink,3)
           vp(indp,1)=vsink(isink,1) ! Velocity
           vp(indp,2)=vsink(isink,2)
           vp(indp,3)=vsink(isink,3)
        endif

  end do

#endif

end subroutine create_part_from_sink
!################################################################
!################################################################
!################################################################
!################################################################
subroutine make_sink(ilevel)
  use amr_commons
  use pm_commons
  use hydro_commons
  use cooling_module, ONLY: XH=>X, rhoc, mH 
  implicit none
#ifndef WITHOUTMPI
  include 'mpif.h'
#endif
  integer::ilevel
  !----------------------------------------------------------------------
  ! Description: This subroutine create sink particle in cells where some
  ! density threshold is crossed. It also removes from the gas the
  ! corresponding particle mass. On exit, all fluid variables in the cell
  ! are modified. This is done only in leaf cells.
  ! Romain Teyssier, October 7th, 2007
  !----------------------------------------------------------------------
  ! local constants
  real(dp),dimension(1:twotondim,1:3)::xc
  integer ::ncache,nnew,ivar,ngrid,icpu,index_sink,index_sink_tot,icloud
  integer ::igrid,ix,iy,iz,ind,i,j,n,iskip,isink,inew,nx_loc
  integer ::ii,jj,kk,ind_cloud,ncloud
  integer ::ntot,ntot_all,info
  logical ::ok_free
  real(dp)::scale_nH,scale_T2,scale_l,scale_d,scale_t,scale_v,scale_m
  real(dp)::d,x,y,z,u,v,w,e,temp,zg,factG
  real(dp)::dxx,dyy,dzz,drr
  real(dp)::msink_max2,rsink_max2
  real(dp)::velc,uc,vc,wc,l_jeans,d_jeans,d_thres,d_sink
  real(dp)::birth_epoch,xx,yy,zz,rr
  real(dp),dimension(1:3)::skip_loc
  real(dp)::dx,dx_loc,scale,vol_loc,dx_min,vol_min
  real(dp)::bx1,bx2,by1,by2,bz1,bz2

  integer ,dimension(1:nvector),save::ind_grid,ind_cell
  integer ,dimension(1:nvector),save::ind_grid_new,ind_cell_new,ind_part
  integer ,dimension(1:nvector),save::ind_part_cloud,ind_grid_cloud
  logical ,dimension(1:nvector),save::ok,ok_new=.true.,ok_true=.true.
  integer ,dimension(1:ncpu)::ntot_sink_cpu,ntot_sink_all
  
  if(numbtot(1,ilevel)==0) return
  if(.not. hydro)return
  if(ndim.ne.3)return

  if(verbose)write(*,*)' Entering make_sink for level ',ilevel

  ! Conversion factor from user units to cgs units                              
  call units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)
  scale_m=scale_d*scale_l**3

  ! Minimum radius to create a new sink from any other
  rsink_max2=(rsink_max*3.08d21/scale_l)**2

  ! Maximum value for the initial sink mass
  msink_max2=msink_max*2d33/scale_m
  
  ! Gravitational constant
  factG=1d0
  if(cosmo)factG=3d0/8d0/3.1415926*omega_m*aexp

  ! Density threshold for sink particle creation
  d_sink=n_sink/scale_nH

  ! Mesh spacing in that level
  dx=0.5D0**ilevel 
  nx_loc=(icoarse_max-icoarse_min+1)
  skip_loc=(/0.0d0,0.0d0,0.0d0/)
  if(ndim>0)skip_loc(1)=dble(icoarse_min)
  if(ndim>1)skip_loc(2)=dble(jcoarse_min)
  if(ndim>2)skip_loc(3)=dble(kcoarse_min)
  scale=boxlen/dble(nx_loc)
  dx_loc=dx*scale
  vol_loc=dx_loc**ndim
!  dx_min=(0.5D0**nlevelmax)*scale
  dx_min=(0.5D0**level_agn)*scale
  vol_min=dx_min**ndim

  ! Birth epoch
  birth_epoch=t

  ! Cells center position relative to grid center position
  do ind=1,twotondim  
     iz=(ind-1)/4
     iy=(ind-1-4*iz)/2
     ix=(ind-1-2*iy-4*iz)
     xc(ind,1)=(dble(ix)-0.5D0)*dx
     xc(ind,2)=(dble(iy)-0.5D0)*dx
     xc(ind,3)=(dble(iz)-0.5D0)*dx
  end do

  xx=0.0; yy=0.0;zz=0.0
  ncloud=0
  do kk=-2*ir_cloud,2*ir_cloud
     zz=dble(kk)*0.5
     do jj=-2*ir_cloud,2*ir_cloud
        yy=dble(jj)*0.5
        do ii=-2*ir_cloud,2*ir_cloud
           xx=dble(ii)*0.5
           rr=sqrt(xx*xx+yy*yy+zz*zz)
           if(rr<=dble(ir_cloud))ncloud=ncloud+1
        end do
     end do
  end do
  ncloud_sink=ncloud

  ! Set new sink variables to zero
  msink_new=0d0; tsink_new=0d0; delta_mass_new=0d0; xsink_new=0d0; vsink_new=0d0; oksink_new=0d0; idsink_new=0

#if NDIM==3

  !------------------------------------------------
  ! Convert hydro variables to primitive variables
  !------------------------------------------------
  ncache=active(ilevel)%ngrid
  do igrid=1,ncache,nvector
     ngrid=MIN(nvector,ncache-igrid+1)
     do i=1,ngrid
        ind_grid(i)=active(ilevel)%igrid(igrid+i-1)
     end do
     do ind=1,twotondim  
        iskip=ncoarse+(ind-1)*ngridmax
        do i=1,ngrid
           ind_cell(i)=iskip+ind_grid(i)
        end do
        do i=1,ngrid
           d=uold(ind_cell(i),1)
           u=uold(ind_cell(i),2)/d
           v=uold(ind_cell(i),3)/d
           w=uold(ind_cell(i),4)/d
           e=uold(ind_cell(i),5)/d
#ifdef SOLVERmhd
           bx1=uold(ind_cell(i),6)
           by1=uold(ind_cell(i),7)
           bz1=uold(ind_cell(i),8)
           bx2=uold(ind_cell(i),nvar+1)
           by2=uold(ind_cell(i),nvar+2)
           bz2=uold(ind_cell(i),nvar+3)
           e=e-0.125d0*((bx1+bx2)**2+(by1+by2)**2+(bz1+bz2)**2)/d
#endif
           e=e-0.5d0*(u**2+v**2+w**2)
           uold(ind_cell(i),1)=d
           uold(ind_cell(i),2)=u
           uold(ind_cell(i),3)=v
           uold(ind_cell(i),4)=w
           uold(ind_cell(i),5)=e
        end do
        do ivar=imetal,nvar
           do i=1,ngrid
              d=uold(ind_cell(i),1)
              w=uold(ind_cell(i),ivar)/d
              uold(ind_cell(i),ivar)=w
           end do
        enddo
     end do
  end do

  !-------------------------------
  ! Determine SMBH formation sites
  !-------------------------------
!! JMG  if(smbh)call quenching(ilevel)
  if(check_smbh_formation_sites) call quenching(ilevel)

  !----------------------------
  ! Compute number of new sinks
  !----------------------------
  ntot=0
  ! Loop over grids
  ncache=active(ilevel)%ngrid
  do igrid=1,ncache,nvector
     ngrid=MIN(nvector,ncache-igrid+1)
     do i=1,ngrid
        ind_grid(i)=active(ilevel)%igrid(igrid+i-1)
     end do
     ! Density threshold crossed ---> logical array ok(i)
     do ind=1,twotondim
        iskip=ncoarse+(ind-1)*ngridmax
        do i=1,ngrid
           ind_cell(i)=iskip+ind_grid(i)
        end do

        ! Flag leaf cells
        do i=1,ngrid
           ok(i)=son(ind_cell(i))==0
        end do

        ! Create new sink if the gas density exceed some threshold
        do i=1,ngrid
           d=uold(ind_cell(i),1)

           ! User defined density threshold
           d_thres=d_sink

           ! Jeans length related density threshold
           if(d_thres<0.0)then
              temp=max(uold(ind_cell(i),5)*(gamma-1.0),smallc**2)
              d_jeans=temp*3.1415926/(4.0*dx_loc)**2/factG
              d_thres=d_jeans
           endif

           ! Density criterion
           if(d < d_thres)ok(i)=.false.

           ! Quenching criterion
           ! JMG          if(smbh.and.flag2(ind_cell(i))==1)ok(i)=.false.
           if(check_smbh_formation_sites.and.flag2(ind_cell(i))==1)ok(i)=.false.
           
           ! Geometrical criterion
           if(ivar_refine>0)then
              d=uold(ind_cell(i),ivar_refine)
              if(d<=var_cut_refine)ok(i)=.false.
           endif

           ! Proximity criterion
           if(ok(i).and.rsink_max>0d0)then
              x=(xg(ind_grid(i),1)+xc(ind,1)-skip_loc(1))*scale
              y=(xg(ind_grid(i),2)+xc(ind,2)-skip_loc(2))*scale
              z=(xg(ind_grid(i),3)+xc(ind,3)-skip_loc(3))*scale
              do isink=1,nsink
                 dxx=x-xsink(isink,1)
                 if(dxx> 0.5*scale)then
                    dxx=dxx-scale
                 endif
                 if(dxx<-0.5*scale)then
                    dxx=dxx+scale
                 endif
                 dyy=y-xsink(isink,2)
                 if(dyy> 0.5*scale)then
                    dyy=dyy-scale
                 endif
                 if(dyy<-0.5*scale)then
                    dyy=dyy+scale
                 endif
                 dzz=z-xsink(isink,3)
                 if(dzz> 0.5*scale)then
                    dzz=dzz-scale
                 endif
                 if(dzz<-0.5*scale)then
                    dzz=dzz+scale
                 endif
                 drr=dxx*dxx+dyy*dyy+dzz*dzz
                 if(drr.le.rsink_max2)ok(i)=.false.
              enddo
           endif

        end do

        ! Calculate number of new sinks in each cell
        do i=1,ngrid
           flag2(ind_cell(i))=0
           if(ok(i))then
              ntot=ntot+1
              flag2(ind_cell(i))=1
           endif
        enddo
     end do
  end do

  !---------------------------------
  ! Check for free particle memory
  !--------------------------------
  ok_free=(numbp_free-ntot)>=0
#ifndef WITHOUTMPI
  call MPI_ALLREDUCE(numbp_free,numbp_free_tot,1,MPI_INTEGER,MPI_MIN,MPI_COMM_WORLD,info)
#endif
#ifdef WITHOUTMPI
  numbp_free_tot=numbp_free
#endif
  if(.not. ok_free)then
     write(*,*)'No more free memory for particles'
     write(*,*)'New sink particles',ntot
     write(*,*)'Increase npartmax'
#ifndef WITHOUTMPI
    call MPI_ABORT(MPI_COMM_WORLD,1,info)
#else
    stop
#endif
  end if
  
  !---------------------------------
  ! Compute global sink statistics
  !---------------------------------
#ifndef WITHOUTMPI
  call MPI_ALLREDUCE(ntot,ntot_all,1,MPI_INTEGER,MPI_SUM,MPI_COMM_WORLD,info)
#endif
#ifdef WITHOUTMPI
  ntot_all=ntot
#endif
#ifndef WITHOUTMPI
  ntot_sink_cpu=0; ntot_sink_all=0
  ntot_sink_cpu(myid)=ntot
  call MPI_ALLREDUCE(ntot_sink_cpu,ntot_sink_all,ncpu,MPI_INTEGER,MPI_SUM,MPI_COMM_WORLD,info)
  ntot_sink_cpu(1)=ntot_sink_all(1)
  do icpu=2,ncpu
     ntot_sink_cpu(icpu)=ntot_sink_cpu(icpu-1)+ntot_sink_all(icpu)
  end do
#endif
  nsink=nsink+ntot_all  
  nindsink=nindsink+ntot_all
  if(myid==1)then
     if(ntot_all.gt.0)then
        write(*,'(" Level = ",I6," New sink before merging= ",I6," Tot =",I8)')&
             & ilevel,ntot_all,nsink
     endif
  end if

  !------------------------------
  ! Create new sink particles
  !------------------------------
  ! Starting identity number
  if(myid==1)then
     index_sink=nsink-ntot_all
     index_sink_tot=nindsink-ntot_all
  else
     index_sink=nsink-ntot_all+ntot_sink_cpu(myid-1)
     index_sink_tot=nindsink-ntot_all+ntot_sink_cpu(myid-1)
  end if

  !-------- Comment by JMG --------!
  ! Now we have the following counter variables:
  ! nsink -- total number of sinks, adding new ones to old ones 
  ! ntot -- # of new sinks to create in cells on this proc
  ! ntot_all -- # of new sinks to create (summed over all procs)
  ! ntot_sink_cpu -- cumulative count of the number of new sinks over processors
  !    i.e. ntot_sink_cpu(3) = nsink3 + nsink2 + nsink1
  ! ntot_sink_all -- array containing the the number of new sinks on each proc
  ! nindsink -- global variable.  I think it equals the a) the highest index
  !    value of any sink and b) the total number of sinks
  ! index_sink -- on root proc: == number of OLD sinks
  !               other procs: == # of old sinks PLUS cum. number of new sinks on lower-rank procs
  ! index_sink_tot -- root: == 0 if this is the first time we create sinks
  !           other procs : == cum. number of new sinks on lower-rank procs if this is the first
  !                 time we create sinks.  So index_sink == index_sink_tot if there are no OLD sinks
  ! -------------------------------!

  ! Loop over grids
  ncache=active(ilevel)%ngrid
  do igrid=1,ncache,nvector
     ngrid=MIN(nvector,ncache-igrid+1)
     do i=1,ngrid
        ind_grid(i)=active(ilevel)%igrid(igrid+i-1)
     end do
     
     ! Loop over cells
     do ind=1,twotondim
        iskip=ncoarse+(ind-1)*ngridmax
        do i=1,ngrid
           ind_cell(i)=iskip+ind_grid(i)
        end do
        
        ! Gather cells with a new sink
        nnew=0
        do i=1,ngrid
           if (flag2(ind_cell(i))>0)then
              nnew=nnew+1
              ind_grid_new(nnew)=ind_grid(i)
              ind_cell_new(nnew)=ind_cell(i)
           end if
        end do
        
        ! Create new sink particles
        do i=1,nnew
           index_sink=index_sink+1
           index_sink_tot=index_sink_tot+1

           ! Get gas variables
           d=uold(ind_cell_new(i),1)
           u=uold(ind_cell_new(i),2)
           v=uold(ind_cell_new(i),3)
           w=uold(ind_cell_new(i),4)
           e=uold(ind_cell_new(i),5)

           ! Get gas cell position
           x=(xg(ind_grid_new(i),1)+xc(ind,1)-skip_loc(1))*scale
           y=(xg(ind_grid_new(i),2)+xc(ind,2)-skip_loc(2))*scale
           z=(xg(ind_grid_new(i),3)+xc(ind,3)-skip_loc(3))*scale

           ! User defined density threshold
           d_thres=d_sink
           
           ! Jeans length related density threshold
           if(d_thres<0.0)then
              temp=max(e*(gamma-1.0),smallc**2)
              d_jeans=temp*3.1415926/(4.0*dx_loc)**2/factG
              d_thres=d_jeans
           endif

           ! Mass of the new sink
           msink_new(index_sink)=min((d-d_thres)*vol_loc,msink_max2)
           delta_mass_new(index_sink)=0d0

           ! Global index of the new sink
           oksink_new(index_sink)=1d0
           idsink_new(index_sink)=index_sink_tot

           ! Update linked list
           ind_grid_cloud(1)=ind_grid_new(i)
           call remove_free(ind_part_cloud,1)
           call add_list(ind_part_cloud,ind_grid_cloud,ok_true,1)
           ind_cloud=ind_part_cloud(1)

           ! Set new sink particle variables
           tp(ind_cloud)=birth_epoch     ! Birth epoch
           mp(ind_cloud)=msink_new(index_sink) ! Mass
           if(msink_dynamical > 0.0) mp(ind_cloud) = msink_dynamical
           levelp(ind_cloud)=ilevel      ! Level
           idp(ind_cloud)=-index_sink    ! Identity
           xp(ind_cloud,1)=x
           xp(ind_cloud,2)=y
           xp(ind_cloud,3)=z
           vp(ind_cloud,1)=u
           vp(ind_cloud,2)=v
           vp(ind_cloud,3)=w
           
           ! Store properties of the new sink
           tsink_new(index_sink)=birth_epoch
           xsink_new(index_sink,1)=x
           xsink_new(index_sink,2)=y
           xsink_new(index_sink,3)=z
           vsink_new(index_sink,1)=u
           vsink_new(index_sink,2)=v
           vsink_new(index_sink,3)=w
           
           uold(ind_cell_new(i),1)=uold(ind_cell_new(i),1)-msink_new(index_sink)/vol_loc
           
        end do
        ! End loop over new sink particle cells

     end do
     ! End loop over cells
  end do
  ! End loop over grids

  !---------------------------------------------------------
  ! Convert hydro variables back to conservative variables
  !---------------------------------------------------------
  ncache=active(ilevel)%ngrid
  do igrid=1,ncache,nvector
     ngrid=MIN(nvector,ncache-igrid+1)
     do i=1,ngrid
        ind_grid(i)=active(ilevel)%igrid(igrid+i-1)
     end do
     do ind=1,twotondim  
        iskip=ncoarse+(ind-1)*ngridmax
        do i=1,ngrid
           ind_cell(i)=iskip+ind_grid(i)
        end do
        do i=1,ngrid
           d=uold(ind_cell(i),1)
           u=uold(ind_cell(i),2)
           v=uold(ind_cell(i),3)
           w=uold(ind_cell(i),4)
           e=uold(ind_cell(i),5)
#ifdef SOLVERmhd
           bx1=uold(ind_cell(i),6)
           by1=uold(ind_cell(i),7)
           bz1=uold(ind_cell(i),8)
           bx2=uold(ind_cell(i),nvar+1)
           by2=uold(ind_cell(i),nvar+2)
           bz2=uold(ind_cell(i),nvar+3)
           e=e+0.125d0*((bx1+bx2)**2+(by1+by2)**2+(bz1+bz2)**2)/d
#endif
           e=e+0.5d0*(u**2+v**2+w**2)
           uold(ind_cell(i),1)=d
           uold(ind_cell(i),2)=d*u
           uold(ind_cell(i),3)=d*v
           uold(ind_cell(i),4)=d*w
           uold(ind_cell(i),5)=d*e
        end do
        do ivar=imetal,nvar
           do i=1,ngrid
              d=uold(ind_cell(i),1)
              w=uold(ind_cell(i),ivar)
              uold(ind_cell(i),ivar)=d*w
           end do
        end do
     end do
  end do

#ifndef WITHOUTMPI
  call MPI_ALLREDUCE(oksink_new,oksink_all,nsinkmax,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
  call MPI_ALLREDUCE(idsink_new,idsink_all,nsinkmax,MPI_INTEGER         ,MPI_SUM,MPI_COMM_WORLD,info)
  call MPI_ALLREDUCE(msink_new ,msink_all ,nsinkmax,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
  call MPI_ALLREDUCE(tsink_new ,tsink_all ,nsinkmax,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
  call MPI_ALLREDUCE(xsink_new ,xsink_all ,nsinkmax*ndim,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
  call MPI_ALLREDUCE(vsink_new ,vsink_all ,nsinkmax*ndim,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
  call MPI_ALLREDUCE(delta_mass_new,delta_mass_all,nsinkmax,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
#else
  oksink_all=oksink_new
  idsink_all=idsink_new
  msink_all=msink_new
  tsink_all=tsink_new
  xsink_all=xsink_new
  vsink_all=vsink_new
  delta_mass_all=delta_mass_new
#endif
  do isink=1,nsink
     if(oksink_all(isink)==1d0)then
        idsink(isink)=idsink_all(isink)
        msink(isink)=msink_all(isink)
        tsink(isink)=tsink_all(isink)
        xsink(isink,1:ndim)=xsink_all(isink,1:ndim)
        vsink(isink,1:ndim)=vsink_all(isink,1:ndim)
        delta_mass(isink)=delta_mass_all(isink)
     endif
  end do

#endif

end subroutine make_sink
!################################################################
!################################################################
!################################################################
!################################################################
subroutine merge_sink(ilevel)
  use pm_commons
  use amr_commons
  implicit none
#ifndef WITHOUTMPI
  include 'mpif.h'
#endif
  integer::ilevel
  !------------------------------------------------------------------------
  ! This routine merges sink usink the FOF algorithm.
  ! It keeps only the group centre of mass and remove other sinks.
  !------------------------------------------------------------------------
  integer::j,isink,ii,jj,kk,ind,idim,new_sink
  real(dp)::dx_loc,scale,dx_min,xx,yy,zz,rr,rmax2,rmax
  integer::igrid,jgrid,ipart,jpart,next_part,info
  integer::i,ig,ip,npart1,npart2,icpu,nx_loc
  integer::igrp,icomp,gndx,ifirst,ilast,indx
  integer,dimension(1:nvector),save::ind_grid,ind_part,ind_grid_part
  integer,dimension(:),allocatable::psink,gsink
  real(dp),dimension(1:3)::xbound,skip_loc

  if(numbtot(1,ilevel)==0)return
  if(nsink==0)return
  if(verbose)write(*,111)ilevel

  ! Mesh spacing in that level
  dx_loc=0.5D0**ilevel
  xbound(1:3)=(/dble(nx),dble(ny),dble(nz)/)
  nx_loc=(icoarse_max-icoarse_min+1)
  skip_loc=(/0.0d0,0.0d0,0.0d0/)
  if(ndim>0)skip_loc(1)=dble(icoarse_min)
  if(ndim>1)skip_loc(2)=dble(jcoarse_min)
  if(ndim>2)skip_loc(3)=dble(kcoarse_min)
  scale=boxlen/dble(nx_loc)
!  dx_min=scale*0.5D0**nlevelmax/aexp
  dx_min=scale*0.5D0**level_agn/aexp
  rmax=dble(ir_cloud)*dx_min ! Linking length in physical units
  rmax2=rmax*rmax

  allocate(psink(1:nsink),gsink(1:nsink))
  
  !-------------------------------
  ! Merge sinks using FOF
  !-------------------------------
  do isink=1,nsink
     psink(isink)=isink
     gsink(isink)=0
  end do
  
  igrp=0
  icomp=1
  ifirst=2
  do while(icomp.le.nsink)
     gndx=psink(icomp)
     if(gsink(gndx)==0)then
        igrp=igrp+1
        gsink(gndx)=igrp
     endif
     ilast=nsink
     do while((ilast-ifirst+1)>0)
        indx=psink(ifirst)
        xx=xsink(indx,1)-xsink(gndx,1)
        if(xx>scale*xbound(1)/2.0)then
           xx=xx-scale*xbound(1)
        endif
        if(xx<-scale*xbound(1)/2.0)then
           xx=xx+scale*xbound(1)
        endif
        rr=xx**2
        yy=xsink(indx,2)-xsink(gndx,2)
        if(yy>scale*xbound(2)/2.0)then
           yy=yy-scale*xbound(2)
        endif
        if(yy<-scale*xbound(2)/2.0)then
           yy=yy+scale*xbound(2)
        endif
        rr=yy**2+rr
        zz=xsink(indx,3)-xsink(gndx,3)
        if(zz>scale*xbound(3)/2.0)then
           zz=zz-scale*xbound(3)
        endif
        if(zz<-scale*xbound(3)/2.0)then
           zz=zz+scale*xbound(3)
        endif
        rr=zz**2+rr
        if(rr.le.rmax2)then
           ifirst=ifirst+1
           gsink(indx)=igrp
        else
           psink(ifirst)=psink(ilast)
           psink(ilast)=indx
           ilast=ilast-1
        endif
     end do
     icomp=icomp+1
  end do
  new_sink=igrp

  if(myid==1)then
     write(*,*)'Number of sinks after merging',new_sink
!     do isink=1,nsink
!        write(*,'(3(I3,1x),3(1PE10.3))')isink,psink(isink),gsink(isink),xsink(isink,1:ndim)
!     end do
  endif
  
  !----------------------------------------------------
  ! Compute group centre of mass and average velocty
  !----------------------------------------------------
  xsink_new=0d0; vsink_new=0d0; msink_new=0d0; tsink_new=0d0; delta_mass_new=0d0
  oksink_all=0d0; oksink_new=0d0; idsink_all=0d0; idsink_new=0d0
  do isink=1,nsink
     igrp=gsink(isink)
     if(oksink_new(igrp)==0d0)then
        oksink_all(isink)=igrp
        oksink_new(igrp)=isink
     endif
     msink_new(igrp)=msink_new(igrp)+msink(isink)
     delta_mass_new(igrp)=delta_mass_new(igrp)+delta_mass(isink)
     if(tsink_new(igrp)==0d0)then
        tsink_new(igrp)=tsink(isink)
     else
        tsink_new(igrp)=min(tsink_new(igrp),tsink(isink))
     endif
     if(idsink_new(igrp)==0)then
        idsink_new(igrp)=idsink(isink)
     else
        idsink_new(igrp)=min(idsink_new(igrp),idsink(isink))
     endif

     xx=xsink(isink,1)-xsink(int(oksink_new(igrp)),1)
     if(xx>scale*xbound(1)/2.0)then
        xx=xx-scale*xbound(1)
     endif
     if(xx<-scale*xbound(1)/2.0)then
        xx=xx+scale*xbound(1)
     endif
     xsink_new(igrp,1)=xsink_new(igrp,1)+msink(isink)*xx
     vsink_new(igrp,1)=vsink_new(igrp,1)+msink(isink)*vsink(isink,1)
     yy=xsink(isink,2)-xsink(int(oksink_new(igrp)),2)
     if(yy>scale*xbound(2)/2.0)then
        yy=yy-scale*xbound(2)
     endif
     if(yy<-scale*xbound(2)/2.0)then
        yy=yy+scale*xbound(2)
     endif
     xsink_new(igrp,2)=xsink_new(igrp,2)+msink(isink)*yy
     vsink_new(igrp,2)=vsink_new(igrp,2)+msink(isink)*vsink(isink,2)
     zz=xsink(isink,3)-xsink(int(oksink_new(igrp)),3)
     if(zz>scale*xbound(3)/2.0)then
        zz=zz-scale*xbound(3)
     endif
     if(zz<-scale*xbound(3)/2.0)then
        zz=zz+scale*xbound(3)
     endif
     xsink_new(igrp,3)=xsink_new(igrp,3)+msink(isink)*zz
     vsink_new(igrp,3)=vsink_new(igrp,3)+msink(isink)*vsink(isink,3)
  end do
  do isink=1,new_sink
     xsink_new(isink,1)=xsink_new(isink,1)/msink_new(isink)+xsink(int(oksink_new(isink)),1)
     vsink_new(isink,1)=vsink_new(isink,1)/msink_new(isink)
     xsink_new(isink,2)=xsink_new(isink,2)/msink_new(isink)+xsink(int(oksink_new(isink)),2)
     vsink_new(isink,2)=vsink_new(isink,2)/msink_new(isink)
     xsink_new(isink,3)=xsink_new(isink,3)/msink_new(isink)+xsink(int(oksink_new(isink)),3)
     vsink_new(isink,3)=vsink_new(isink,3)/msink_new(isink)
  end do
  nsink=new_sink
  msink(1:nsink)=msink_new(1:nsink)
  tsink(1:nsink)=tsink_new(1:nsink)
  idsink(1:nsink)=idsink_new(1:nsink)
  delta_mass(1:nsink)=delta_mass_new(1:nsink)
  xsink(1:nsink,1:ndim)=xsink_new(1:nsink,1:ndim)
  vsink(1:nsink,1:ndim)=vsink_new(1:nsink,1:ndim)

  ! Periodic boundary conditions
  do isink=1,nsink
     xx=xsink(isink,1)
     if(xx<-scale*skip_loc(1))then
        xx=xx+scale*(xbound(1)-skip_loc(1))
     endif
     if(xx>scale*(xbound(1)-skip_loc(1)))then
        xx=xx-scale*(xbound(1)-skip_loc(1))
     endif
     xsink(isink,1)=xx
     yy=xsink(isink,2)
     if(yy<-scale*skip_loc(2))then
        yy=yy+scale*(xbound(2)-skip_loc(2))
     endif
     if(yy>scale*(xbound(2)-skip_loc(2)))then
        yy=yy-scale*(xbound(2)-skip_loc(2))
     endif
     xsink(isink,2)=yy
     zz=xsink(isink,3)
     if(zz<-scale*skip_loc(3))then
        zz=zz+scale*(xbound(3)-skip_loc(3))
     endif
     if(zz>scale*(xbound(3)-skip_loc(3)))then
        zz=zz-scale*(xbound(3)-skip_loc(3))
     endif
     xsink(isink,3)=zz
  enddo

  deallocate(psink,gsink)
  
  !-----------------------------------------------------
  ! Remove sink particles that are part of a FOF group.
  !-----------------------------------------------------
  ! Loop over cpus
  do icpu=1,ncpu
     igrid=headl(icpu,ilevel)
     ig=0
     ip=0
     ! Loop over grids
     do jgrid=1,numbl(icpu,ilevel)
        npart1=numbp(igrid)  ! Number of particles in the grid
        npart2=0
        
        ! Count sink particles
        if(npart1>0)then
           ipart=headp(igrid)
           ! Loop over particles
           do jpart=1,npart1
              ! Save next particle   <--- Very important !!!
              next_part=nextp(ipart)
              if(idp(ipart).lt.0 .and. idp(ipart) .gt. -1*nsinkmax)then
                 npart2=npart2+1
              endif

              ipart=next_part  ! Go to next particle
           end do
        endif
        
        ! Gather sink particles
        if(npart2>0)then        
           ig=ig+1
           ind_grid(ig)=igrid
           ipart=headp(igrid)
           ! Loop over particles
           do jpart=1,npart1
              ! Save next particle   <--- Very important !!!
              next_part=nextp(ipart)
              ! Select only sink particles
              if(idp(ipart).lt.0 .and. idp(ipart) .gt. -1*nsinkmax)then
                 if(ig==0)then
                    ig=1
                    ind_grid(ig)=igrid
                 end if
                 ip=ip+1
                 ind_part(ip)=ipart
                 ind_grid_part(ip)=ig   
              endif
              if(ip==nvector)then
                 call kill_sink(ind_grid,ind_part,ind_grid_part,ig,ip,ilevel)
                 ip=0
                 ig=0
              end if
              ipart=next_part  ! Go to next particle
           end do
           ! End loop over particles
        end if

        igrid=next(igrid)   ! Go to next grid
     end do

     ! End loop over grids
     if(ip>0)call kill_sink(ind_grid,ind_part,ind_grid_part,ig,ip,ilevel)
  end do 
  ! End loop over cpus

111 format('   Entering merge_sink for level ',I2)

end subroutine merge_sink
!################################################################
!################################################################
!################################################################
!################################################################
subroutine kill_sink(ind_grid,ind_part,ind_grid_part,ng,np,ilevel)
  use amr_commons
  use pm_commons
  use hydro_commons
  implicit none
  integer::ng,np,ilevel
  integer,dimension(1:nvector)::ind_grid
  integer,dimension(1:nvector)::ind_grid_part,ind_part
  !-----------------------------------------------------------------------
  ! This routine is called by subroutine merge_sink
  ! It removes sink particles that are part of a FOF group.
  !-----------------------------------------------------------------------
  integer::j,isink,ii,jj,kk,ind,idim,isink_new
  real(dp)::dx_loc,scale,dx_min,xx,yy,zz,rr,rmax
  ! Particle-based arrays
  logical ,dimension(1:nvector),save::ok

  do j=1,np
     isink=-idp(ind_part(j))
     ok(j)=(oksink_all(isink)==0)
     if(.not. ok(j))then
        isink_new=oksink_all(isink)
        idp(ind_part(j))=-isink_new
        mp(ind_part(j))=msink(isink_new)
        if(msink_dynamical > 0.0) mp(ind_part(j)) = msink_dynamical

        xp(ind_part(j),1)=xsink(isink_new,1)
        vp(ind_part(j),1)=vsink(isink_new,1)
        xp(ind_part(j),2)=xsink(isink_new,2)
        vp(ind_part(j),2)=vsink(isink_new,2)
        xp(ind_part(j),3)=xsink(isink_new,3)
        vp(ind_part(j),3)=vsink(isink_new,3)
     endif
  end do

  ! Remove particles from parent linked list
  call remove_list(ind_part,ind_grid_part,ok,np)
  call add_free_cond(ind_part,ok,np)

end subroutine kill_sink
!################################################################
!################################################################
!################################################################
!################################################################
subroutine create_cloud(ilevel)
  use pm_commons
  use amr_commons
  implicit none
#ifndef WITHOUTMPI
  include 'mpif.h'
#endif
  integer::ilevel
  !------------------------------------------------------------------------
  ! This routine creates a cloud of test particle around each sink particle.
  !------------------------------------------------------------------------
  integer::igrid,jgrid,ipart,jpart,next_part,info
  integer::i,ig,ip,npart1,npart2,icpu,nx_loc
  integer,dimension(1:nvector),save::ind_grid,ind_part,ind_grid_part

  if(numbtot(1,ilevel)==0)return
  if(verbose)write(*,111)ilevel

  ! Gather sink particles only.

  ! Loop over cpus
  do icpu=1,ncpu
     igrid=headl(icpu,ilevel)
     ig=0
     ip=0
     ! Loop over grids
     do jgrid=1,numbl(icpu,ilevel)
        npart1=numbp(igrid)  ! Number of particles in the grid
        npart2=0
        
        ! Count sink particles
        if(npart1>0)then
           ipart=headp(igrid)
           ! Loop over particles
           do jpart=1,npart1
              ! Save next particle   <--- Very important !!!
              next_part=nextp(ipart)
              if(idp(ipart).lt.0 .and. idp(ipart) .gt. -1*nsinkmax)then
                 npart2=npart2+1
              endif

              ipart=next_part  ! Go to next particle
           end do
        endif
        
        ! Gather sink particles
        if(npart2>0)then        
           ig=ig+1
           ind_grid(ig)=igrid
           ipart=headp(igrid)
           ! Loop over particles
           do jpart=1,npart1
              ! Save next particle   <--- Very important !!!
              next_part=nextp(ipart)
              ! Select only sink particles
              if(idp(ipart).lt.0 .and. idp(ipart) .gt. -1*nsinkmax)then
                 if(ig==0)then
                    ig=1
                    ind_grid(ig)=igrid
                 end if
                 ip=ip+1
                 ind_part(ip)=ipart
                 ind_grid_part(ip)=ig   
              endif
              if(ip==nvector)then
                 call mk_cloud(ind_grid,ind_part,ind_grid_part,ig,ip,ilevel)
                 ip=0
                 ig=0
              end if
              ipart=next_part  ! Go to next particle
           end do
           ! End loop over particles
        end if

        igrid=next(igrid)   ! Go to next grid
     end do

     ! End loop over grids
     if(ip>0)call mk_cloud(ind_grid,ind_part,ind_grid_part,ig,ip,ilevel)
  end do 
  ! End loop over cpus

111 format('   Entering create_cloud for level ',I2)

end subroutine create_cloud
!################################################################
!################################################################
!################################################################
!################################################################
subroutine mk_cloud(ind_grid,ind_part,ind_grid_part,ng,np,ilevel)
  use amr_commons
  use pm_commons
  use hydro_commons
  implicit none
  integer::ng,np,ilevel
  integer,dimension(1:nvector)::ind_grid
  integer,dimension(1:nvector)::ind_grid_part,ind_part
  !-----------------------------------------------------------------------
  ! This routine is called by subroutine create_cloud.
  !-----------------------------------------------------------------------
  logical::error
  integer::j,isink,ii,jj,kk,ind,idim,nx_loc,ncloud
  real(dp)::dx_loc,scale,dx_min,xx,yy,zz,rr,rmax
  ! Particle-based arrays
  integer ,dimension(1:nvector),save::ind_cloud
  logical ,dimension(1:nvector),save::ok_true=.true.

  ! Mesh spacing in that level
  dx_loc=0.5D0**ilevel
  nx_loc=(icoarse_max-icoarse_min+1)
  scale=boxlen/dble(nx_loc)
!  dx_min=scale*0.5D0**nlevelmax/aexp
  dx_min=scale*0.5D0**level_agn/aexp

  rmax=dble(ir_cloud)*dx_min
  xx=0.0; yy=0.0;zz=0.0
  ncloud=0
  do kk=-2*ir_cloud,2*ir_cloud
     zz=dble(kk)*dx_min/2.0
     do jj=-2*ir_cloud,2*ir_cloud
        yy=dble(jj)*dx_min/2.0
        do ii=-2*ir_cloud,2*ir_cloud
           xx=dble(ii)*dx_min/2.0
           rr=sqrt(xx*xx+yy*yy+zz*zz)
           if(rr<=rmax)ncloud=ncloud+1
        end do
     end do
  end do

  write(*,*) "ncloud set to ", ncloud

  do kk=-2*ir_cloud,2*ir_cloud
     zz=dble(kk)*dx_min/2.0
     do jj=-2*ir_cloud,2*ir_cloud
        yy=dble(jj)*dx_min/2.0
        do ii=-2*ir_cloud,2*ir_cloud
           xx=dble(ii)*dx_min/2.0
           rr=sqrt(xx*xx+yy*yy+zz*zz)
           if(rr>0.and.rr<=rmax)then
              call remove_free(ind_cloud,np)
              call add_list(ind_cloud,ind_grid_part,ok_true,np)
              do j=1,np
                 isink=-idp(ind_part(j))
                 idp(ind_cloud(j))=-isink
                 mp(ind_cloud(j))=msink(isink)/dble(ncloud)
                 if(msink_dynamical > 0.0) mp(ind_cloud(j)) = msink_dynamical/dble(ncloud)

                 xp(ind_cloud(j),1)=xp(ind_part(j),1)+xx
                 vp(ind_cloud(j),1)=vsink(isink,1)
                 xp(ind_cloud(j),2)=xp(ind_part(j),2)+yy
                 vp(ind_cloud(j),2)=vsink(isink,2)
                 xp(ind_cloud(j),3)=xp(ind_part(j),3)+zz
                 vp(ind_cloud(j),3)=vsink(isink,3)
                 !!!JMG (not sure if this is OK, or why it's not here already)
!!                 levelp(ind_cloud(j))=ilevel
                 !!!JMG
              end do
           end if
        end do
     end do
  end do

  ! Reduce sink particle mass
  do j=1,np
     isink=-idp(ind_part(j))
     mp(ind_part(j))=msink(isink)/dble(ncloud)
     if(msink_dynamical > 0.0) mp(ind_part(j)) = msink_dynamical/dble(ncloud)

     vp(ind_part(j),1)=vsink(isink,1)
     vp(ind_part(j),2)=vsink(isink,2)
     vp(ind_part(j),3)=vsink(isink,3)
  end do

end subroutine mk_cloud
!################################################################
!################################################################
!################################################################
!################################################################
subroutine kill_cloud(ilevel)
  use pm_commons
  use amr_commons
  implicit none
  integer::ilevel
  !------------------------------------------------------------------------
  ! This routine removes from the list cloud particles and keeps only
  ! sink particles. 
  !------------------------------------------------------------------------
  integer::igrid,jgrid,ipart,jpart,next_part
  integer::i,ig,ip,npart1,npart2,icpu,nx_loc
  integer,dimension(1:nvector),save::ind_grid,ind_part,ind_grid_part

  if(numbtot(1,ilevel)==0)return
  if(verbose)write(*,111)ilevel

  ! Gather sink and cloud particles.

  ! Loop over cpus
  do icpu=1,ncpu
     igrid=headl(icpu,ilevel)
     ig=0
     ip=0
     ! Loop over grids
     do jgrid=1,numbl(icpu,ilevel)
        npart1=numbp(igrid)  ! Number of particles in the grid
        npart2=0
        
        ! Count sink and cloud particles
        if(npart1>0)then
           ipart=headp(igrid)
           ! Loop over particles
           do jpart=1,npart1
              ! Save next particle   <--- Very important !!!
              next_part=nextp(ipart)
              if(idp(ipart).lt.0 .and. idp(ipart) .gt. -1*nsinkmax)then
                 npart2=npart2+1
              endif

              ipart=next_part  ! Go to next particle
           end do
        endif
        
        ! Gather sink and cloud particles
        if(npart2>0)then        
           ig=ig+1
           ind_grid(ig)=igrid
           ipart=headp(igrid)
           ! Loop over particles
           do jpart=1,npart1
              ! Save next particle   <--- Very important !!!
              next_part=nextp(ipart)
              ! Select only sink particles
              if(idp(ipart).lt.0 .and. idp(ipart) .gt. -1*nsinkmax)then
                 if(ig==0)then
                    ig=1
                    ind_grid(ig)=igrid
                 end if
                 ip=ip+1
                 ind_part(ip)=ipart
                 ind_grid_part(ip)=ig   
              endif
              if(ip==nvector)then
                 call rm_cloud(ind_grid,ind_part,ind_grid_part,ig,ip,ilevel)
                 ip=0
                 ig=0
              end if
              ipart=next_part  ! Go to next particle
           end do
           ! End loop over particles
        end if
        
        igrid=next(igrid)   ! Go to next grid
     end do
     
     ! End loop over grids
     if(ip>0)call rm_cloud(ind_grid,ind_part,ind_grid_part,ig,ip,ilevel)
  end do

111 format('   Entering kill_cloud for level ',I2)

end subroutine kill_cloud
!################################################################
!################################################################
!################################################################
!################################################################
subroutine rm_cloud(ind_grid,ind_part,ind_grid_part,ng,np,ilevel)
  use amr_commons
  use pm_commons
  use hydro_commons
  implicit none
  integer::ng,np,ilevel
  integer,dimension(1:nvector)::ind_grid
  integer,dimension(1:nvector)::ind_grid_part,ind_part
  !-----------------------------------------------------------------------
  ! This routine is called by subroutine kill_cloud.
  !-----------------------------------------------------------------------
  logical::error
  integer::j,isink,ii,jj,kk,ind,idim,nx_loc
  real(dp)::dx_loc,scale,dx_min,xx,yy,zz,rr,r2,r2_eps
  ! Particle-based arrays
  logical,dimension(1:nvector),save::ok

  ! Mesh spacing in that level
  dx_loc=0.5D0**ilevel
  nx_loc=(icoarse_max-icoarse_min+1)
  scale=boxlen/dble(nx_loc)
!  dx_min=scale*0.5D0**nlevelmax/aexp
  dx_min=scale*0.5D0**level_agn/aexp
  r2_eps=(1d-15*dx_min)**2

  do j=1,np
     isink=-idp(ind_part(j))

     ! get distance between this cloud particle and the sink
     r2=0d0
     do idim=1,ndim
        r2=r2+(xp(ind_part(j),idim)-xsink(isink,idim))**2
     end do

     ! Don't kill off the cloud particle that is in the same
     ! position as the sink.  This will represent the sink
     ! itself.
     ok(j)=r2>r2_eps
  end do
  
  ! Remove particles from parent linked list
  call remove_list(ind_part,ind_grid_part,ok,np)
  call add_free_cond(ind_part,ok,np)

end subroutine rm_cloud
!################################################################
!################################################################
!################################################################
!################################################################
subroutine bondi_hoyle(ilevel)
  use pm_commons
  use amr_commons
  implicit none
#ifndef WITHOUTMPI
  include 'mpif.h'
#endif
  integer::ilevel
  !------------------------------------------------------------------------
  ! This routine computes the parameters of Bondi-Hoyle
  ! accretion for sink particles.
  ! It calls routine bondi_velocity and bondi_average.
  !------------------------------------------------------------------------
  integer::igrid,jgrid,ipart,jpart,next_part,idim,info
  integer::i,ig,ip,npart1,npart2,icpu,nx_loc,isink
  integer,dimension(1:nvector),save::ind_grid,ind_part,ind_grid_part
  real(dp)::r2,dx_loc,dx_min,scale,factG

  if(numbtot(1,ilevel)==0)return
  if(verbose)write(*,111)ilevel

  ! Gravitational constant
  factG=1d0
  if(cosmo)factG=3d0/8d0/3.1415926*omega_m*aexp

  ! Mesh spacing in that level
  dx_loc=0.5D0**ilevel
  nx_loc=(icoarse_max-icoarse_min+1)
  scale=boxlen/dble(nx_loc)
!  dx_min=scale*0.5D0**nlevelmax/aexp
  dx_min=scale*0.5D0**level_agn/aexp

  ! Reset new sink variables
  v2sink_new=0d0; c2sink_new=0d0; oksink_new=0d0

  ! Gather sink particles only.

  ! Loop over cpus
  do icpu=1,ncpu
     igrid=headl(icpu,ilevel)
     ig=0
     ip=0
     ! Loop over grids
     do jgrid=1,numbl(icpu,ilevel)
        npart1=numbp(igrid)  ! Number of particles in the grid
        npart2=0

        ! Count only sink particles
        if(npart1>0)then
           ipart=headp(igrid)
           ! Loop over particles
           do jpart=1,npart1
              ! Save next particle   <--- Very important !!!
              next_part=nextp(ipart)
              if(idp(ipart).lt.0 .and. idp(ipart) .gt. -1*nsinkmax)then
                 isink=-idp(ipart)
                 r2=0.0
                 do idim=1,ndim
                    r2=r2+(xp(ipart,idim)-xsink(isink,idim))**2
                 end do
                 if(r2==0.0)then
                    npart2=npart2+1
                 end if
              endif

              ipart=next_part  ! Go to next particle
           end do
        endif

        ! Gather only sink particles
        if(npart2>0)then
           ig=ig+1
           ind_grid(ig)=igrid
           ipart=headp(igrid)
           ! Loop over particles
           do jpart=1,npart1
              ! Save next particle   <--- Very important !!!
              next_part=nextp(ipart)
              ! Select only sink particles
              if(idp(ipart).lt.0 .and. idp(ipart) .gt. -1*nsinkmax)then
                 isink=-idp(ipart)
                 r2=0.0
                 do idim=1,ndim
                    r2=r2+(xp(ipart,idim)-xsink(isink,idim))**2
                 end do
                 if(r2==0.0)then
                    if(ig==0)then
                       ig=1
                       ind_grid(ig)=igrid
                    end if
                    ip=ip+1
                    ind_part(ip)=ipart
                    ind_grid_part(ip)=ig
                 endif
              endif

              if(ip==nvector)then
                 ! calculate soundspeed and velocity (relative to sink particle) 
                 ! in the cell where each sink particle lives.  This sets
                 ! the values of c2sink_new, v2sink_new, and oksink_new
                 call bondi_velocity(ind_grid,ind_part,ind_grid_part,ig,ip,ilevel)
                 ip=0
                 ig=0
              end if
              ipart=next_part  ! Go to next particle
           end do
           ! End loop over particles
        end if

        igrid=next(igrid)   ! Go to next grid
     end do

     ! End loop over grids
     if(ip>0)call bondi_velocity(ind_grid,ind_part,ind_grid_part,ig,ip,ilevel)
  end do
  ! End loop over cpus

  if(nsink>0)then
#ifndef WITHOUTMPI
     call MPI_ALLREDUCE(oksink_new,oksink_all,nsinkmax,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
     call MPI_ALLREDUCE(c2sink_new,c2sink_all,nsinkmax,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
     call MPI_ALLREDUCE(v2sink_new,v2sink_all,nsinkmax,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
#else
     oksink_all=oksink_new
     c2sink_all=c2sink_new
     v2sink_all=v2sink_new
#endif
  endif

  do isink=1,nsink
     if(oksink_all(isink)==1d0)then
        c2sink(isink)=c2sink_all(isink)
        v2sink(isink)=v2sink_all(isink)
        ! Compute sink radius
        r2sink(isink)=(factG*msink(isink)/(v2sink(isink)+c2sink(isink)))**2
        r2k(isink)=min(max(r2sink(isink),(dx_min/4.0)**2),(2.*dx_min)**2)
     endif
  end do

  ! Gather sink and cloud particles.
  wden=0d0; wvol=0d0; weth=0d0; wmom=0d0
  if(mass_weight_acc) accweight=0d0

  ! Loop over cpus
  do icpu=1,ncpu
     igrid=headl(icpu,ilevel)
     ig=0
     ip=0
     ! Loop over grids
     do jgrid=1,numbl(icpu,ilevel)
        npart1=numbp(igrid)  ! Number of particles in the grid
        npart2=0
        
        ! Count sink and cloud particles
        if(npart1>0)then
           ipart=headp(igrid)
           ! Loop over particles
           do jpart=1,npart1
              ! Save next particle   <--- Very important !!!
              next_part=nextp(ipart)
!!              if(idp(ipart).lt.0 .and. idp(ipart) .gt. -1*nsinkmax)then
              if(idp(ipart).lt.0) then
                 if(idp(ipart) > -1*nsinkmax) then
                    npart2=npart2+1
                 endif
                    
              endif
              ipart=next_part  ! Go to next particle
           end do

        
        endif
        
        ! Gather sink and cloud particles
        if(npart2>0)then        

           ig=ig+1
           ind_grid(ig)=igrid
           ipart=headp(igrid)
           ! Loop over particles
           do jpart=1,npart1
              ! Save next particle   <--- Very important !!!
              next_part=nextp(ipart)
              ! Select only sink particles
              if(idp(ipart).lt.0 .and. idp(ipart) .gt. -1*nsinkmax)then

                 if(ig==0)then
                    ig=1
                    ind_grid(ig)=igrid
                 end if
                 ip=ip+1
                 ind_part(ip)=ipart
                 ind_grid_part(ip)=ig   
              endif
              if(ip==nvector)then
                 ! for each "cloud" particle, find the local soundspeed, velocity,
                 ! density, and thermal energy, weighted according to distance 
                 ! from the sink particle.  This sets wden,wvol, weth,and wmom
                 call bondi_average(ind_grid,ind_part,ind_grid_part,ig,ip,ilevel)
                 ip=0
                 ig=0
              end if
              ipart=next_part  ! Go to next particle
           end do
           ! End loop over particles
        end if

        igrid=next(igrid)   ! Go to next grid
     end do

     ! End loop over grids
     if(ip>0)call bondi_average(ind_grid,ind_part,ind_grid_part,ig,ip,ilevel)
  end do 
  ! End loop over cpus

  if(nsink>0)then
#ifndef WITHOUTMPI
     call MPI_ALLREDUCE(wden,wden_new,nsinkmax,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
     call MPI_ALLREDUCE(wvol,wvol_new,nsinkmax,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
     call MPI_ALLREDUCE(weth,weth_new,nsinkmax,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
     call MPI_ALLREDUCE(wmom,wmom_new,nsinkmax*ndim,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
     if(mass_weight_acc) then
        call MPI_ALLREDUCE(accweight,accweight_new,nsinkmax,&
             MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
     endif
#else
     wden_new=wden
     wvol_new=wvol
     weth_new=weth
     wmom_new=wmom
     if(mass_weight_acc) accweight_new = accweight
#endif
  endif
  do isink=1,nsink
     weighted_density(isink,ilevel)=wden_new(isink)
     weighted_volume(isink,ilevel)=wvol_new(isink)
     weighted_momentum(isink,ilevel,1:ndim)=wmom_new(isink,1:ndim)
     weighted_ethermal(isink,ilevel)=weth_new(isink)
     if(mass_weight_acc) weighted_accweight(isink,ilevel)=accweight_new(isink)
  end do

111 format('   Entering bondi_hoyle for level ',I2)

end subroutine bondi_hoyle
!################################################################
!################################################################
!################################################################
!################################################################
subroutine bondi_velocity(ind_grid,ind_part,ind_grid_part,ng,np,ilevel)
  use amr_commons
  use pm_commons
  use hydro_commons
  implicit none
  integer::ng,np,ilevel
  integer,dimension(1:nvector)::ind_grid
  integer,dimension(1:nvector)::ind_grid_part,ind_part
  !-----------------------------------------------------------------------
  ! This routine is called by subroutine bondi_hoyle.
  ! It computes the gas velocity and soud speed in the cell
  ! each sink particle sits in.
  !-----------------------------------------------------------------------
  integer::i,j,idim,nx_loc,isink
  real(dp)::xxx,mmm,r2,v2,c2,d,u,v,w,e,bx1,bx2,by1,by2,bz1,bz2
  real(dp)::dx,dx_loc,scale,vol_loc
  logical::error
  ! Grid based arrays
  real(dp),dimension(1:nvector,1:ndim),save::x0
  integer ,dimension(1:nvector),save::ind_cell
  integer ,dimension(1:nvector,1:threetondim),save::nbors_father_cells
  integer ,dimension(1:nvector,1:twotondim),save::nbors_father_grids
  ! Particle based arrays
  integer,dimension(1:nvector),save::igrid_son,ind_son
  integer,dimension(1:nvector),save::list1
  logical,dimension(1:nvector),save::ok
  real(dp),dimension(1:nvector),save::meff
  real(dp),dimension(1:nvector,1:ndim),save::x
  integer ,dimension(1:nvector,1:ndim),save::id,igd,icd
  integer ,dimension(1:nvector),save::igrid,icell,indp,kg
  real(dp),dimension(1:3)::skip_loc

  if(verbose) write(*,*) "Entering bondi_velocity"

  ! Mesh spacing in that level
  dx=0.5D0**ilevel
  nx_loc=(icoarse_max-icoarse_min+1)
  skip_loc=(/0.0d0,0.0d0,0.0d0/)
  if(ndim>0)skip_loc(1)=dble(icoarse_min)
  if(ndim>1)skip_loc(2)=dble(jcoarse_min)
  if(ndim>2)skip_loc(3)=dble(kcoarse_min)
  scale=boxlen/dble(nx_loc)
  dx_loc=dx*scale
  vol_loc=dx_loc**ndim

#if NDIM==3
  ! Lower left corner of 3x3x3 grid-cube
  do idim=1,ndim
     do i=1,ng
        x0(i,idim)=xg(ind_grid(i),idim)-3.0D0*dx
     end do
  end do

  ! Gather 27 neighboring father cells (should be present anytime !)
  do i=1,ng
     ind_cell(i)=father(ind_grid(i))
  end do
  call get3cubefather(ind_cell,nbors_father_cells,nbors_father_grids,ng,ilevel)

  ! Rescale position at level ilevel
  do idim=1,ndim
     do j=1,np
        x(j,idim)=xp(ind_part(j),idim)/scale+skip_loc(idim)
     end do
  end do
  do idim=1,ndim
     do j=1,np
        x(j,idim)=x(j,idim)-x0(ind_grid_part(j),idim)
     end do
  end do
  do idim=1,ndim
     do j=1,np
        x(j,idim)=x(j,idim)/dx
     end do
  end do

  ! Check for illegal moves
  error=.false.
  do idim=1,ndim
     do j=1,np
        if(x(j,idim)<=0.0D0.or.x(j,idim)>=6.0D0)error=.true.
     end do
  end do
  if(error)then
     write(*,*)'problem in bondi_velocity'
     write(*,*)ilevel,ng,np
     stop
  end if

  ! NGP at level ilevel
  do idim=1,ndim
     do j=1,np
        id(j,idim)=x(j,idim)
     end do
  end do

   ! Compute parent grids
  do idim=1,ndim
     do j=1,np
        igd(j,idim)=id(j,idim)/2
     end do
  end do
  do j=1,np
     kg(j)=1+igd(j,1)+3*igd(j,2)+9*igd(j,3)
  end do
  do j=1,np
     igrid(j)=son(nbors_father_cells(ind_grid_part(j),kg(j)))
  end do

  ! Check if particles are entirely in level ilevel
  ok(1:np)=.true.
  do j=1,np
     ok(j)=ok(j).and.igrid(j)>0
  end do
  ! Compute parent cell position
  do idim=1,ndim
     do j=1,np
        if(ok(j))then
           icd(j,idim)=id(j,idim)-2*igd(j,idim)
        end if
     end do
  end do
  do j=1,np
     if(ok(j))then
        icell(j)=1+icd(j,1)+2*icd(j,2)+4*icd(j,3)
     end if
  end do

  ! Compute parent cell adress
  do j=1,np
     if(ok(j))then
        indp(j)=ncoarse+(icell(j)-1)*ngridmax+igrid(j)
     end if
  end do

  ! Gather hydro variables
  do j=1,np
     if(ok(j))then
        d=uold(indp(j),1)
        u=uold(indp(j),2)/d
        v=uold(indp(j),3)/d
        w=uold(indp(j),4)/d
        e=uold(indp(j),5)/d
#ifdef SOLVERmhd
        bx1=uold(indp(j),6)
        by1=uold(indp(j),7)
        bz1=uold(indp(j),8)
        bx2=uold(indp(j),nvar+1)
        by2=uold(indp(j),nvar+2)
        bz2=uold(indp(j),nvar+3)
        e=e-0.125d0*((bx1+bx2)**2+(by1+by2)**2+(bz1+bz2)**2)/d
#endif
        v2=(u**2+v**2+w**2)
        e=e-0.5d0*v2
        c2=MAX(gamma*(gamma-1.0)*e,smallc**2)
        isink=-idp(ind_part(j))
        v2=(u-vsink(isink,1))**2+(v-vsink(isink,2))**2+(w-vsink(isink,3))**2
        v2sink_new(isink)=v2
        c2sink_new(isink)=c2
        oksink_new(isink)=1d0
     endif
  end do

#endif

end subroutine bondi_velocity
!################################################################
!################################################################
!################################################################
!################################################################
subroutine bondi_average(ind_grid,ind_part,ind_grid_part,ng,np,ilevel)
  use amr_commons
  use pm_commons
  use hydro_commons
  implicit none
  integer::ng,np,ilevel
  integer,dimension(1:nvector)::ind_grid
  integer,dimension(1:nvector)::ind_grid_part,ind_part
  !-----------------------------------------------------------------------
  ! This routine is called by subroutine bondi_hoyle. Each cloud particle
  ! reads up the value of density, sound speed and velocity from its
  ! position in the grid.
  !-----------------------------------------------------------------------
  logical::error
  integer::i,j,ind,idim,nx_loc,isink
  real(dp)::d,u,v=0d0,w=0d0,e,bx1,bx2,by1,by2,bz1,bz2
  real(dp)::dx,length,scale,weight,r2
  ! Grid-based arrays
  real(dp),dimension(1:nvector,1:ndim),save::x0
  integer ,dimension(1:nvector),save::ind_cell
  integer ,dimension(1:nvector,1:threetondim),save::nbors_father_cells
  integer ,dimension(1:nvector,1:twotondim),save::nbors_father_grids
  ! Particle-based arrays
  logical ,dimension(1:nvector),save::ok
  real(dp),dimension(1:nvector),save::dgas,ugas,vgas,wgas,egas
  real(dp),dimension(1:nvector,1:ndim),save::x,dd,dg
  integer ,dimension(1:nvector,1:ndim),save::ig,id,igg,igd,icg,icd
  real(dp),dimension(1:nvector,1:twotondim),save::vol
  integer ,dimension(1:nvector,1:twotondim),save::igrid,icell,indp,kg
  real(dp),dimension(1:3)::skip_loc

  ! Mesh spacing in that level
  dx=0.5D0**ilevel 
  nx_loc=(icoarse_max-icoarse_min+1)
  skip_loc=(/0.0d0,0.0d0,0.0d0/)
  if(ndim>0)skip_loc(1)=dble(icoarse_min)
  if(ndim>1)skip_loc(2)=dble(jcoarse_min)
  if(ndim>2)skip_loc(3)=dble(kcoarse_min)
  scale=boxlen/dble(nx_loc)

  ! Lower left corner of 3x3x3 grid-cube
  do idim=1,ndim
     do i=1,ng
        x0(i,idim)=xg(ind_grid(i),idim)-3.0D0*dx
     end do
  end do

  ! Gather 27 neighboring father cells (should be present anytime !)
  do i=1,ng
     ind_cell(i)=father(ind_grid(i))
  end do
  call get3cubefather(ind_cell,nbors_father_cells,nbors_father_grids,ng,ilevel)

  ! Rescale position at level ilevel
  do idim=1,ndim
     do j=1,np
        x(j,idim)=xp(ind_part(j),idim)/scale+skip_loc(idim)
     end do
  end do
  do idim=1,ndim
     do j=1,np
        x(j,idim)=x(j,idim)-x0(ind_grid_part(j),idim)
     end do
  end do
  do idim=1,ndim
     do j=1,np
        x(j,idim)=x(j,idim)/dx
     end do
  end do

  ! Check for illegal moves
  error=.false.
  do idim=1,ndim
     do j=1,np
        if(x(j,idim)<0.5D0.or.x(j,idim)>5.5D0)error=.true.
     end do
  end do
  if(error)then
     write(*,*)'problem in average_density'
     do idim=1,ndim
        do j=1,np
           if(x(j,idim)<0.5D0.or.x(j,idim)>5.5D0)then
              write(*,*)x(j,1:ndim)
           endif
        end do
     end do
     stop
  end if

  ! CIC at level ilevel (dd: right cloud boundary; dg: left cloud boundary)
  ! JMG -- droit and gauche?
  do idim=1,ndim
     do j=1,np
        dd(j,idim)=x(j,idim)+0.5D0
        id(j,idim)=dd(j,idim)
        dd(j,idim)=dd(j,idim)-id(j,idim)
        dg(j,idim)=1.0D0-dd(j,idim)
        ig(j,idim)=id(j,idim)-1
     end do
  end do

   ! Compute parent grids
  do idim=1,ndim
     do j=1,np
        igg(j,idim)=ig(j,idim)/2
        igd(j,idim)=id(j,idim)/2
     end do
  end do
#if NDIM==1
  do j=1,np
     kg(j,1)=1+igg(j,1)
     kg(j,2)=1+igd(j,1)
  end do
#endif
#if NDIM==2
  do j=1,np
     kg(j,1)=1+igg(j,1)+3*igg(j,2)
     kg(j,2)=1+igd(j,1)+3*igg(j,2)
     kg(j,3)=1+igg(j,1)+3*igd(j,2)
     kg(j,4)=1+igd(j,1)+3*igd(j,2)
  end do
#endif
#if NDIM==3
  do j=1,np
     kg(j,1)=1+igg(j,1)+3*igg(j,2)+9*igg(j,3)
     kg(j,2)=1+igd(j,1)+3*igg(j,2)+9*igg(j,3)
     kg(j,3)=1+igg(j,1)+3*igd(j,2)+9*igg(j,3)
     kg(j,4)=1+igd(j,1)+3*igd(j,2)+9*igg(j,3)
     kg(j,5)=1+igg(j,1)+3*igg(j,2)+9*igd(j,3)
     kg(j,6)=1+igd(j,1)+3*igg(j,2)+9*igd(j,3)
     kg(j,7)=1+igg(j,1)+3*igd(j,2)+9*igd(j,3)
     kg(j,8)=1+igd(j,1)+3*igd(j,2)+9*igd(j,3)
  end do
#endif
  do ind=1,twotondim
     do j=1,np
        igrid(j,ind)=son(nbors_father_cells(ind_grid_part(j),kg(j,ind)))
     end do
  end do

  ! Check if particles are entirely in level ilevel
  ok(1:np)=.true.
  do ind=1,twotondim
     do j=1,np
        ok(j)=ok(j).and.igrid(j,ind)>0
     end do
  end do

  ! If not, rescale position at level ilevel-1
  do idim=1,ndim
     do j=1,np
        if(.not.ok(j))then
           x(j,idim)=x(j,idim)/2.0D0
        end if
     end do
  end do
  ! If not, redo CIC at level ilevel-1
  do idim=1,ndim
     do j=1,np
        if(.not.ok(j))then
           dd(j,idim)=x(j,idim)+0.5D0
           id(j,idim)=dd(j,idim)
           dd(j,idim)=dd(j,idim)-id(j,idim)
           dg(j,idim)=1.0D0-dd(j,idim)
           ig(j,idim)=id(j,idim)-1
        end if
     end do
  end do

 ! Compute parent cell position
  do idim=1,ndim
     do j=1,np
        if(ok(j))then
           icg(j,idim)=ig(j,idim)-2*igg(j,idim)
           icd(j,idim)=id(j,idim)-2*igd(j,idim)
        else
           icg(j,idim)=ig(j,idim)
           icd(j,idim)=id(j,idim)
        end if
     end do
  end do
#if NDIM==1
  do j=1,np
     icell(j,1)=1+icg(j,1)
     icell(j,2)=1+icd(j,1)
  end do
#endif
#if NDIM==2
  do j=1,np
     if(ok(j))then
        icell(j,1)=1+icg(j,1)+2*icg(j,2)
        icell(j,2)=1+icd(j,1)+2*icg(j,2)
        icell(j,3)=1+icg(j,1)+2*icd(j,2)
        icell(j,4)=1+icd(j,1)+2*icd(j,2)
     else
        icell(j,1)=1+icg(j,1)+3*icg(j,2)
        icell(j,2)=1+icd(j,1)+3*icg(j,2)
        icell(j,3)=1+icg(j,1)+3*icd(j,2)
        icell(j,4)=1+icd(j,1)+3*icd(j,2)
     end if
  end do
#endif
#if NDIM==3
  do j=1,np
     if(ok(j))then
        icell(j,1)=1+icg(j,1)+2*icg(j,2)+4*icg(j,3)
        icell(j,2)=1+icd(j,1)+2*icg(j,2)+4*icg(j,3)
        icell(j,3)=1+icg(j,1)+2*icd(j,2)+4*icg(j,3)
        icell(j,4)=1+icd(j,1)+2*icd(j,2)+4*icg(j,3)
        icell(j,5)=1+icg(j,1)+2*icg(j,2)+4*icd(j,3)
        icell(j,6)=1+icd(j,1)+2*icg(j,2)+4*icd(j,3)
        icell(j,7)=1+icg(j,1)+2*icd(j,2)+4*icd(j,3)
        icell(j,8)=1+icd(j,1)+2*icd(j,2)+4*icd(j,3)
     else
        icell(j,1)=1+icg(j,1)+3*icg(j,2)+9*icg(j,3)
        icell(j,2)=1+icd(j,1)+3*icg(j,2)+9*icg(j,3)
        icell(j,3)=1+icg(j,1)+3*icd(j,2)+9*icg(j,3)
        icell(j,4)=1+icd(j,1)+3*icd(j,2)+9*icg(j,3)
        icell(j,5)=1+icg(j,1)+3*icg(j,2)+9*icd(j,3)
        icell(j,6)=1+icd(j,1)+3*icg(j,2)+9*icd(j,3)
        icell(j,7)=1+icg(j,1)+3*icd(j,2)+9*icd(j,3)
        icell(j,8)=1+icd(j,1)+3*icd(j,2)+9*icd(j,3)   
     end if
  end do
#endif
        
  ! Compute parent cell adresses
  do ind=1,twotondim
     do j=1,np
        if(ok(j))then
           indp(j,ind)=ncoarse+(icell(j,ind)-1)*ngridmax+igrid(j,ind)
        else
           indp(j,ind)=nbors_father_cells(ind_grid_part(j),icell(j,ind))
        end if
     end do
  end do

  ! Compute cloud volumes
#if NDIM==1
  do j=1,np
     vol(j,1)=dg(j,1)
     vol(j,2)=dd(j,1)
  end do
#endif
#if NDIM==2
  do j=1,np
     vol(j,1)=dg(j,1)*dg(j,2)
     vol(j,2)=dd(j,1)*dg(j,2)
     vol(j,3)=dg(j,1)*dd(j,2)
     vol(j,4)=dd(j,1)*dd(j,2)
  end do
#endif
#if NDIM==3
  do j=1,np
     vol(j,1)=dg(j,1)*dg(j,2)*dg(j,3)
     vol(j,2)=dd(j,1)*dg(j,2)*dg(j,3)
     vol(j,3)=dg(j,1)*dd(j,2)*dg(j,3)
     vol(j,4)=dd(j,1)*dd(j,2)*dg(j,3)
     vol(j,5)=dg(j,1)*dg(j,2)*dd(j,3)
     vol(j,6)=dd(j,1)*dg(j,2)*dd(j,3)
     vol(j,7)=dg(j,1)*dd(j,2)*dd(j,3)
     vol(j,8)=dd(j,1)*dd(j,2)*dd(j,3)
  end do
#endif

  dgas(1:np)=0.0D0  ! Gather gas density
  ugas(1:np)=0.0D0  ! Gather gas x-momentum (velocity ?)
  vgas(1:np)=0.0D0  ! Gather gas y-momentum (velocity ?)
  wgas(1:np)=0.0D0  ! Gather gas z-momentum (velocity ?)
  egas(1:np)=0.0D0  ! Gather gas thermal energy (specific ?)

  ! ROM to AJC: if you want, replace below vol(j,ind) by 1./twotondim
  do ind=1,twotondim
     do j=1,np
        d=uold(indp(j,ind),1)
        u=uold(indp(j,ind),2)/d
        v=uold(indp(j,ind),3)/d
        w=uold(indp(j,ind),4)/d
        e=uold(indp(j,ind),5)/d
#ifdef SOLVERmhd
        bx1=uold(indp(j,ind),6)
        by1=uold(indp(j,ind),7)
        bz1=uold(indp(j,ind),8)
        bx2=uold(indp(j,ind),nvar+1)
        by2=uold(indp(j,ind),nvar+2)
        bz2=uold(indp(j,ind),nvar+3)
        e=e-0.125d0*((bx1+bx2)**2+(by1+by2)**2+(bz1+bz2)**2)/d
#endif
        e=e-0.5*(u*u+v*v+w*w)
        dgas(j)=dgas(j)+d*vol(j,ind)
        ugas(j)=ugas(j)+d*u*vol(j,ind)
        vgas(j)=vgas(j)+d*v*vol(j,ind)
        wgas(j)=wgas(j)+d*w*vol(j,ind)
        egas(j)=egas(j)+d*e*vol(j,ind)
     end do
  end do

  do j=1,np
     isink=-idp(ind_part(j))
     r2=0d0
     do idim=1,ndim
        r2=r2+(xp(ind_part(j),idim)-xsink(isink,idim))**2
     end do
     weight=exp(-r2/r2k(isink))
!!!JMG
     ! Weight quantities by density as well as distance when
     ! removing gas from gas cells (via accretion).
     if(mass_weight_acc) accweight(isink)=accweight(isink) + weight*dgas(j)
!!!end JMG
     wden(isink)=wden(isink)+weight*dgas(j)
     wmom(isink,1)=wmom(isink,1)+weight*ugas(j)
     wmom(isink,2)=wmom(isink,2)+weight*vgas(j)
     wmom(isink,3)=wmom(isink,3)+weight*wgas(j)
     weth(isink)=weth(isink)+weight*egas(j)
     wvol(isink)=wvol(isink)+weight
  end do

end subroutine bondi_average
!################################################################
!################################################################
!################################################################
!################################################################
subroutine grow_bondi(ilevel)
  use pm_commons
  use amr_commons
  use hydro_commons
  implicit none
#ifndef WITHOUTMPI
  include 'mpif.h'
#endif
  integer::ilevel
  !------------------------------------------------------------------------
  ! This routine performs Bondi-Hoyle accretion of the gas onto 
  ! sink particles. On exit, sink mass and velocity are modified.
  ! Method is to loop over all particles to find the sinks, then 
  ! call "accrete_bondi" for each sink.
  !------------------------------------------------------------------------
  integer::igrid,jgrid,ipart,jpart,next_part,idim,info,iskip,ind
  integer::i,ig,ip,npart1,npart2,icpu,nx_loc,isink
  integer,dimension(1:nvector),save::ind_grid,ind_part,ind_grid_part
  real(dp)::r2,v2,c2,density,volume,ethermal,dx_min,scale
  real(dp),dimension(1:3)::velocity

  if(numbtot(1,ilevel)==0)return
  if(verbose)write(*,111)ilevel

  ! Compute sink accretion rates
!!!JMG  call compute_accretion_rate(0)
!!! The argument (-1 vs. 0) ONLY affects whether we print out accretion rate data
  call compute_accretion_rate(-1)

  ! Reset new sink variables                                                                        
  msink_new=0d0; vsink_new=0d0; delta_mass_new=0d0

  ! Should we actually do the accretion?
  if(do_accretion .eqv.  .false.) return

  ! Store initial gas density in unew(:,1)
  do ind=1,twotondim
     iskip=ncoarse+(ind-1)*ngridmax
     do i=1,active(ilevel)%ngrid
        unew(active(ilevel)%igrid(i)+iskip,1) = uold(active(ilevel)%igrid(i)+iskip,1)
     enddo
  enddo
     
  ! Loop over cpus
  do icpu=1,ncpu
     igrid=headl(icpu,ilevel)
     ig=0
     ip=0
     ! Loop over grids
     do jgrid=1,numbl(icpu,ilevel)
        npart1=numbp(igrid)  ! Number of particles in the grid
        npart2=0
        
        ! Count sink and cloud particles
        if(npart1>0)then
           ipart=headp(igrid)
           ! Loop over particles
           do jpart=1,npart1
              ! Save next particle   <--- Very important !!!
              next_part=nextp(ipart)
              if(idp(ipart).lt.0 .and. idp(ipart) .gt. -1*nsinkmax)then
                 npart2=npart2+1
              endif

              ipart=next_part  ! Go to next particle
           end do
        endif
        
        ! Gather sink and cloud particles
        if(npart2>0)then        
           ig=ig+1
           ind_grid(ig)=igrid
           ipart=headp(igrid)
           ! Loop over particles
           do jpart=1,npart1
              ! Save next particle   <--- Very important !!!
              next_part=nextp(ipart)
              ! Select only sink particles
              if(idp(ipart).lt.0 .and. idp(ipart) .gt. -1*nsinkmax)then
                 if(ig==0)then
                    ig=1
                    ind_grid(ig)=igrid
                 end if
                 ip=ip+1
                 ind_part(ip)=ipart
                 ind_grid_part(ip)=ig   
              endif
              if(ip==nvector)then
                 call accrete_bondi(ind_grid,ind_part,ind_grid_part,ig,ip,ilevel)
                 ip=0
                 ig=0
              end if
              ipart=next_part  ! Go to next particle
           end do
           ! End loop over particles
        end if

        igrid=next(igrid)   ! Go to next grid
     end do

     ! End loop over grids
     if(ip>0)call accrete_bondi(ind_grid,ind_part,ind_grid_part,ig,ip,ilevel)

  end do
  ! End loop over cpus

  if(nsink>0)then
#ifndef WITHOUTMPI
     call MPI_ALLREDUCE(msink_new,msink_all,nsinkmax,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
     call MPI_ALLREDUCE(delta_mass_new,delta_mass_all,nsinkmax,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
     call MPI_ALLREDUCE(vsink_new,vsink_all,nsinkmax*ndim,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
#else
     msink_all=msink_new
     delta_mass_all=delta_mass_new
     vsink_all=vsink_new
#endif
  endif
  do isink=1,nsink
     if(msink_dynamical > 0.0) then
        vsink(isink,1:ndim)=vsink(isink,1:ndim)*msink_dynamical+vsink_all(isink,1:ndim)
     else
        if(msink_pseudo_dyn > 0.0) then
           vsink(isink,1:ndim)=vsink(isink,1:ndim)*msink_pseudo_dyn+vsink_all(isink,1:ndim)
        else  !! NORMAL BEHAVIOR
           vsink(isink,1:ndim)=vsink(isink,1:ndim)*msink(isink)+vsink_all(isink,1:ndim)
        endif
     endif
!!! JMG
     if(drag_simple) then
        ! dv_drag here is actually acceleration, dv/dt_drag.  Need to multiply by timestep.
        vsink(isink,1:ndim)=vsink(isink,1:ndim) + (dv_drag(isink,1:ndim) * dtnew(ilevel)) * msink(isink)
     end if
!!! end JMG

     msink(isink)=msink(isink)+msink_all(isink)


!!! JMG
     if(fix_msink) then
        msink(isink) = msink_fix / (1e9)
     endif
!!!JMG

     delta_mass(isink)=delta_mass(isink)+delta_mass_all(isink)
     if(msink_dynamical > 0.0) then
        vsink(isink,1:ndim)=vsink(isink,1:ndim)/msink_dynamical
     else
        if(msink_pseudo_dyn > 0.0) then
           vsink(isink,1:ndim)=vsink(isink,1:ndim)/msink_pseudo_dyn
        else !! NORMAL BEHAVIOR
           vsink(isink,1:ndim)=vsink(isink,1:ndim)/msink(isink)
        endif
     endif
     if(static_sink) vsink(isink,1:ndim) = 0.0
  end do

  if(verbose_agn .and. myid == 1) write(*,*) "done grow_bondi", msink(1), msink_all(1), delta_mass(1), delta_mass_all(1), dtnew(ilevel)

111 format('   Entering grow_bondi for level ',I2)

contains
  ! Routine to return alpha, defined as rho/rho_inf, for a critical
  ! Bondi accretion solution. The argument is x = r / r_Bondi.
  ! This is from Krumholz et al. (AJC)
  REAL(dp) function bondi_alpha(x)
    implicit none
    REAL(dp) x
    REAL(dp), PARAMETER :: XMIN=0.01, xMAX=2.0
    INTEGER, PARAMETER :: NTABLE=51
    REAL(dp) lambda_c, xtable, xtablep1, alpha_exp
    integer idx
    !     Table of alpha values. These correspond to x values that run from
    !     0.01 to 2.0 with uniform logarithmic spacing. The reason for
    !     this choice of range is that the asymptotic expressions are
    !     accurate to better than 2% outside this range.
    REAL(dp), PARAMETER, DIMENSION(NTABLE) :: alphatable = (/ &
         820.254, 701.882, 600.752, 514.341, 440.497, 377.381, 323.427, &
         277.295, 237.845, 204.1, 175.23, 150.524, 129.377, 111.27, 95.7613, &
         82.4745, 71.0869, 61.3237, 52.9498, 45.7644, 39.5963, 34.2989, &
         29.7471, 25.8338, 22.4676, 19.5705, 17.0755, 14.9254, 13.0714, &
         11.4717, 10.0903, 8.89675, 7.86467, 6.97159, 6.19825, 5.52812, &
         4.94699, 4.44279, 4.00497, 3.6246, 3.29395, 3.00637, 2.75612, &
         2.53827, 2.34854, 2.18322, 2.03912, 1.91344, 1.80378, 1.70804, &
         1.62439 /)
    !     Define a constant that appears in these formulae
    lambda_c    = 0.25 * exp(1.5) 
    !     Deal with the off-the-table cases 
    if (x .le. XMIN) then
       bondi_alpha = lambda_c / sqrt(2. * x**3)
    else if (x .ge. XMAX) then 
       bondi_alpha = exp(1./x) 
    else
       !     We are on the table
       idx = floor ((NTABLE-1) * log(x/XMIN) / log(XMAX/XMIN))
       xtable = exp(log(XMIN) + idx*log(XMAX/XMIN)/(NTABLE-1))
       xtablep1 = exp(log(XMIN) + (idx+1)*log(XMAX/XMIN)/(NTABLE-1))
       alpha_exp = log(x/xtable) / log(xtablep1/xtable)
       !     Note the extra +1s below because of fortran 1 offset arrays
       bondi_alpha = alphatable(idx+1) * (alphatable(idx+2)/alphatable(idx+1))**alpha_exp
    end if
  end function bondi_alpha
end subroutine grow_bondi
!################################################################
!################################################################
!################################################################
!################################################################
subroutine accrete_bondi(ind_grid,ind_part,ind_grid_part,ng,np,ilevel)
  use amr_commons
  use pm_commons
  use hydro_commons
  implicit none
  integer::ng,np,ilevel
  integer,dimension(1:nvector)::ind_grid
  integer,dimension(1:nvector)::ind_grid_part,ind_part
  !-----------------------------------------------------------------------
  ! This routine is called by subroutine bondi_hoyle.
  ! It actually removes mass from gas cells in which
  ! the sink "cloud" particles lie.  dMBHoverdt is used
  ! as the accretion rate, but this number should be
  ! capped by the Eddington limit in function
  ! "compute_accretion_rate()."  Method is to loop over
  ! the cloud particles provided in the arguments,
  ! find each particle's parent grid cell, and remove
  ! mass from that cell according to a weighting scheme.
  !-----------------------------------------------------------------------
  integer::i,j,idim,nx_loc,isink,ivar
  real(dp)::xxx,mmm,r2,v2,c2,d,u,v,w,e,bx1,bx2,by1,by2,bz1,bz2,dini
  real(dp),dimension(1:nvar)::z
  real(dp)::scale_nH,scale_T2,scale_l,scale_d,scale_t,scale_v,scale_m
  real(dp)::dx,dx_loc,scale,vol_loc,weight,acc_mass,temp
  logical::error
  ! Grid based arrays
  real(dp),dimension(1:nvector,1:ndim),save::x0
  integer ,dimension(1:nvector),save::ind_cell
  integer ,dimension(1:nvector,1:threetondim),save::nbors_father_cells
  integer ,dimension(1:nvector,1:twotondim),save::nbors_father_grids
  ! Particle based arrays
  integer,dimension(1:nvector),save::igrid_son,ind_son
  integer,dimension(1:nvector),save::list1
  logical,dimension(1:nvector),save::ok
  real(dp),dimension(1:nvector),save::meff
  real(dp),dimension(1:nvector,1:ndim),save::x
  integer ,dimension(1:nvector,1:ndim),save::id,igd,icd
  integer ,dimension(1:nvector),save::igrid,icell,indp,kg
  real(dp),dimension(1:3)::skip_loc

!!!JMG
  real(dp) :: weight_sum
  weight_sum = 0.0

  if(verbose) WRITE(*,*) "Entering accrete_bondi()", myid, ilevel

  ! Conversion factor from user units to cgs units
  call units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)
  scale_m=scale_d*scale_l**3

  ! Mesh spacing in that level
  dx=0.5D0**ilevel 
  nx_loc=(icoarse_max-icoarse_min+1)
  skip_loc=(/0.0d0,0.0d0,0.0d0/)
  if(ndim>0)skip_loc(1)=dble(icoarse_min)
  if(ndim>1)skip_loc(2)=dble(jcoarse_min)
  if(ndim>2)skip_loc(3)=dble(kcoarse_min)
  scale=boxlen/dble(nx_loc)
  dx_loc=dx*scale
  vol_loc=dx_loc**ndim

  ! Lower left corner of 3x3x3 grid-cube
  do idim=1,ndim
     do i=1,ng
        x0(i,idim)=xg(ind_grid(i),idim)-3.0D0*dx
     end do
  end do

  ! Gather 27 neighboring father cells (should be present anytime !)
  do i=1,ng
     ind_cell(i)=father(ind_grid(i))
  end do
  call get3cubefather(ind_cell,nbors_father_cells,nbors_father_grids,ng,ilevel)

  ! Rescale position at level ilevel
  do idim=1,ndim
     do j=1,np
        x(j,idim)=xp(ind_part(j),idim)/scale+skip_loc(idim)
     end do
  end do
  do idim=1,ndim
     do j=1,np
        x(j,idim)=x(j,idim)-x0(ind_grid_part(j),idim)
     end do
  end do
  do idim=1,ndim
     do j=1,np
        x(j,idim)=x(j,idim)/dx
     end do
  end do

  ! Check for illegal moves
  error=.false.
  do idim=1,ndim
     do j=1,np
        if(x(j,idim)<=0.0D0.or.x(j,idim)>=6.0D0)error=.true.
     end do
  end do
  if(error)then
     write(*,*)'problem in accrete_bondi'
     write(*,*)ilevel,ng,np
     stop
  end if

  ! NGP at level ilevel
  do idim=1,ndim
     do j=1,np
        id(j,idim)=x(j,idim)
     end do
  end do

   ! Compute parent grids
  do idim=1,ndim
     do j=1,np
        igd(j,idim)=id(j,idim)/2
     end do
  end do
#if NDIM==1
  do j=1,np
     kg(j)=1+igd(j,1)
  end do
#endif
#if NDIM==2
  do j=1,np
     kg(j)=1+igd(j,1)+3*igd(j,2)
  end do
#endif
#if NDIM==3
  do j=1,np
     kg(j)=1+igd(j,1)+3*igd(j,2)+9*igd(j,3)
  end do
#endif
  do j=1,np
     igrid(j)=son(nbors_father_cells(ind_grid_part(j),kg(j)))
  end do

  ! Check if particles are entirely in level ilevel
  ok(1:np)=.true.
  do j=1,np
     ok(j)=ok(j).and.igrid(j)>0
  end do

  ! Compute parent cell position
  do idim=1,ndim
     do j=1,np
        if(ok(j))then
           icd(j,idim)=id(j,idim)-2*igd(j,idim)
        end if
     end do
  end do
#if NDIM==1
  do j=1,np
     if(ok(j))then
        icell(j)=1+icd(j,1)
     end if
  end do
#endif
#if NDIM==2
  do j=1,np
     if(ok(j))then
        icell(j)=1+icd(j,1)+2*icd(j,2)
     end if
  end do
#endif
#if NDIM==3
  do j=1,np
     if(ok(j))then
        icell(j)=1+icd(j,1)+2*icd(j,2)+4*icd(j,3)
     end if
  end do
#endif
        
  ! Compute parent cell adress
  do j=1,np
     if(ok(j))then
        indp(j)=ncoarse+(icell(j)-1)*ngridmax+igrid(j)
     end if
  end do

  ! Remove mass from hydro cells
  do j=1,np
     if(ok(j))then
        isink=-idp(ind_part(j))
        r2=0d0
        do idim=1,ndim
           r2=r2+(xp(ind_part(j),idim)-xsink(isink,idim))**2
        end do
        weight=exp(-r2/r2k(isink))

!!!JMG
        weight_sum = weight_sum + weight
           
        d=uold(indp(j),1)
        dini=unew(indp(j),1) ! Initial density
        u=uold(indp(j),2)/d
        v=uold(indp(j),3)/d
        w=uold(indp(j),4)/d
        e=uold(indp(j),5)/d
#ifdef SOLVERmhd
        bx1=uold(indp(j),6)
        by1=uold(indp(j),7)
        bz1=uold(indp(j),8)
        bx2=uold(indp(j),nvar+1)
        by2=uold(indp(j),nvar+2)
        bz2=uold(indp(j),nvar+3)
        e=e-0.125d0*((bx1+bx2)**2+(by1+by2)**2+(bz1+bz2)**2)/d
#endif
        do ivar=imetal,nvar
           z(ivar)=uold(indp(j),ivar)/d
        end do

        ! Compute accreted mass with cloud weighting
        acc_mass=dMBHoverdt(isink)*weight/total_volume(isink)*dtnew(ilevel)

!!!JMG
        ! accretion weighted by the mass of gas in the cell
        ! re-calculate acc_mass with a new weighting
        if(mass_weight_acc) then
           weight = weight * dini
           acc_mass=dMBHoverdt(isink)*weight/total_accweight(isink)*dtnew(ilevel)
        endif
!!! end JMG

        ! JMG 
        ! Do angular momentum correction.  Calculate Keplerian 
        ! trajectory of gas "sampled" by this "cloud" particle.
        ! If it passes within dx/4 of the sink particle, then 
        ! accretion happens as normal.  If not then the accretion
        ! associated with this cloud particle is ignored.
#ifdef NEW_CODE
        if(correct_bondi_angmom) then
           rmin = get_rmin_for_bondi_correction(ind_part(j), isink)
           if( rmin > ) then
              acc_mass = 0.0
           endif
        endif
#endif

        ! Added by JMG Feb 2012 so that acc_mass can never be NaN
        if(total_volume(isink)==0) acc_mass = 0

        ! Cannot accrete more than 25% of initial gass mass in the cell
!!!JMG
        if(verbose_agn) then
           if(acc_mass > (d-0.75*dini)*vol_loc) &
                write(*,*) ".....Reducing accretion ", acc_mass, (d-0.75*dini)*vol_loc
        endif
!!! end JMG
        acc_mass=max(min(acc_mass,(d-0.75*dini)*vol_loc),0.0_dp)
        msink_new(isink)=msink_new(isink)+acc_mass
        delta_mass_new(isink)=delta_mass_new(isink)+acc_mass
        vsink_new(isink,1)=vsink_new(isink,1)+acc_mass*u
        vsink_new(isink,2)=vsink_new(isink,2)+acc_mass*v
        vsink_new(isink,3)=vsink_new(isink,3)+acc_mass*w
        ! Remove accreted mass
        d=d-acc_mass/vol_loc


#ifdef SOLVERmhd
        e=e+0.125d0*((bx1+bx2)**2+(by1+by2)**2+(bz1+bz2)**2)/d
#endif
        uold(indp(j),1)=d
        uold(indp(j),2)=d*u
        uold(indp(j),3)=d*v
        uold(indp(j),4)=d*w
        uold(indp(j),5)=d*e
        do ivar=imetal,nvar
           uold(indp(j),ivar)=d*z(ivar)
        end do
     endif

  end do

  if(verbose_agn) WRITE(*,'(a,3i6, 5ES17.6)') "accrete_bondi", myid, np, ilevel, &
       weight_sum , &
       total_volume(1), &
       dMBHoverdt(1)*scale_m/scale_t/(1.9891d33/(365.*24.*3600.)), &
       dtnew(ilevel) * scale_t / (365.*24.*3600.), &
       msink_new(1)


end subroutine accrete_bondi
!################################################################
!################################################################
!################################################################
!################################################################
subroutine compute_accretion_rate(ilevel)
  use pm_commons
  use amr_commons
  use hydro_commons
  implicit none
#ifndef WITHOUTMPI
  include 'mpif.h'
#endif
  integer::ilevel
  !------------------------------------------------------------------------
  ! This routine computes the accretion rate on the sink particles.
  !
  ! Here, ilevel is used only as a flag to tell us whether to print
  ! out some sink particle info.
  !------------------------------------------------------------------------
  integer::igrid,jgrid,ipart,jpart,next_part,idim,info,iskip,ind
  integer::i,ig,ip,npart1,npart2,icpu,nx_loc,isink
  real(dp)::scale_nH,scale_T2,scale_l,scale_d,scale_t,scale_v,scale_m
  real(dp)::factG,d_star,boost,vel_max
  real(dp)::r2,v2,c2,density,volume,ethermal,dx_min,scale
  real(dp),dimension(1:3)::velocity

  real(dp) :: mach, Fdrag, factor
  integer :: kdim

  ! Gravitational constant
  factG=1d0
  if(cosmo)factG=3d0/8d0/3.1415926*omega_m*aexp

  ! Conversion factor from user units to cgs units
  call units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)
  scale_m=scale_d*scale_l**3d0
  nx_loc=(icoarse_max-icoarse_min+1)
  scale=boxlen/dble(nx_loc)
!  dx_min=scale*0.5D0**nlevelmax/aexp
  dx_min=scale*0.5D0**level_agn/aexp
  d_star=n_star/scale_nH

  ! JMG -- not sure why vel_max is so low...

   ! Maximum relative velocity
  vel_max=100. ! in km/sec --> differs from Kintherm and Kintherm_movie versions
!!!JMG  vel_max=10. ! in km/sec 
  vel_max=vel_max*1d5/scale_v
 
  ! added by JMG 27 Feb 2012
  ! Previously, if smbh=.false., then dMBHoverdt would contain garbage.
  ! And dMBHoverdt is used for bondi accretion whether or not smbh=.true.
  ! I'm not sure what purpose "smbh" serves here
  dMBHoverdt = 0_dp
  dMEDoverdt = 0_dp
  total_volume = 0_dp

!!!  if(smbh)then  JMG
  if(bondi) then

  ! Compute sink particle accretion rate
  do isink=1,nsink
     density=0d0
     volume=0d0
     velocity=0d0
     ethermal=0d0
     if(mass_weight_acc) total_accweight(isink)=0d0
     ! Loop over level: sink cloud can overlap several levels
     do i=levelmin,nlevelmax
        density=density+weighted_density(isink,i)
        ethermal=ethermal+weighted_ethermal(isink,i)
        velocity(1)=velocity(1)+weighted_momentum(isink,i,1)
        velocity(2)=velocity(2)+weighted_momentum(isink,i,2)
        velocity(3)=velocity(3)+weighted_momentum(isink,i,3)
        volume=volume+weighted_volume(isink,i)
        if(mass_weight_acc) total_accweight(isink) = &
             & total_accweight(isink)+weighted_accweight(isink,i)
     end do
     density=density/volume
     velocity(1:3)=velocity(1:3)/density/volume
     ethermal=ethermal/density/volume
     total_volume(isink)=volume
!!!     if(mass_weight_acc) total_accweight(isink) = accretion_weight
     c2=MAX(gamma*(gamma-1.0)*ethermal,smallc**2)
     v2=min(SUM((velocity(1:3)-vsink(isink,1:3))**2),vel_max**2)
     r2=(factG*msink(isink)/(c2+v2))**2

     ! Compute Bondi-Hoyle accretion rate in code units
     boost=1.0
! JMG     if(star)boost=max((density/d_star)**2,1.0_dp)
     if(star)boost=max((density/d_star)**beta_accretion, 1.0_dp)
!     dMBHoverdt(isink)=boost*4.*3.1415926*density*r2*sqrt(1.12**2*c2+v2)/bondi_alpha(1.2*dx_min/sqrt(r2))
     dMBHoverdt(isink)=boost*4.*3.1415926*density*r2*sqrt(c2+v2)
     
     ! Compute Eddington accretion rate in code units
     dMEDoverdt(isink)=4.*3.1415926*6.67d-8*msink(isink)*1.66d-24/(0.1*6.652d-25*3d10)*scale_t

     ! Compute BH radius of influence in kpc
     rBH(isink)=250.*(msink(isink)*scale_m/(5d9*2d33))**(0.5)

     ! Compute BH radius of influence in code units
     epsBH(isink)=max(4.*dx_min,rBH(isink)*3.08d21/scale_l)

!!!JMG
     ! calculate a hydrodynamic drag force applied to the BH
     if(drag_simple) then
        v2 = (SUM((velocity(1:3)-vsink(isink,1:3))**2))
        mach = sqrt( v2 / c2)
        ! see Damien Chapon's thesis and Ostriker 99.  Below is kluge to fit the formula.
        if(mach <= 1.0) factor = 0.5 * log( (1.0+mach)/(1.0-mach)) - mach
        if(mach > 1.0 .and. mach <2.0) factor = -1.25*(mach - 1.0) + 2.0
        if(mach > 2.0) factor = -0.125 * (mach - 2.0) + 0.75
        !!   factor = 0.5 * log( (1.0+mach)/(1.0-mach)) + log( (mach - 1.0) / 3.5)
        if(factor > 2.0) factor=2.0
        if(factor < 0.25) factor = 0.25

        do kdim = 1, ndim
           ! Drag force in the direction denoted by kdim.  The second line just defines
           ! the direction.  The direction of the force should be opposite the direction
           ! of motion, i.e. in the same direction as the gas relative to the sink.
           Fdrag = factor * 4.0*3.14159 * density * (factG * msink(isink))**2 / c2  * &
                (velocity(kdim)-vsink(isink,kdim)) / sqrt(v2)
!!!!if(myid == 1)           write(*,*) ilevel, isink, kdim, Fdrag, factor, density, factG, msink(isink), sqrt(c2)*scale_v/1e5, (velocity(kdim)-vsink(isink,kdim))*scale_v/1e5, sqrt(v2)*scale_v/1e5
           ! this is actually the acceleration.  Need to multiply by timestep to get dv.
           dv_drag(isink, kdim) = Fdrag / msink(isink)                 !!!* dtnew(ilevel)
!!$        enddo
        enddo
     endif

     ! print out quantities that go into bondi calculation
     if(myid==1.and.ilevel==levelmin) then
        write(*,"(A, I4, 3F13.5)") "bondi params", isink, &
             density*scale_nH, sqrt(c2)*scale_v/1e5, sqrt(v2)*scale_v/1e5
        if(drag_simple) &
             write(*,*) "dv_drag:", dv_drag(isink, 1:3) * scale_v/1e5 / scale_t, factor, mach, msink(isink), velocity(1) - vsink(isink,1)
     endif
!!!JMG end
  end do  ! loop over sinks

  if(myid==1.and.ilevel==levelmin.and.nsink>0)then
     do i=1,nsink
        xmsink(i)=msink(i)
     end do
     call quick_sort(xmsink(1),idsink_sort(1),nsink)
     write(*,*)'Number of sink = ',nsink
     write(*,'(" ============================================================================================")')
     write(*,'(" Id     Mass(Msol)    Bondi(Msol/yr)   Edd(Msol/yr)              x              y              z")')
     write(*,'(" ============================================================================================")')
     do i=nsink,max(nsink-10,1),-1
        isink=idsink_sort(i)
        write(*,'(I3, 1(1X,1PE17.10), 10(1X,1PE14.7))')idsink(isink),msink(isink)*scale_m/2d33 &
             & ,dMBHoverdt(isink)*scale_m/scale_t/(2d33/(365.*24.*3600.)) &
             & ,dMEDoverdt(isink)*scale_m/scale_t/(2d33/(365.*24.*3600.)) &
             & ,xsink(isink,1:ndim), delta_mass(isink)*scale_m/2d33
     end do
     write(*,'(" ============================================================================================")')
  endif

!!!JMG
! print out BH stuff at every fine timestep
  if(verbose_agn) then 
     if(myid==1 .and. ilevel==-1 .and. nsink>0) then
        do isink=1,nsink
           write(*,'("BH", I3, 1(1X,1PE17.10), 10(1X,1PE14.7))')idsink(isink),msink(isink)*scale_m/2d33 &
                & ,dMBHoverdt(isink)*scale_m/scale_t/(2d33/(365.*24.*3600.)) &
                & ,dMEDoverdt(isink)*scale_m/scale_t/(2d33/(365.*24.*3600.)) &
                & ,xsink(isink,1:ndim), delta_mass(isink)*scale_m/2d33
        end do
     endif
  endif
!!! end JMG

  ! Take the minimum accretion rate
  do isink=1,nsink
     dMBHoverdt(isink)=min(dMBHoverdt(isink),dMEDoverdt(isink))
  end do

  else

     if(myid==1.and.ilevel==levelmin.and.nsink>0)then
        do i=1,nsink
           xmsink(i)=msink(i)
        end do
        call quick_sort(xmsink(1),idsink_sort(1),nsink)
        write(*,*)'Number of sink = ',nsink
        write(*,'(" ============================================================================================")')
        write(*,'(" Id     Mass(Msol)          x              y              z")')
        write(*,'(" ============================================================================================")')
        do i=nsink,max(nsink-10,1),-1
           isink=idsink_sort(i)
           write(*,'(I3,10(1X,1PE14.7))')idsink(isink),msink(isink)*scale_m/2d33,xsink(isink,1:ndim)
        end do
        write(*,'(" ============================================================================================")')
     endif
     
  endif  ! endelse ( if(smbh))

end subroutine compute_accretion_rate
!################################################################
!################################################################
!################################################################
!################################################################
subroutine grow_jeans(ilevel)
  use pm_commons
  use amr_commons
  implicit none
#ifndef WITHOUTMPI
  include 'mpif.h'
#endif
  integer::ilevel
  !------------------------------------------------------------------------
  ! This routine determines if a cell covered by an old sink particle
  ! cross the density threshold for sink particle formation.If so, the 
  ! density is reduced and the corresponding mass is given to the sink.
  ! On exit, sink mass and velocity are modified.
  !------------------------------------------------------------------------
  integer::igrid,jgrid,ipart,jpart,next_part,idim,info
  integer::i,ig,ip,npart1,npart2,icpu,nx_loc,isink
  integer,dimension(1:nvector),save::ind_grid,ind_part,ind_grid_part
  real(dp)::r2,density,volume

  if(numbtot(1,ilevel)==0)return
  if(verbose)write(*,111)ilevel

  ! Reset new sink variables
  msink_new=0d0; vsink_new=0d0

  ! Should we actually do the accretion?
  if(do_accretion .eqv. .false.) return

  ! Loop over cpus
  do icpu=1,ncpu
     igrid=headl(icpu,ilevel)
     ig=0
     ip=0
     ! Loop over grids
     do jgrid=1,numbl(icpu,ilevel)
        npart1=numbp(igrid)  ! Number of particles in the grid
        npart2=0
        
        ! Count sink and cloud particles
        if(npart1>0)then
           ipart=headp(igrid)
           ! Loop over particles
           do jpart=1,npart1
              ! Save next particle   <--- Very important !!!
              next_part=nextp(ipart)
              if(idp(ipart).lt.0 .and. idp(ipart) .gt. -1*nsinkmax)then
                 npart2=npart2+1
              endif

              ipart=next_part  ! Go to next particle
           end do
        endif
        
        ! Gather sink and cloud particles
        if(npart2>0)then        
           ig=ig+1
           ind_grid(ig)=igrid
           ipart=headp(igrid)
           ! Loop over particles
           do jpart=1,npart1
              ! Save next particle   <--- Very important !!!
              next_part=nextp(ipart)
              ! Select only sink particles
              if(idp(ipart).lt.0 .and. idp(ipart) .gt. -1*nsinkmax)then
                 if(ig==0)then
                    ig=1
                    ind_grid(ig)=igrid
                 end if
                 ip=ip+1
                 ind_part(ip)=ipart
                 ind_grid_part(ip)=ig   
              endif
              
              if(ip==nvector)then
                 call accrete_jeans(ind_grid,ind_part,ind_grid_part,ig,ip,ilevel)
                 ip=0
                 ig=0
              end if
              ipart=next_part  ! Go to next particle
           end do
           ! End loop over particles
        end if

        igrid=next(igrid)   ! Go to next grid
     end do

     ! End loop over grids
     if(ip>0)call accrete_jeans(ind_grid,ind_part,ind_grid_part,ig,ip,ilevel)
  end do 
  ! End loop over cpus

  if(nsink>0)then
#ifndef WITHOUTMPI
     call MPI_ALLREDUCE(msink_new,msink_all,nsinkmax     ,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
     call MPI_ALLREDUCE(vsink_new,vsink_all,nsinkmax*ndim,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
#else
     msink_all=msink_new
     vsink_all=vsink_new
#endif
  endif
  do isink=1,nsink
     vsink(isink,1)=vsink(isink,1)*msink(isink)+vsink_all(isink,1)
     vsink(isink,2)=vsink(isink,2)*msink(isink)+vsink_all(isink,2)
     vsink(isink,3)=vsink(isink,3)*msink(isink)+vsink_all(isink,3)
     msink(isink)=msink(isink)+msink_all(isink)
     vsink(isink,1:ndim)=vsink(isink,1:ndim)/msink(isink)
     if(static_sink) vsink(isink,1:ndim) = 0.0
  end do

111 format('   Entering grow_jeans for level ',I2)

end subroutine grow_jeans
!################################################################
!################################################################
!################################################################
!################################################################
subroutine accrete_jeans(ind_grid,ind_part,ind_grid_part,ng,np,ilevel)
  use amr_commons
  use pm_commons
  use hydro_commons
  implicit none
  integer::ng,np,ilevel
  integer,dimension(1:nvector)::ind_grid
  integer,dimension(1:nvector)::ind_grid_part,ind_part
  !-----------------------------------------------------------------------
  ! This routine is called by subroutine bondi_hoyle.
  !-----------------------------------------------------------------------
  integer::i,j,idim,nx_loc,isink,ivar
  real(dp)::scale_nH,scale_T2,scale_l,scale_d,scale_t,scale_v,factG
  real(dp)::xxx,mmm,r2,v2,c2,d,u,v=0d0,w=0d0,e,bx1,bx2,by1,by2,bz1,bz2
  real(dp),dimension(1:nvar)::z
  real(dp)::dx,dx_loc,scale,vol_loc,temp,d_jeans,acc_mass,d_sink,d_thres
  logical::error
  ! Grid based arrays
  real(dp),dimension(1:nvector,1:ndim),save::x0
  integer ,dimension(1:nvector),save::ind_cell
  integer ,dimension(1:nvector,1:threetondim),save::nbors_father_cells
  integer ,dimension(1:nvector,1:twotondim),save::nbors_father_grids
  ! Particle based arrays
  integer,dimension(1:nvector),save::igrid_son,ind_son
  integer,dimension(1:nvector),save::list1
  logical,dimension(1:nvector),save::ok
  real(dp),dimension(1:nvector,1:ndim),save::x
  integer ,dimension(1:nvector,1:ndim),save::id,igd,icd
  integer ,dimension(1:nvector),save::igrid,icell,indp,kg
  real(dp),dimension(1:3)::skip_loc

  ! Conversion factor from user units to cgs units
  call units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)

  ! Gravitational constant
  factG=1d0
  if(cosmo)factG=3d0/8d0/3.1415926*omega_m*aexp

  ! Mesh spacing in that level
  dx=0.5D0**ilevel 
  nx_loc=(icoarse_max-icoarse_min+1)
  skip_loc=(/0.0d0,0.0d0,0.0d0/)
  if(ndim>0)skip_loc(1)=dble(icoarse_min)
  if(ndim>1)skip_loc(2)=dble(jcoarse_min)
  if(ndim>2)skip_loc(3)=dble(kcoarse_min)
  scale=boxlen/dble(nx_loc)
  dx_loc=dx*scale
  vol_loc=dx_loc**ndim

  ! Density threshold for sink particle formation
  d_sink=n_sink/scale_nH

#if NDIM==3
  ! Lower left corner of 3x3x3 grid-cube
  do idim=1,ndim
     do i=1,ng
        x0(i,idim)=xg(ind_grid(i),idim)-3.0D0*dx
     end do
  end do

  ! Gather 27 neighboring father cells (should be present anytime !)
  do i=1,ng
     ind_cell(i)=father(ind_grid(i))
  end do
  call get3cubefather(ind_cell,nbors_father_cells,nbors_father_grids,ng,ilevel)

  ! Rescale position at level ilevel
  do idim=1,ndim
     do j=1,np
        x(j,idim)=xp(ind_part(j),idim)/scale+skip_loc(idim)
     end do
  end do
  do idim=1,ndim
     do j=1,np
        x(j,idim)=x(j,idim)-x0(ind_grid_part(j),idim)
     end do
  end do
  do idim=1,ndim
     do j=1,np
        x(j,idim)=x(j,idim)/dx
     end do
  end do

  ! Check for illegal moves
  error=.false.
  do idim=1,ndim
     do j=1,np
        if(x(j,idim)<=0.0D0.or.x(j,idim)>=6.0D0)error=.true.
     end do
  end do
  if(error)then
     write(*,*)'problem in accrete_jeans'
     write(*,*)ilevel,ng,np
     stop
  end if

  ! NGP at level ilevel
  do idim=1,ndim
     do j=1,np
        id(j,idim)=x(j,idim)
     end do
  end do

   ! Compute parent grids
  do idim=1,ndim
     do j=1,np
        igd(j,idim)=id(j,idim)/2
     end do
  end do
#if NDIM==1
  do j=1,np
     kg(j)=1+igd(j,1)
  end do
#endif
#if NDIM==2
  do j=1,np
     kg(j)=1+igd(j,1)+3*igd(j,2)
  end do
#endif
#if NDIM==3
  do j=1,np
     kg(j)=1+igd(j,1)+3*igd(j,2)+9*igd(j,3)
  end do
#endif
  do j=1,np
     igrid(j)=son(nbors_father_cells(ind_grid_part(j),kg(j)))
  end do

  ! Check if particles are entirely in level ilevel
  ok(1:np)=.true.
  do j=1,np
     ok(j)=ok(j).and.igrid(j)>0
  end do

  ! Compute parent cell position
  do idim=1,ndim
     do j=1,np
        if(ok(j))then
           icd(j,idim)=id(j,idim)-2*igd(j,idim)
        end if
     end do
  end do
#if NDIM==1
  do j=1,np
     if(ok(j))then
        icell(j)=1+icd(j,1)
     end if
  end do
#endif
#if NDIM==2
  do j=1,np
     if(ok(j))then
        icell(j)=1+icd(j,1)+2*icd(j,2)
     end if
  end do
#endif
#if NDIM==3
  do j=1,np
     if(ok(j))then
        icell(j)=1+icd(j,1)+2*icd(j,2)+4*icd(j,3)
     end if
  end do
#endif
        
  ! Compute parent cell adress
  do j=1,np
     if(ok(j))then
        indp(j)=ncoarse+(icell(j)-1)*ngridmax+igrid(j)
     end if
  end do

  ! Gather hydro variables
  do j=1,np
     if(ok(j))then

        ! Convert to primitive variables
        d=uold(indp(j),1)
        u=uold(indp(j),2)/d
        v=uold(indp(j),3)/d
        w=uold(indp(j),4)/d
        e=uold(indp(j),5)/d
#ifdef SOLVERmhd
        bx1=uold(indp(j),6)
        by1=uold(indp(j),7)
        bz1=uold(indp(j),8)
        bx2=uold(indp(j),nvar+1)
        by2=uold(indp(j),nvar+2)
        bz2=uold(indp(j),nvar+3)
        e=e-0.125d0*((bx1+bx2)**2+(by1+by2)**2+(bz1+bz2)**2)/d
#endif
        v2=(u**2+v**2+w**2)
        e=e-0.5d0*v2
        do ivar=imetal,nvar
           z(ivar)=uold(indp(j),ivar)/d
        end do

        ! User defined density threshold 
        d_thres=d_sink

        ! Jeans length related density threshold
        if(d_thres<0.0)then
           temp=max(e*(gamma-1.0),smallc**2)
           d_jeans=temp*3.1415926/(4.0*dx_loc)**2/factG
           d_thres=d_jeans
        endif

        if(d>d_thres)then
           isink=-idp(ind_part(j))
           acc_mass=(d-d_thres)*vol_loc
           msink_new(isink  )=msink_new(isink  )+acc_mass
           vsink_new(isink,1)=vsink_new(isink,1)+acc_mass*u
           vsink_new(isink,2)=vsink_new(isink,2)+acc_mass*v
           vsink_new(isink,3)=vsink_new(isink,3)+acc_mass*w
           d=d_thres

           ! Convert back to conservative variable
#ifdef SOLVERmhd
           e=e+0.125d0*((bx1+bx2)**2+(by1+by2)**2+(bz1+bz2)**2)/d
#endif
           e=e+0.5d0*(u**2+v**2+w**2)
           uold(indp(j),1)=d
           uold(indp(j),2)=d*u
           uold(indp(j),3)=d*v
           uold(indp(j),4)=d*w
           uold(indp(j),5)=d*e
           do ivar=imetal,nvar
              uold(indp(j),ivar)=d*z(ivar)
           end do
        endif
          
     endif
  end do

#endif
  
end subroutine accrete_jeans
!################################################################
!################################################################
!################################################################
!################################################################
subroutine agn_feedback
  use amr_commons
  use pm_commons
  use hydro_commons
  use cooling_module, ONLY: XH=>X, rhoc, mH 
  implicit none
#ifndef WITHOUTMPI
  include 'mpif.h'
  integer::nSN_tot_all
  integer,dimension(1:ncpu)::nSN_icpu_all
  real(dp),dimension(:),allocatable::mSN_all,sSN_all,ZSN_all
  real(dp),dimension(:,:),allocatable::xSN_all,vSN_all
#endif
  !----------------------------------------------------------------------
  ! Description: This subroutine checks SN events in cells where a
  ! star particle has been spawned.
  ! Yohan Dubois
  !----------------------------------------------------------------------
  ! local constants
  integer::ip,icpu,igrid,jgrid,npart1,npart2,ipart,jpart,next_part
  integer::nSN,nSN_loc,nSN_tot,info,isink,ilevel,ivar
  integer,dimension(1:ncpu)::nSN_icpu
  real(dp)::scale_nH,scale_T2,scale_l,scale_d,scale_t,scale_v,t0
  real(dp)::scale,dx_min,vol_min,nISM,nCOM,d0,mstar,temp_blast
  real(dp)::T2_AGN,T2_min,T2_max,delta_mass_max
  integer::nx_loc
  integer,dimension(:),allocatable::ind_part,ind_grid
  logical,dimension(:),allocatable::ok_free

! JMG -- variables for iterative rblast search to get rid
! of excess AGN energy (above T2_max)
  logical::iterate_flag
  real(dp), dimension(1:nsink) :: rblast_ncells
  real(dp) :: max_rblast_ncells = 100

!OR -- debugging
    INTEGER::err_len, err_err
    CHARACTER(LEN=100)::err_name

  interface 
     subroutine average_AGN(ncells)
       use amr_commons
       real(dp), optional, dimension(:), INTENT(in):: ncells
     end subroutine average_AGN
     subroutine AGN_blast(ncells)
       use amr_commons
       real(dp), optional, dimension(:), INTENT(in):: ncells
     end subroutine AGN_blast
  end interface

  rblast_ncells = 4.0  ! default value
  if(increase_rblast > 0) rblast_ncells = increase_rblast
! JMG

  if(.not. hydro)return
  if(ndim.ne.3)return

  if(verbose)write(*,*)'Entering agn_feedback'
  
  ! Conversion factor from user units to cgs units
  call units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)

  ! Mesh spacing in that level
  nx_loc=(icoarse_max-icoarse_min+1)
  scale=boxlen/dble(nx_loc)
!  dx_min=(0.5D0**nlevelmax)*scale
  dx_min=(0.5D0**level_agn)*scale
  vol_min=dx_min**ndim

  ! AGN specific energy
  T2_AGN=0.15*1d12 ! in Kelvin

  ! Minimum specific energy
  !  T2_min=1d7  ! in Kelvin
  ! JMG
  T2_min = Tmin_AGN

  ! Maximum specific energy
!!  T2_max=1d9 ! in Kelvin
  T2_max=Tmax_AGN ! in Kelvin

  ! Instead of using the real accretion rate, look up a 
  ! value from a table.  This should update "delta_mass"
  ! for each sink particle.
  if(lookup_acc_rate) call lookup_accretion_rate

!!! MOVED HERE BY JMG
! I moved this here for the case where expand_agn_blast is true. In this
! case, once ok_blast_agn=true for a sink particle, it should remain true
! even as we expand the AGN blast radius.  Otherwise, the temp_blast
! would have to fall between Tmin and Tmax in order to set off the blast,
! even as we're expanding the blast radius.  This could lead to the problem
! where at small radius, temp_blast > Tmax, but at an expanded radius
! temp_blast < Tmin, so the blast doesn't go off at all.  We should avoid this
! problem by placing this here instead of after "average_AGN"
  ok_blast_agn(1:nsink)=.false.

  ! Iterate to make sure the blast radius is large enough so 
  ! that all the energy is used.  This really only applies
  ! if expand_agn_blast==.true.
  ! NB: if there are multiple sinks, then extra iterations 
  !  w/ iterate_flag=true can be redundant.  There is no error
  !  here, it's just not very efficient
  iterate_flag = .true.
  do while(iterate_flag .eqv. .true.)
     iterate_flag = .false.

     if(debug2) call MPI_Barrier(MPI_COMM_WORLD, info)

     ! Compute the grid discretization effects
     call average_AGN(rblast_ncells)

     ! Check if sink goes into blast wave mode
!!! MOVED ABOVE:     ok_blast_agn(1:nsink)=.false.
     do isink=1,nsink

!!!OR debug
!if (myid == 1 .or. nsink /= 1 .or. isink /= 1) WRITE(*,*) "Blast wave loop, beginning : nsink, isink, ok_blast_agn(isink)  = ", nsink, isink, ok_blast_agn(isink)
!!!

        ! Compute estimated average temperature in the blast
        temp_blast=0.0
        if(vol_gas_agn(isink)>0.0)then
           temp_blast=T2_AGN*delta_mass(isink)/mass_gas_agn(isink)
        else
           if(ind_blast_agn(isink)>0)then
              temp_blast=T2_AGN*delta_mass(isink)/mass_blast_agn(isink)
           endif
        endif
        if(temp_blast>T2_min)then
           ok_blast_agn(isink)=.true.
!!!OR debug
!if (myid == 1 .or. nsink /= 1 .or. isink /= 1) WRITE(*,*) "Blast wave loop, temperature loop : nsink, isink, ok_blast_agn(isink) = ", nsink, isink, ok_blast_agn(isink)
!!! 
        endif

!!!OR debug
!if (myid == 1 .or. nsink /= 1 .or. isink /= 1) WRITE(*,*) "Blast wave loop, after temp. loop : nsink, isink, ok_blast_agn(isink)  = ", nsink, isink, ok_blast_agn(isink)
!!!
        ! decide if we need to expand the AGN blast radius because
        ! we're not using enough of the blast energy
        if(expand_agn_blast) then
!!!OR debug
!if (myid == 1 .or. nsink /= 1 .or. isink /= 1) WRITE(*,*) "Blast wave loop, ENTER EXPAND AGN BLAST : nsink, isink, ok_blast_agn(isink)  = ", nsink, isink, ok_blast_agn(isink)
!!!
           ! In the case where only the cell in which the sink particle 
           ! lives is going to receive the blast, we actually want to
           ! expand the blast radius.  Facilitate this by setting
           ! temp_blast = 0.0.
           if(vol_gas_agn(isink)<=0.0)then  ! no cells within blast radius
              if(ind_blast_agn(isink)>0)then ! we know the cell where AGN lives
                 temp_blast=0.0
              endif
           endif

           ! If the blast temperature is too large then expand the bubble
           ! to spread the energy around.  If the temp_blast == 0 (<1d-5)then it's
           ! likely that all the gas within the blast radius is already hot.
           ! In this case expand the bubble.
           if( (temp_blast > T2_max) .or. (temp_blast < 1d-5) ) then
              if(rblast_ncells(isink) < max_rblast_ncells) then

                 if(debug2) then 
                  if (myid == 1)  WRITE(*,*) "Expand blast", myid, temp_blast, rblast_ncells(isink), &
                         vol_gas_agn(isink), delta_mass(isink), mass_gas_agn(isink), &
                         ind_blast_agn(isink)
                 endif

                 iterate_flag = .true.
!!!OR debug
!if (myid == 1) WRITE(*,*) "Blast wave loop, ITERATE FLAG = TRUE : nsink, isink, ok_blast_agn(isink)  = ", nsink, isink, ok_blast_agn(isink)
!!!
                 ! increase radius to double the volume
                 rblast_ncells(isink) = rblast_ncells(isink) * 1.25
              endif
           endif
        endif ! expand_agn_blast
     end do

  end do  ! end iteration of expanding blast radius

if (myid == 1)  WRITE(*,*) "Expand blast END", myid, temp_blast, rblast_ncells(1), &
                         vol_gas_agn(1), delta_mass(1), mass_gas_agn(1), &
                         ind_blast_agn(1)

#ifndef WITHOUTMPI
!!!  WRITE(*,*) "doing allreduce", myid, ok_blast_agn(1:5), ok_blast_agn_all(1:5), nsink
!!OR debug
!if (myid == 1) WRITE(*,*) "doing allreduce", myid, ok_blast_agn(1:5), "   ", ok_blast_agn_all(1:5)
!if (myid == 1) WRITE(*,*) "agn_feedback 3152", temp_blast, isink, delta_mass(1), mass_gas_agn(1), vol_gas_agn(1), rblast_ncells(1), ind_blast_agn(1) 
!if (myid == 1) WRITE(*,*) "ok_blast_agn", ok_blast_agn
!if (myid == 1) WRITE(*,*) "nsink, nsink_max, size(ok_blast_agn), size(ok_blast_agn_all)", nsink, nsinkmax, size(ok_blast_agn), size(ok_blast_agn_all)
 if(debug2) WRITE(*,*) "agn_feedback 3152", myid, temp_blast, isink, delta_mass(1), mass_gas_agn(1), vol_gas_agn(1), rblast_ncells(1), ind_blast_agn(1)

  ! need this barrier statement due to "expand_agn_blast"?
  call MPI_Barrier(MPI_COMM_WORLD, info)
  !if (myid == 1) WRITE(*,*) "IERROR barrier ", info  
  call MPI_ALLREDUCE(ok_blast_agn,ok_blast_agn_all,nsink,MPI_LOGICAL,MPI_LOR,MPI_COMM_WORLD,info) !code crashes inside this routine after passing once through it
  !call MPI_ERROR_STRING(info, err_name, err_len, err_err) !no error code is recovered after crash (stuck inside previous routine)
  !if (index(err_name,"MPI_SUCCESS: no errors") == 0 .or. myid == 1) WRITE(*,*) "info, err_name ", info, err_name, myid
  ok_blast_agn=ok_blast_agn_all
  !WRITE(*,*) "Processor ", myid, "is still alive !"
!!
#endif


  ! Modify hydro quantities to account for the AGN blast
  call AGN_blast(rblast_ncells)

  ! Reset accreted mass
  do isink=1,nsink

!!!OR debug
!if (myid == 1 .or. nsink /= 1 .or. isink /= 1) WRITE(*,*) "Reset accreted mass loop : nsink, isink, ok_blast_agn(isink)  = ", nsink, isink, ok_blast_agn(isink)
!!!

     if(ok_blast_agn(isink))then
!!!OR debug
!if (myid == 1) WRITE(*,*) "Acc. mass loop : OK_BLAST_AGN IS TRUE : nsink, isink, ok_blast_agn(isink)  = ", nsink, isink, ok_blast_agn(isink)
!!!
        ! Compute estimated average temperature in the blast
        temp_blast=0.0
        if(vol_gas_agn(isink)>0.0)then
           temp_blast=T2_AGN*delta_mass(isink)/mass_gas_agn(isink)
        else
           if(ind_blast_agn(isink)>0)then
              temp_blast=T2_AGN*delta_mass(isink)/mass_blast_agn(isink)
           endif
        endif

!!!! This print out has been moved by JMG to be after temp_blast calculation
        if(myid==1)then
           write(*,'("***BLAST***",I4,1X,5(1PE12.5,1X),1X,I4)') &
                & isink &
                & ,msink(isink)*scale_d*scale_l**3/2d33 &  
                & ,delta_mass(isink)*scale_d*scale_l**3/2d33 &
                & ,temp_blast &
                & ,vol_gas_agn(isink) &
                & ,rblast_ncells(isink) &
                & ,ind_blast_agn(isink)
        endif
!!!! JMG 

        if(temp_blast<T2_max)then
           delta_mass(isink)=0.0
           accmass_step_counter(isink) = 0
        else
           if(vol_gas_agn(isink)>0.0)then
              delta_mass_max=T2_max/T2_AGN*mass_gas_agn(isink)
           else
              if(ind_blast_agn(isink)>0)then
                 delta_mass_max=T2_max/T2_AGN*mass_blast_agn(isink)
              endif
           endif
           delta_mass(isink)=max(delta_mass(isink)-delta_mass_max,0.0_dp)

           ! Added by JMG -- reset the accreted mass if there is so much
           ! stored up that it's not really going away.  This is mainly
           ! for cases where we turn on the AGN in the middle of a sim.
           if(reset_accreted_mass) then
              if( (delta_mass_max / delta_mass(isink)) < 1000.) then
                 accmass_step_counter(isink) = accmass_step_counter(isink) + 1

                 if(accmass_step_counter(isink) > 10) then
                    write(*,'("---RESETTING DELTA_MASS---",' // &
                         & 'I4, 1X, I4, 1X, I4, 1X, (1PE12.5,1X))'),&
                         & myid, &
                         & isink, accmass_step_counter(isink), &
                         & delta_mass(isink)
                    delta_mass(isink) = 0.0
                    accmass_step_counter(isink) = 0
                 endif
              endif
           endif
           ! END JMG 

        endif
     endif
  end do

  ! Update hydro quantities for split cells
  do ilevel=nlevelmax,levelmin,-1
     call upload_fine(ilevel)
     do ivar=1,nvar
        call make_virtual_fine_dp(uold(1,ivar),ilevel)
     enddo
  enddo

!!!OR debug
!if (myid == 1 .or. nsink /= 1) WRITE(*,*) "End of routine agn_feedback : nsink  = ", nsink
!!!

end subroutine agn_feedback
!################################################################
!################################################################
!################################################################
!################################################################
subroutine average_AGN(rblast_ncells)
  use pm_commons
  use amr_commons
  use hydro_commons
  implicit none
#ifndef WITHOUTMPI
  include 'mpif.h'
#endif
  !------------------------------------------------------------------------
  ! Calculate the total volume and gas mass of cells within the
  ! sink radius of the AGN.  This is used as the AGN blast region.
  !
  !  rblast_ncells (real, optional; nsink) -- 
  !    the radius of the AGN blast region, in cells (at the highest
  !    resolution).  This should be an array of nsink elements, because
  !    each AGN will (potentially) have its own blast radius.
  !    Default == 4 cells.
  !    
  !------------------------------------------------------------------------
  integer::ilevel,ncache,nSN,j,isink,ind,ix,iy,iz,ngrid,iskip
  integer::i,nx_loc,igrid,info
  integer,dimension(1:nvector),save::ind_grid,ind_cell
  real(dp)::x,y,z,drr,d,u,v,w,ek,u2,v2,w2,dr_cell
  real(dp)::scale,dx,dxx,dyy,dzz,dx_min,dx_loc,vol_loc,rmax2,rmax
  real(dp)::scale_nH,scale_T2,scale_l,scale_d,scale_t,scale_v
  real(dp),dimension(1:3)::skip_loc
  real(dp),dimension(1:twotondim,1:3)::xc
  logical ,dimension(1:nvector),save::ok

  !!! JMG
  ! variables for "inject_agn_coldgas"
  logical :: count_this_cell = .true.
  real(dp) :: T2_AGN
  integer :: iteration =0
  
  interface 
     real(dp) function get_cell_temp(index_cell, scale_T2)
       use amr_commons
       real(dp), INTENT(in):: scale_T2
       integer, INTENT(in) :: index_cell
     end function get_cell_temp

     real(dp) function get_temp_after_heating(index_cell, scale_T2, p_agn)
       use amr_commons
!       use pm_commons
!       use hydro_commons
       real(dp), INTENT(in):: scale_T2, p_agn
       integer, INTENT(in) :: index_cell     
     end function get_temp_after_heating
  end interface

  ! variables for changing the blast radius
  real(dp), optional, dimension(:), INTENT(in):: rblast_ncells
  real(dp), dimension(1:nsink) :: rad_ncells
  if(present(rblast_ncells)) then
     if(size(rblast_ncells) .ne. nsink) stop
     rad_ncells = rblast_ncells
  else
     rad_ncells = 4.0  ! default value
  endif

  ! Conversion factor from user units to cgs units
  !  - need scale_T2 to find cell Temperatures
  if(inject_agn_coldgas) then
     call units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)

     ! AGN specific energy
     T2_AGN=0.15*1d12 ! in Kelvin
     T2_AGN=T2_AGN/scale_T2 ! in code units
  endif

  !!! JMG


  if(nsink==0)return
  if(verbose)write(*,*)'Entering average_AGN', myid

  ! Mesh spacing in that level
  nx_loc=(icoarse_max-icoarse_min+1)
  skip_loc=(/0.0d0,0.0d0,0.0d0/)
  skip_loc(1)=dble(icoarse_min)
  skip_loc(2)=dble(jcoarse_min)
  skip_loc(3)=dble(kcoarse_min)
  scale=boxlen/dble(nx_loc)
!  dx_min=scale*0.5D0**nlevelmax
  dx_min=scale*0.5D0**level_agn

  ! Maximum radius of the ejecta
! commented out by JMG 
!  rmax=4.0d0*dx_min/aexp
!  rmax2=rmax*rmax

!!! JMG
  ! For inject_agn_coldgas, we will do an iterative loop.  The first iteration
  ! is just to calculate p_agn, so that we will know how much each cell will
  ! be heated.  During subsequent iterations, we exclude cells that will be 
  ! too hot (above Tmax_AGN) after being heated.  Then we have to recalculate 
  ! p_agn based on the included cells, and then reiterate.
  !
  ! Currently, the following loop is iterated 10 times no matter what -- there
  ! is no test for convergence.  It typically converges before 10 iters, but
  ! continues doing them anyway.
  p_agn=0
  iteration = 0
  do
  
!!! JMG

     ! Initialize the averaged variables
     vol_gas_agn=0.0;vol_blast_agn=0.0;mass_gas_agn=0.0;ind_blast_agn=-1
     
     ! Loop over levels
     do ilevel=levelmin,nlevelmax
        ! Computing local volume (important for averaging hydro quantities) 
        dx=0.5D0**ilevel 
        dx_loc=dx*scale
        vol_loc=dx_loc**ndim
        ! Cells center position relative to grid center position
        do ind=1,twotondim  
           iz=(ind-1)/4
           iy=(ind-1-4*iz)/2
           ix=(ind-1-2*iy-4*iz)
           xc(ind,1)=(dble(ix)-0.5D0)*dx
           xc(ind,2)=(dble(iy)-0.5D0)*dx
           xc(ind,3)=(dble(iz)-0.5D0)*dx
        end do
        
        ! Loop over grids
        ncache=active(ilevel)%ngrid
        do igrid=1,ncache,nvector
           ngrid=MIN(nvector,ncache-igrid+1)
           do i=1,ngrid
              ind_grid(i)=active(ilevel)%igrid(igrid+i-1)
           end do
           
           ! Loop over cells
           do ind=1,twotondim  
              iskip=ncoarse+(ind-1)*ngridmax
              do i=1,ngrid
                 ind_cell(i)=iskip+ind_grid(i)
              end do
              
              ! Flag leaf cells
              do i=1,ngrid
                 ok(i)=son(ind_cell(i))==0
              end do
              
              do i=1,ngrid
                 if(ok(i))then
                    ! Get gas cell position
                    x=(xg(ind_grid(i),1)+xc(ind,1)-skip_loc(1))*scale
                    y=(xg(ind_grid(i),2)+xc(ind,2)-skip_loc(2))*scale
                    z=(xg(ind_grid(i),3)+xc(ind,3)-skip_loc(3))*scale
                    
                    do isink=1,nsink
                       ! Check if the cell lies within the sink radius
                       rmax= rad_ncells(isink) * dx_min/aexp
                       rmax2=rmax*rmax
                       
                       dxx=x-xsink(isink,1)
                       if(dxx> 0.5*scale)then
                          dxx=dxx-scale
                       endif
                       if(dxx<-0.5*scale)then
                          dxx=dxx+scale
                       endif
                       dyy=y-xsink(isink,2)
                       if(dyy> 0.5*scale)then
                          dyy=dyy-scale
                       endif
                       if(dyy<-0.5*scale)then
                          dyy=dyy+scale
                       endif
                       dzz=z-xsink(isink,3)
                       if(dzz> 0.5*scale)then
                          dzz=dzz-scale
                       endif
                       if(dzz<-0.5*scale)then
                          dzz=dzz+scale
                       endif
                       drr=dxx*dxx+dyy*dyy+dzz*dzz
                       dr_cell=MAX(ABS(dxx),ABS(dyy),ABS(dzz))
                       ! this cell is within sink radius
                       if(drr.lt.rmax2)then
                          !! JMG
                          count_this_cell = .true.
                          ! only count cold gas toward the total.  This will only 
                          ! include gas that, after injecting the energy, remains 
                          ! below Tmax_AGN.
                          if(inject_agn_coldgas) then
                             ! In the "zeroth" iteration, count all gas cells.  We do
                             ! this to compute p_agn so that we can estimate
                             ! how much each cell will be heated.
                             ! In later iterations, we exclude gas that will be
                             ! too hot after injection.
                             if( iteration > 0) then
                                
                                ! is the cell already above the AGN temperature?
!!                                if(get_cell_temp(ind_cell(i), scale_T2) > Tmin_AGN) then
                                if(get_temp_after_heating(ind_cell(i), scale_T2, p_agn(isink)) > Tmax_AGN) then
                                   count_this_cell = .false.

                                   ! write(*,*) "AGN skipping cell:", iteration, &
                                   !      uold(ind_cell(i),1)*scale_nH, &
                                   !      get_temp_after_heating(ind_cell(i),scale_T2,p_agn(isink) ),&
                                   !      get_cell_temp(ind_cell(i), scale_T2)
                                else
!                                   write(*,*) "AGN keeping cell:", iteration, ind_cell(i)
                                endif
                             endif   !! iteration > 0
                          endif  !! inject_agn_coldgas
                          !! JMG
                          
                          if(count_this_cell) then 
                             vol_gas_agn(isink)=vol_gas_agn(isink)+vol_loc
                             mass_gas_agn(isink)=mass_gas_agn(isink)+vol_loc*uold(ind_cell(i),1)
                          endif
                       endif
                       if(dr_cell.le.dx_loc/2.0)then
                          ind_blast_agn(isink)=ind_cell(i)
                          vol_blast_agn(isink)=vol_loc
                          mass_blast_agn(isink)=vol_loc*uold(ind_cell(i),1)
                       endif
                    end do
                 endif
              end do
           
           end do
           ! End loop over cells
        end do
        ! End loop over grids
     end do
     ! End loop over levels

#ifndef WITHOUTMPI
     ! Barrier statement added by JMG due to expand_agn_blast
     if(debug2) write(*,*) "barrier statement in average_AGN", myid
     call MPI_Barrier(MPI_COMM_WORLD, info)
     if(debug2) then
        write(*,*) "about to allreduce statement in average_AGN", myid
        write(*,*) size(vol_gas_agn), size(vol_gas_agn_all), nsink,"]]", &
             size(mass_gas_agn), size(mass_gas_agn_all)
     endif
     
     call MPI_ALLREDUCE(vol_gas_agn,vol_gas_agn_all,nsink,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
     call MPI_ALLREDUCE(mass_gas_agn,mass_gas_agn_all,nsink,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
     vol_gas_agn=vol_gas_agn_all
     mass_gas_agn=mass_gas_agn_all

  ! CORRECTION: We don't want to do the following, because it will
  ! make every processor think it is hosting the sink particle (and
  ! the sink particle's gas cell).
  !
  ! added by JMG for expand_agn_blast.  Maybe needed even without
  ! expand_agn_blast.  Share ind_blast_agn among all processors.  
  ! This gives the index of the cell where the AGN lives.  It also
  ! acts as a flag to tell the AGN to inject its energy in this cell.
!  ind_blast_agn_all = 0
!  call MPI_ALLREDUCE(ind_blast_agn,ind_blast_agn_all,nsink,MPI_DOUBLE_PRECISION,MPI_MAX,MPI_COMM_WORLD,info)
!  ind_blast_agn = ind_blast_agn_all
#endif

     ! exit the iterative exclusion of too-hot cells
     if(debug2 .and. myid == 1) write(*,'(A, L, 2E15.7)') "average_AGN iteration # ", iteration, rad_ncells(1), vol_gas_agn(1)
     iteration = iteration+1
     if(iteration > 10) exit

     if(inject_agn_coldgas) then 
        ! calculate p_agn and ... for each sink particle
        do isink=1, nsink
           p_agn(isink) = delta_mass(isink)*T2_AGN/vol_gas_agn(isink)
        enddo
        ! 
     else
     ! only do the first iteration if we're not doing inject_agn_coldgas
        exit
     endif

  enddo  ! iteration over the whole process, for inject_agn_coldgas

  if(verbose)write(*,*)'Exiting average_AGN', myid

end subroutine average_AGN
!################################################################
!################################################################
!################################################################
!################################################################
!!!! JMG--added rblast_ncells argument
!!!!   subroutine AGN_blast
subroutine AGN_blast(rblast_ncells)
  use pm_commons
  use amr_commons
  use hydro_commons
  implicit none
#ifndef WITHOUTMPI
  include 'mpif.h'
#endif
  !------------------------------------------------------------------------
  ! Find cells which are within the sink radius, and change
  ! their energy (i.e. temperature) to reflect AGN heating.
  !  rblast_ncells (default=4.0) --  define the blast radius as
  !   this times the smallest cell size
  !------------------------------------------------------------------------
  integer::ilevel,j,isink,nSN,ind,ix,iy,iz,ngrid,iskip
  integer::i,nx_loc,igrid,info,ncache
  integer,dimension(1:nvector),save::ind_grid,ind_cell
  real(dp)::x,y,z,dx,dxx,dyy,dzz,drr,d,u,v,w,ek,u_r,ESN
  real(dp)::scale,dx_min,dx_loc,vol_loc,rmax2,rmax,T2_AGN,T2_max
  real(dp)::scale_nH,scale_T2,scale_l,scale_d,scale_t,scale_v
  real(dp),dimension(1:3)::skip_loc
  real(dp),dimension(1:twotondim,1:3)::xc
  logical ,dimension(1:nvector),save::ok

  ! variables for changing the blast radius
  real(dp), optional, dimension(:), INTENT(in):: rblast_ncells
  real(dp), dimension(1:nsink) :: rad_ncells

!! JMG
  interface 
     real(dp) function get_cell_temp(index_cell, scale_T2)
       use amr_commons
       real(dp), INTENT(in):: scale_T2
       integer, INTENT(in) :: index_cell
     end function get_cell_temp

     real(dp) function get_temp_after_heating(index_cell, scale_T2, p_agn)
       use amr_commons, ONLY: dp
!       use pm_commons
!       use hydro_commons
       real(dp), INTENT(in):: scale_T2, p_agn
       integer, INTENT(in) :: index_cell     
     end function get_temp_after_heating
  end interface

  ! variables for inject_agn_coldgas
  logical :: count_this_cell

  ! for Tmax_global
  real(dp) :: added_etherm
!! JMG

  if(nsink==0)return
  if(verbose)write(*,*)'Entering AGN_blast'

  ! Mesh spacing in that level
  nx_loc=(icoarse_max-icoarse_min+1)
  skip_loc=(/0.0d0,0.0d0,0.0d0/)
  skip_loc(1)=dble(icoarse_min)
  skip_loc(2)=dble(jcoarse_min)
  skip_loc(3)=dble(kcoarse_min)
  scale=boxlen/dble(nx_loc)
!  dx_min=scale*0.5D0**nlevelmax
  dx_min=scale*0.5D0**level_agn

  ! Conversion factor from user units to cgs units
  call units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)

  ! Maximum radius of the ejecta
! commented out by JMG 
!  rmax=4.0d0*dx_min/aexp
!  rmax2=rmax*rmax

  ! Maximum radius of blast
  if(present(rblast_ncells)) then
     if(size(rblast_ncells) .ne. nsink) stop
     rad_ncells = rblast_ncells
  else
     rad_ncells = 4.0  ! default value
  endif

  ! AGN specific energy
  T2_AGN=0.15*1d12 ! in Kelvin
  T2_AGN=T2_AGN/scale_T2 ! in code units

  ! Maximum specific energy
!!!  T2_max=1d9 ! in Kelvin
  T2_max=Tmax_AGN ! in Kelvin
  T2_max=T2_max/scale_T2 ! in code units

  do isink=1,nsink
     if(ok_blast_agn(isink))then
        if(vol_gas_agn(isink)>0d0)then
           p_agn(isink)=MIN(delta_mass(isink)*T2_AGN/vol_gas_agn(isink), &
                &         mass_gas_agn(isink)*T2_max/vol_gas_agn(isink)  )
        else
           p_agn(isink)=MIN(delta_mass(isink)*T2_AGN, &
                &       mass_blast_agn(isink)*T2_max  )
        endif
     endif
  end do

  ! Loop over levels
  do ilevel=levelmin,nlevelmax
     ! Computing local volume (important for averaging hydro quantities) 
     dx=0.5D0**ilevel 
     dx_loc=dx*scale
     vol_loc=dx_loc**ndim
     ! Cells center position relative to grid center position
     do ind=1,twotondim  
        iz=(ind-1)/4
        iy=(ind-1-4*iz)/2
        ix=(ind-1-2*iy-4*iz)
        xc(ind,1)=(dble(ix)-0.5D0)*dx
        xc(ind,2)=(dble(iy)-0.5D0)*dx
        xc(ind,3)=(dble(iz)-0.5D0)*dx
     end do

     ! Loop over grids
     ncache=active(ilevel)%ngrid
     do igrid=1,ncache,nvector
        ngrid=MIN(nvector,ncache-igrid+1)
        do i=1,ngrid
           ind_grid(i)=active(ilevel)%igrid(igrid+i-1)
        end do

        ! Loop over cells
        do ind=1,twotondim  
           iskip=ncoarse+(ind-1)*ngridmax
           do i=1,ngrid
              ind_cell(i)=iskip+ind_grid(i)
           end do

           ! Flag leaf cells
           do i=1,ngrid
              ok(i)=son(ind_cell(i))==0
           end do

           do i=1,ngrid
              if(ok(i))then
                 ! Get gas cell position
                 x=(xg(ind_grid(i),1)+xc(ind,1)-skip_loc(1))*scale
                 y=(xg(ind_grid(i),2)+xc(ind,2)-skip_loc(2))*scale
                 z=(xg(ind_grid(i),3)+xc(ind,3)-skip_loc(3))*scale
                 do isink=1,nsink
                    rmax= rad_ncells(isink) * dx_min/aexp
                    rmax2=rmax*rmax

                    if(debug2 .and. i==1 .and. ind==1 .and. igrid==1 .and. ilevel==nlevelmax) &
                         write(*,*) "Rmax", rmax, rad_ncells(isink), rblast_ncells(isink)

                    ! Check if sink is in blast mode
                    if(ok_blast_agn(isink))then
                       ! Check if the cell lies within the sink radius
                       dxx=x-xsink(isink,1)
                       if(dxx> 0.5*scale)then
                          dxx=dxx-scale
                       endif
                       if(dxx<-0.5*scale)then
                          dxx=dxx+scale
                       endif
                       dyy=y-xsink(isink,2)
                       if(dyy> 0.5*scale)then
                          dyy=dyy-scale
                       endif
                       if(dyy<-0.5*scale)then
                          dyy=dyy+scale
                       endif
                       dzz=z-xsink(isink,3)
                       if(dzz> 0.5*scale)then
                          dzz=dzz-scale
                       endif
                       if(dzz<-0.5*scale)then
                          dzz=dzz+scale
                       endif
                       drr=dxx*dxx+dyy*dyy+dzz*dzz
                       
                       if(drr.lt.rmax2)then
                          
                          !! JMG 
                          count_this_cell = .true.
                          if(inject_agn_coldgas) then 
!!!                             if(get_cell_temp(ind_cell(i), scale_T2) > Tmin_AGN) then
!!! if the cell will become too hot _after_ heating, then don't include it
!!!                             if( p_agn(isink)/uold(ind_cell(i),1)*scale_T2 > Tmax_AGN) then
                             if(get_temp_after_heating(ind_cell(i), scale_T2, p_agn(isink)) > Tmax_AGN) then
                                count_this_cell = .false.
                             endif
                          endif

                          ! print out gas temperatures...
!!$                          if(debug2) then
!!$                             ! use dxx for temperature
!!$                             dxx = 0
!!$                             dxx = dxx + 0.5 * uold(ind_cell(i),2)**2
!!$                             dxx = dxx + 0.5 * uold(ind_cell(i),3)**2
!!$                             dxx = dxx + 0.5 * uold(ind_cell(i),4)**2
!!$                             dxx = dxx / uold(ind_cell(i),1)
!!$                             dxx = (2./3) * (uold(ind_cell(i),5) - dxx)
!!$                             dxx = dxx / uold(ind_cell(i),1)*scale_T2
!!$                             write(*,'(A, L, I2, 6ES9.2)') "Blast temps pre and post:",&
!!$                                  count_this_cell, ilevel, x, y, z, dxx, &
!!$                                  p_agn(isink) / uold(ind_cell(i),1) * scale_T2,&
!!$                                  uold(ind_cell(i),1) * scale_nH
!!$                          endif  ! debug2
                          !! JMG                          

                          ! Update the total energy of the gas
                          if(count_this_cell) then
!!!JMG-------------------missing a (gamma-1.0) here?  Is p_agn really a pressure or just a thermal energy?
                             added_etherm = p_agn(isink)
                             ! limit gas temperature (added) to Tmax_global
                             if(Tmax_global > 0.0) then 
                                if( added_etherm / uold(ind_cell(i),1) * scale_T2 > Tmax_global) then
                                   added_etherm = Tmax_global * uold(ind_cell(i),1) / scale_T2
                                endif
                             endif

!!!JMG                             uold(ind_cell(i),5)=uold(ind_cell(i),5)+p_agn(isink)
                             uold(ind_cell(i),5)=uold(ind_cell(i),5) + added_etherm
                          endif
                       endif
                    endif
                 end do
              endif
           end do
           
        end do
        ! End loop over cells
     end do
     ! End loop over grids
  end do
  ! End loop over levels

  do isink=1,nsink
     if(ok_blast_agn(isink).and.vol_gas_agn(isink)==0d0)then
        if(ind_blast_agn(isink)>0)then
           uold(ind_blast_agn(isink),5)=uold(ind_blast_agn(isink),5)+p_agn(isink)/vol_blast_agn(isink)
        endif
     endif
  end do

  if(verbose)write(*,*)'Exiting AGN_blast'

end subroutine AGN_blast
!###########################################################
!###########################################################
!###########################################################
!###########################################################
subroutine quenching(ilevel)
  use amr_commons
  use pm_commons
  use hydro_commons
  implicit none
  integer::ilevel
  !------------------------------------------------------------------------
  ! This routine selects regions which are eligible for SMBH formation.
  ! It is based on a stellar density threshold and on a stellar velocity
  ! dispersion threshold.
  ! On exit, flag2 array is set to 0 for AGN sites and to 1 otherwise.
  !------------------------------------------------------------------------
  real(dp)::scale_nH,scale_T2,scale_l,scale_d,scale_t,scale_v
  real(dp)::dx,dx_loc,scale,vol_loc
  real(dp)::str_d,tot_m,ave_u,ave_v,ave_w,sig_u,sig_v,sig_w
  integer::igrid,jgrid,ipart,jpart,next_part,ind_cell,iskip,ind
  integer::i,ig,ip,npart1,npart2,icpu,nx_loc
  real(dp),dimension(1:3)::skip_loc
  integer,dimension(1:nvector),save::ind_grid,ind_part,ind_grid_part

  if(numbtot(1,ilevel)==0)return
  if(verbose)write(*,111)ilevel

  ! Conversion factor from user units to cgs units
  call units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)

  ! Mesh spacing in that level
  dx=0.5D0**ilevel
  nx_loc=(icoarse_max-icoarse_min+1)
  skip_loc=(/0.0d0,0.0d0,0.0d0/)
  if(ndim>0)skip_loc(1)=dble(icoarse_min)
  if(ndim>1)skip_loc(2)=dble(jcoarse_min)
  if(ndim>2)skip_loc(3)=dble(kcoarse_min)
  scale=boxlen/dble(nx_loc)
  dx_loc=dx*scale
  vol_loc=dx_loc**ndim

#if NDIM==3
  ! Gather star particles only.

  ! Loop over grids
  do i=1,active(ilevel)%ngrid
     igrid=active(ilevel)%igrid(i)
     ! Number of particles in the grid
     npart1=numbp(igrid)
     npart2=0
     
     ! Reset velocity moments
     str_d=0.0
     tot_m=0.0
     ave_u=0.0
     ave_v=0.0
     ave_w=0.0
     sig_u=0.0
     sig_v=0.0
     sig_w=0.0
     
     ! Count star particles
     if(npart1>0)then
        ipart=headp(igrid)
        ! Loop over particles
        do jpart=1,npart1
           ! Save next particle   <--- Very important !!!
           next_part=nextp(ipart)
           if(idp(ipart).gt.0.and.tp(ipart).ne.0)then
              npart2=npart2+1
              tot_m=tot_m+mp(ipart)
              ave_u=ave_u+mp(ipart)*vp(ipart,1)
              ave_v=ave_v+mp(ipart)*vp(ipart,2)
              ave_w=ave_w+mp(ipart)*vp(ipart,3)
              sig_u=sig_u+mp(ipart)*vp(ipart,1)**2
              sig_v=sig_v+mp(ipart)*vp(ipart,2)**2
              sig_w=sig_w+mp(ipart)*vp(ipart,3)**2
           endif
           ipart=next_part  ! Go to next particle
        end do
     endif
     
     ! Normalize velocity moments
     if(npart2.gt.0)then
        ave_u=ave_u/tot_m
        ave_v=ave_v/tot_m
        ave_w=ave_w/tot_m
        sig_u=sqrt(sig_u/tot_m-ave_u**2)*scale_v/1d5
        sig_v=sqrt(sig_v/tot_m-ave_v**2)*scale_v/1d5
        sig_w=sqrt(sig_w/tot_m-ave_w**2)*scale_v/1d5
        str_d=tot_m/(2**ndim*vol_loc)*scale_nH
     endif
     
     ! Loop over cells
     do ind=1,twotondim
        iskip=ncoarse+(ind-1)*ngridmax
        ind_cell=iskip+igrid
        ! AGN formation sites
        ! if n_star>0.1 H/cc and v_disp>100 km/s
        if(str_d>0.1.and.MAX(sig_u,sig_v,sig_w)>100.)then
           flag2(ind_cell)=0
        else
           flag2(ind_cell)=1
        end if
     end do
  end do
  ! End loop over grids

#endif

111 format('   Entering quenching for level ',I2)

end subroutine quenching
!################################################################
!################################################################
!################################################################
!################################################################
!################################################################
!################################################################
subroutine make_sink_from_clump(ilevel)
  use amr_commons
  use pm_commons
  use hydro_commons
  use cooling_module, ONLY: XH=>X, rhoc, mH 
  implicit none
#ifndef WITHOUTMPI
  include 'mpif.h'
#endif
  integer::ilevel
  !----------------------------------------------------------------------
  ! Description: This subroutine create sink particle in cells where some
  ! density threshold is crossed. It also removes from the gas the
  ! corresponding particle mass. On exit, all fluid variables in the cell
  ! are modified. This is done only in leaf cells.
  ! Romain Teyssier, October 7th, 2007
  !----------------------------------------------------------------------
  ! local constants
  real(dp),dimension(1:twotondim,1:3)::xc
  integer ::ncache,nnew,ivar,ngrid,icpu,index_sink,index_sink_tot,icloud
  integer ::igrid,ix,iy,iz,ind,i,j,n,iskip,isink,inew,nx_loc
  integer ::ii,jj,kk,ind_cloud,ncloud,n_cls
  integer ::ntot,ntot_all,info
  logical ::ok_free

  real(dp)::scale_nH,scale_T2,scale_l,scale_d,scale_t,scale_v,scale_m
  real(dp)::d,x,y,z,u,v,w,e,temp,zg,factG
  real(dp)::dxx,dyy,dzz,drr
  real(dp)::msink_max2,rsink_max2
  real(dp)::rmax,rmax2
  real(dp)::velc,uc,vc,wc,l_jeans,d_jeans,d_thres,d_sink
  real(dp)::birth_epoch,xx,yy,zz,rr
  real(dp),dimension(1:3)::skip_loc
  real(dp)::dx,dx_loc,scale,vol_loc,dx_min,vol_min
  real(dp)::bx1,bx2,by1,by2,bz1,bz2

  integer ,dimension(1:nvector),save::ind_grid,ind_cell
  integer ,dimension(1:nvector),save::ind_grid_new,ind_cell_new,ind_part
  integer ,dimension(1:nvector),save::ind_part_cloud,ind_grid_cloud
  logical ,dimension(1:nvector),save::ok,ok_new=.true.,ok_true=.true.
  integer ,dimension(1:ncpu)::ntot_sink_cpu,ntot_sink_all
  
  if(.not. hydro)return
  if(ndim.ne.3)return

  if(verbose)write(*,*)'entering make_sink_from_clump for level ',ilevel

  ! Conversion factor from user units to cgs units                              
  call units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)
  scale_m=scale_d*scale_l**3

  ! Minimum radius to create a new sink from any other
  rsink_max=10.0 ! in kpc
  rsink_max2=(rsink_max*3.08d21/scale_l)**2

  ! Maximum value for the initial sink mass
  msink_max=1d5 ! in Msol
  msink_max2=msink_max*2d33/scale_m
  
  ! Gravitational constant
  factG=1d0
  if(cosmo)factG=3d0/8d0/3.1415926*omega_m*aexp

  ! Density threshold for sink particle creation
  d_sink=n_sink/scale_nH

  ! Mesh spacing in that level
  dx=0.5D0**ilevel 
  nx_loc=(icoarse_max-icoarse_min+1)
  skip_loc=(/0.0d0,0.0d0,0.0d0/)
  if(ndim>0)skip_loc(1)=dble(icoarse_min)
  if(ndim>1)skip_loc(2)=dble(jcoarse_min)
  if(ndim>2)skip_loc(3)=dble(kcoarse_min)
  scale=boxlen/dble(nx_loc)
  dx_loc=dx*scale
  vol_loc=dx_loc**ndim
!  dx_min=(0.5D0**nlevelmax)*scale
  dx_min=(0.5D0**level_agn)*scale
  vol_min=dx_min**ndim

  rmax=dble(ir_cloud)*dx_min/aexp ! Linking length in physical units
  rmax2=rmax*rmax

  ! Birth epoch
  birth_epoch=t

  ! Cells center position relative to grid center position
  do ind=1,twotondim  
     iz=(ind-1)/4
     iy=(ind-1-4*iz)/2
     ix=(ind-1-2*iy-4*iz)
     xc(ind,1)=(dble(ix)-0.5D0)*dx
     xc(ind,2)=(dble(iy)-0.5D0)*dx
     xc(ind,3)=(dble(iz)-0.5D0)*dx
  end do

  xx=0.0; yy=0.0;zz=0.0
  ncloud=0
  do kk=-2*ir_cloud,2*ir_cloud
     zz=dble(kk)*0.5
     do jj=-2*ir_cloud,2*ir_cloud
        yy=dble(jj)*0.5
        do ii=-2*ir_cloud,2*ir_cloud
           xx=dble(ii)*0.5
           rr=sqrt(xx*xx+yy*yy+zz*zz)
           if(rr<=dble(ir_cloud))ncloud=ncloud+1
        end do
     end do
  end do
  ncloud_sink=ncloud

  ! Set new sink variables to zero
  msink_new=0d0; tsink_new=0d0; delta_mass_new=0d0; xsink_new=0d0; vsink_new=0d0; oksink_new=0d0; idsink_new=0; new_born=0;

#if NDIM==3

  !------------------------------------------------
  ! Convert hydro variables to primitive variables
  !------------------------------------------------
  if(numbtot(1,ilevel)>0)then
     ncache=active(ilevel)%ngrid
     do igrid=1,ncache,nvector
        ngrid=MIN(nvector,ncache-igrid+1)
        do i=1,ngrid
           ind_grid(i)=active(ilevel)%igrid(igrid+i-1)
        end do
        do ind=1,twotondim  
           iskip=ncoarse+(ind-1)*ngridmax
           do i=1,ngrid
              ind_cell(i)=iskip+ind_grid(i)
           end do
           do i=1,ngrid
              d=uold(ind_cell(i),1)
              u=uold(ind_cell(i),2)/d
              v=uold(ind_cell(i),3)/d
              w=uold(ind_cell(i),4)/d
              e=uold(ind_cell(i),5)/d
#ifdef SOLVERmhd
              bx1=uold(ind_cell(i),6)
              by1=uold(ind_cell(i),7)
              bz1=uold(ind_cell(i),8)
              bx2=uold(ind_cell(i),nvar+1)
              by2=uold(ind_cell(i),nvar+2)
              bz2=uold(ind_cell(i),nvar+3)
              e=e-0.125d0*((bx1+bx2)**2+(by1+by2)**2+(bz1+bz2)**2)/d
#endif
              e=e-0.5d0*(u**2+v**2+w**2)
              uold(ind_cell(i),1)=d
              uold(ind_cell(i),2)=u
              uold(ind_cell(i),3)=v
              uold(ind_cell(i),4)=w
              uold(ind_cell(i),5)=e
              !           ! No AGN formation site by default
              !           flag2(ind_cell(i))=1
           end do
           do ivar=imetal,nvar
              do i=1,ngrid
                 d=uold(ind_cell(i),1)
                 w=uold(ind_cell(i),ivar)/d
                 uold(ind_cell(i),ivar)=w
              end do
           enddo
        end do
     end do
  end if

  !----------------------------
  ! Compute number of new sinks
  !----------------------------
  ntot=0
  n_cls=size(flag2,1)
  do jj=0,n_cls-1
     ntot=ntot+flag2(jj)
  end do

  !---------------------------------
  ! Check for free particle memory
  !--------------------------------
  ok_free=(numbp_free-ntot)>=0
#ifndef WITHOUTMPI
  call MPI_ALLREDUCE(numbp_free,numbp_free_tot,1,MPI_INTEGER,MPI_MIN,MPI_COMM_WORLD,info)
#endif
#ifdef WITHOUTMPI
  numbp_free_tot=numbp_free
#endif
   if(.not. ok_free)then
      write(*,*)'No more free memory for particles'
      write(*,*)'New sink particles',ntot
      write(*,*)'Increase npartmax'
#ifndef WITHOUTMPI
      call MPI_ABORT(MPI_COMM_WORLD,1,info)
#endif
#ifdef WITHOUTMPI
      stop
#endif
   end if
   
  !---------------------------------
  ! Compute global sink statistics
  !---------------------------------
#ifndef WITHOUTMPI
  call MPI_ALLREDUCE(ntot,ntot_all,1,MPI_INTEGER,MPI_SUM,MPI_COMM_WORLD,info)
#endif
#ifdef WITHOUTMPI
  ntot_all=ntot
#endif
#ifndef WITHOUTMPI
  ntot_sink_cpu=0; ntot_sink_all=0
  ntot_sink_cpu(myid)=ntot
  call MPI_ALLREDUCE(ntot_sink_cpu,ntot_sink_all,ncpu,MPI_INTEGER,MPI_SUM,MPI_COMM_WORLD,info)
  ntot_sink_cpu(1)=ntot_sink_all(1)
  do icpu=2,ncpu
     ntot_sink_cpu(icpu)=ntot_sink_cpu(icpu-1)+ntot_sink_all(icpu)
  end do
#endif
  nsink=nsink+ntot_all  
  nindsink=nindsink+ntot_all
  if(myid==1)then
     if(ntot_all.gt.0)then
        write(*,'(" Level = ",I6," New sinks prduced from clumps= ",I6," Total sinks =",I8)')&
             & ilevel,ntot_all,nsink
     endif
  end if

  !------------------------------
  ! Create new sink particles
  !------------------------------
  ! Starting identity number
  if(myid==1)then
     index_sink=nsink-ntot_all
     index_sink_tot=nindsink-ntot_all
  else
     index_sink=nsink-ntot_all+ntot_sink_cpu(myid-1)
     index_sink_tot=nindsink-ntot_all+ntot_sink_cpu(myid-1)
  end if

  ! Loop over grids
  if(numbtot(1,ilevel)>0)then
     ncache=active(ilevel)%ngrid
     do igrid=1,ncache,nvector
        ngrid=MIN(nvector,ncache-igrid+1)
        do i=1,ngrid
           ind_grid(i)=active(ilevel)%igrid(igrid+i-1)
        end do

        ! Loop over cells
        do ind=1,twotondim
           iskip=ncoarse+(ind-1)*ngridmax
           do i=1,ngrid
              ind_cell(i)=iskip+ind_grid(i)
           end do

           ! Gather cells with a new sink
           nnew=0
           do i=1,ngrid
              if (flag2(ind_cell(i))>0)then
                 nnew=nnew+1
                 ind_grid_new(nnew)=ind_grid(i)
                 ind_cell_new(nnew)=ind_cell(i)
              end if
           end do

           ! Create new sink particles
           do i=1,nnew
              index_sink=index_sink+1
              index_sink_tot=index_sink_tot+1

              ! Get gas variables
              d=uold(ind_cell_new(i),1)
              u=uold(ind_cell_new(i),2)
              v=uold(ind_cell_new(i),3)
              w=uold(ind_cell_new(i),4)
              e=uold(ind_cell_new(i),5)

              ! Get gas cell position
              x=(xg(ind_grid_new(i),1)+xc(ind,1)-skip_loc(1))*scale
              y=(xg(ind_grid_new(i),2)+xc(ind,2)-skip_loc(2))*scale
              z=(xg(ind_grid_new(i),3)+xc(ind,3)-skip_loc(3))*scale

              ! User defined density threshold
              d_thres=d_sink

              ! Jeans length related density threshold
              if(d_thres<0.0)then
                 temp=max(e*(gamma-1.0),smallc**2)
                 d_jeans=temp*3.1415926/(4.0*dx_loc)**2/factG
                 d_thres=d_jeans
              endif

              ! Mass of the new sink
              msink_new(index_sink)=min((d-d_thres)*vol_loc,msink_max2)
              delta_mass_new(index_sink)=0d0

              ! Global index of the new sink
              oksink_new(index_sink)=1d0
              idsink_new(index_sink)=index_sink_tot

              ! Store properties of the new sink
              tsink_new(index_sink)=birth_epoch
              xsink_new(index_sink,1)=x
              xsink_new(index_sink,2)=y
              xsink_new(index_sink,3)=z
              vsink_new(index_sink,1)=u
              vsink_new(index_sink,2)=v
              vsink_new(index_sink,3)=w

              new_born(index_sink)=1

              uold(ind_cell_new(i),1)=uold(ind_cell_new(i),1)-msink_new(index_sink)/vol_loc

           end do
           ! End loop over new sink particle cells

        end do
        ! End loop over cells
     end do
     ! End loop over grids

     !---------------------------------------------------------
     ! Convert hydro variables back to conservative variables
     !---------------------------------------------------------
     ncache=active(ilevel)%ngrid
     do igrid=1,ncache,nvector
        ngrid=MIN(nvector,ncache-igrid+1)
        do i=1,ngrid
           ind_grid(i)=active(ilevel)%igrid(igrid+i-1)
        end do
        do ind=1,twotondim  
           iskip=ncoarse+(ind-1)*ngridmax
           do i=1,ngrid
              ind_cell(i)=iskip+ind_grid(i)
           end do
           do i=1,ngrid
              d=uold(ind_cell(i),1)
              u=uold(ind_cell(i),2)
              v=uold(ind_cell(i),3)
              w=uold(ind_cell(i),4)
              e=uold(ind_cell(i),5)
#ifdef SOLVERmhd
              bx1=uold(ind_cell(i),6)
              by1=uold(ind_cell(i),7)
              bz1=uold(ind_cell(i),8)
              bx2=uold(ind_cell(i),nvar+1)
              by2=uold(ind_cell(i),nvar+2)
              bz2=uold(ind_cell(i),nvar+3)
              e=e+0.125d0*((bx1+bx2)**2+(by1+by2)**2+(bz1+bz2)**2)/d
#endif
              e=e+0.5d0*(u**2+v**2+w**2)
              uold(ind_cell(i),1)=d
              uold(ind_cell(i),2)=d*u
              uold(ind_cell(i),3)=d*v
              uold(ind_cell(i),4)=d*w
              uold(ind_cell(i),5)=d*e
           end do
           do ivar=imetal,nvar
              do i=1,ngrid
                 d=uold(ind_cell(i),1)
                 w=uold(ind_cell(i),ivar)
                 uold(ind_cell(i),ivar)=d*w
              end do
           end do
        end do
     end do
  end if

#ifndef WITHOUTMPI
  call MPI_ALLREDUCE(oksink_new,oksink_all,nsinkmax,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
  call MPI_ALLREDUCE(idsink_new,idsink_all,nsinkmax,MPI_INTEGER         ,MPI_SUM,MPI_COMM_WORLD,info)
  call MPI_ALLREDUCE(msink_new ,msink_all ,nsinkmax,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
  call MPI_ALLREDUCE(tsink_new ,tsink_all ,nsinkmax,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
  call MPI_ALLREDUCE(xsink_new ,xsink_all ,nsinkmax*ndim,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
  call MPI_ALLREDUCE(vsink_new ,vsink_all ,nsinkmax*ndim,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
  call MPI_ALLREDUCE(delta_mass_new,delta_mass_all,nsinkmax,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
  call MPI_ALLREDUCE(new_born,new_born_all,nsinkmax,MPI_INTEGER,MPI_SUM,MPI_COMM_WORLD,info)
#else
  oksink_all=oksink_new
  idsink_all=idsink_new
  msink_all=msink_new
  tsink_all=tsink_new
  xsink_all=xsink_new
  vsink_all=vsink_new
  delta_mass_all=delta_mass_new
  new_born_all=new_born
#endif
  do isink=1,nsink
     if(oksink_all(isink)==1)then
        idsink(isink)=idsink_all(isink)
        msink(isink)=msink_all(isink)
        tsink(isink)=tsink_all(isink)
        xsink(isink,1:ndim)=xsink_all(isink,1:ndim)
        vsink(isink,1:ndim)=vsink_all(isink,1:ndim)
        delta_mass(isink)=delta_mass_all(isink)
     endif
  end do

#endif



end subroutine make_sink_from_clump
!
!----------------------------------------------------------------
!----------------------------------------------------------------
! STUFF BELOW ALL ADDED BY JMG
!################################################################
!################################################################
!################################################################
!################################################################
subroutine lookup_accretion_rate

  use amr_commons
  use pm_commons
!  use hydro_commons
  implicit none

!---------------------------------------------------------------
! Look up the accretion rate in a table.  Interpolate between
! the times supplied if necessary.  On exit, the global
! variables "delta_mass" for each sink will be changed.
!---------------------------------------------------------------

  integer :: j, isink
  real(dp) :: acc_rate, acc_mass
  ! interpolation variables  
  real(dp) :: xlo, xhi, ylo, yhi

  ! loop over sinks, getting accretion rate for each
  do isink=1, nsink
     
     ! Do NOT extrapolate.  Just set to zero if we're outside the
     ! time domain provided in the lookup table.
     if(t < acc_lookup_times(n_acc_lookup) .and. t > acc_lookup_times(1)) then

        ! find the nearest time in the lookup table
        ! loop over times in the table
        do j = 1, n_acc_lookup - 1
           if (t >= acc_lookup_times(j)) then
              if( t < acc_lookup_times(j+1)) then
                 xlo = acc_lookup_times(j)
                 xhi = acc_lookup_times(j+1)
                 ylo = acc_lookup_rates(j)
                 yhi = acc_lookup_rates(j+1)
                 exit
              endif
           endif
        enddo

        ! interpolate
        acc_rate = (t - xlo) * (yhi - ylo) / (xhi - xlo) + ylo
     else
        acc_rate = 0.0
     endif
     dMBHoverdt(isink) = acc_rate
     acc_mass=dMBHoverdt(isink)*dtnew(levelmin)
     delta_mass(isink) = delta_mass(isink) + acc_mass
  end do
  
end subroutine lookup_accretion_rate

!################################################################
!################################################################
!################################################################
!################################################################
subroutine setup_acc_lookup

  use amr_commons
  use pm_commons
!  use hydro_commons
  implicit none

#ifndef WITHOUTMPI
  include 'mpif.h'
#endif

!---------------------------------------------------------------
! Set up the lookup table for accretion rates by reading the
! file containing the lookup table.  This should be called
! during initialization of the simulation (e.g. init_part)
!---------------------------------------------------------------
  integer:: file_unit=29, ierr, nlines, info, i
  logical:: ok_file
  
  ! read the file on root processor only (later we'll share the data)
  if(myid == 1) then 

     ! check whether the the lookup table file exists
     inquire(file=acc_lookup_file, exist = ok_file)
     if(ok_file .eqv. .false.) then
        WRITE(*,*) "Could not find file: ", acc_lookup_file, " -- exiting"
        stop
     endif

     ! open the file containing the accretion lookup table
     open(file_unit, file=acc_lookup_file, form='formatted')
     
     ! count number of lines in the file
     nlines = 0
     do 
        read(file_unit, '()', iostat=ierr)
        if(ierr .ne. 0) then ! we proabably reached EOF
           exit
        else
           nlines = nlines + 1
        endif
     enddo
     rewind(file_unit)

     ! number of entries in the lookup table == nlines
     n_acc_lookup = nlines

     ! allocate data arrays for the table
     allocate(acc_lookup_times(1:nlines), acc_lookup_rates(1:nlines))
     
     ! read the data from the file
     do i=1, nlines
        read(file_unit, *) acc_lookup_times, acc_lookup_rates
     enddo

     close(file_unit)
  endif ! myid==1

  ! share the lookup table arrays with all other processors
  call MPI_BCAST(acc_lookup_times, nlines, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, info)
  call MPI_BCAST(acc_lookup_rates, nlines, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, info)
  call MPI_BCAST(n_acc_lookup,     1,      MPI_INTEGER,          0, MPI_COMM_WORLD, info)


end subroutine setup_acc_lookup

!################################################################
!################################################################
!################################################################
!################################################################
real(dp) function get_cell_temp(index_cell, scale_T2)
  use amr_commons
  use pm_commons
  use hydro_commons
  implicit none
#ifndef WITHOUTMPI
  include 'mpif.h'
#endif
!======================================================
! Calculate the temperature of a cell (actually T/mu)
! in Kelvin
!======================================================
  integer, INTENT(in) :: index_cell
  real(dp), INTENT(in):: scale_T2
  real(dp) :: E_tot, E_internal, cell_Temp, nH
  integer :: idim

  ! gas density in cell
  nH = uold(index_cell,1)

  ! total energy in the cell
  E_tot = uold(index_cell,5)
  
  ! subtract off the kinetic energy in each of 3 dimensions
  E_internal = E_tot  
  do idim=1,ndim
     E_internal = E_internal - &
          0.5 * uold(index_cell, 1+idim)**2 / nH
  enddo

  ! convert from internal energy to temperature
  cell_Temp = E_internal * &
       (gamma - 1.0) / nH * scale_T2

  get_cell_Temp = cell_Temp

end function get_cell_temp

!################################################################
!################################################################
!################################################################
!################################################################
real(dp) function get_temp_after_heating(index_cell, scale_T2, p_agn)
  use amr_commons, ONLY: dp, ndim
!  use pm_commons
  use hydro_commons, ONLY: gamma, uold
  implicit none
#ifndef WITHOUTMPI
  include 'mpif.h'
#endif
  integer, INTENT(in) :: index_cell
  real(dp), INTENT(in):: scale_T2
  real(dp), INTENT(in) :: p_agn
!======================================================
! Calculate the temperature of a cell (actually T/mu)
! in Kelvin, after heating by an AGN with p_agn
!======================================================
  real(dp) :: E_tot, E_internal, cell_Temp, nH
  integer :: idim

  ! gas density in cell
  nH = uold(index_cell,1)

  ! total energy in the cell
  E_tot = uold(index_cell,5)+p_agn

  ! subtract off the kinetic energy in each of 3 dimensions
  E_internal = E_tot  
  do idim=1,ndim
     E_internal = E_internal - &
          0.5 * uold(index_cell, 1+idim)**2 / nH
  enddo

  ! convert from internal energy to temperature
  cell_Temp = E_internal * &
       (gamma - 1.0) / nH * scale_T2

  get_temp_after_heating = cell_Temp

end function get_temp_after_heating

!################################################################
!################################################################
!################################################################
!################################################################
subroutine measure_outflow
  use amr_commons
  use pm_commons
  use hydro_commons
  implicit none
#ifndef WITHOUTMPI
  include 'mpif.h'
#endif

  !======================================================
  ! Measure the outflow rate through two planes: one above
  ! and one below the disk.
  ! NB this hasn't been fully debugged yet.
  !======================================================

  integer::ilevel,j,ind,ix,iy,iz,ngrid,iskip
  integer::i,nx_loc,igrid,info,ncache
  integer,dimension(1:nvector),save::ind_grid,ind_cell
  real(dp)::x,y,z,dx,dxx,dyy,dzz,drr,d,u,v,w,ek,u_r,ESN
  real(dp)::scale,dx_min,dx_loc,vol_loc !, T2_AGN,T2_max
  real(dp)::scale_nH,scale_T2,scale_l,scale_d,scale_t,scale_v
  real(dp),dimension(1:3)::skip_loc
  real(dp),dimension(1:twotondim,1:3)::xc
  logical ,dimension(1:nvector),save::ok

  ! variables defining the outflow surface
  integer, parameter :: nx_outflow = 100
  integer, parameter :: ny_outflow = nx_outflow
  integer, parameter :: n_surf = 2
  integer :: ind_surf, ind_x, ind_y
  real(dp), dimension(1:nx_outflow, 1:ny_outflow, 1:n_surf) :: &
       & outflow, outflow_net, outflow_all, outflow_net_all
  real(dp), dimension(1:n_surf) :: z_surf

  ! outflow variables
  real(dp) :: rho, vel, dA, this_outflow
  real(dp) :: total_outflow, total_outflow_net,scale_outflow

  ! check_total_mass variables
  real(dp) :: part_in, fraction, cell_mass, mass_inside, mass_inside_all
  real(dp), save :: mass_inside_old, time_old
  logical :: check_total_mass = .true.

  if(verbose)write(*,*)'Entering measure_outflow()'

  call units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)

  ! Mesh spacing in that level
  nx_loc=(icoarse_max-icoarse_min+1)
  skip_loc=(/0.0d0,0.0d0,0.0d0/)
  skip_loc(1)=dble(icoarse_min)
  skip_loc(2)=dble(jcoarse_min)
  skip_loc(3)=dble(kcoarse_min)
  scale=boxlen/dble(nx_loc)
!  dx_min=scale*0.5D0**nlevelmax
  dx_min=scale*0.5D0**level_agn

  ! define the outflow surface, top then bottom
  ! 3 kpc above the disk
  z_surf(1) = scale / 2.0 + 3.0
  ! 3 kpc below the disk
  z_surf(2) = scale / 2.0 - 3.0  ! bottom

  ! Initialize the averaged variables
  outflow_net=0.0; outflow=0.0; outflow_net_all=0.0; outflow_all=0.0

  mass_inside = 0.0; mass_inside_all=0.0

  ! Loop over levels
  do ilevel=levelmin,nlevelmax
     ! Computing local volume (important for averaging hydro quantities) 
     dx=0.5D0**ilevel 
     dx_loc=dx*scale
     vol_loc=dx_loc**ndim
     ! Cells center position relative to grid center position
     do ind=1,twotondim  
        iz=(ind-1)/4
        iy=(ind-1-4*iz)/2
        ix=(ind-1-2*iy-4*iz)
        xc(ind,1)=(dble(ix)-0.5D0)*dx
        xc(ind,2)=(dble(iy)-0.5D0)*dx
        xc(ind,3)=(dble(iz)-0.5D0)*dx
     end do

     ! Loop over grids
     ncache=active(ilevel)%ngrid
     do igrid=1,ncache,nvector
        ngrid=MIN(nvector,ncache-igrid+1)
        do i=1,ngrid
           ind_grid(i)=active(ilevel)%igrid(igrid+i-1)
        end do

        ! Loop over cells
        do ind=1,twotondim  
           iskip=ncoarse+(ind-1)*ngridmax
           do i=1,ngrid
              ind_cell(i)=iskip+ind_grid(i)
           end do

           ! Flag leaf cells
           do i=1,ngrid
              ok(i)=son(ind_cell(i))==0
           end do

           do i=1,ngrid
              if(ok(i))then
                 ! Get gas cell position
                 x=(xg(ind_grid(i),1)+xc(ind,1)-skip_loc(1))*scale
                 y=(xg(ind_grid(i),2)+xc(ind,2)-skip_loc(2))*scale
                 z=(xg(ind_grid(i),3)+xc(ind,3)-skip_loc(3))*scale

                 ! Loop over surfaces of that define the outflow shell
                 ! We have an upper surface and a lower one
                 ! (above and below the disk plane)
                 do ind_surf=1, 2

                    ! Check if this cell intersects the current surface
                    if( abs(z - z_surf(ind_surf)) < (0.5 * dx_loc) ) then
                       ! find this cell within the outflow grid
                       ind_x = CEILING( x / (scale / nx_outflow) )
                       ind_y = CEILING( y / (scale / ny_outflow) )

                       ! calculate the density, z-velocity, and area of this cell
                       rho = uold(ind_cell(i),1) 
                       vel = uold(ind_cell(i),4) / rho 
                       dA = dx_loc * dx_loc
                       ! convert everything to cgs units
                       rho = rho * scale_d
                       vel = vel * scale_v
                       dA = dA * scale_l * scale_l
                       


!!!                       if(debug2) write(*,'(4F8.2, 2I5, F8.2)') "OUTFLOW", x, y, z, z_surf(ind_surf), ind_x, ind_y, dx_loc

                       ! this cell's contribution to the outflow
                       this_outflow = rho * vel * dA  
                       outflow_net(ind_x, ind_y, ind_surf) = &
                            outflow_net(ind_x, ind_y, ind_surf) + this_outflow
                       
                       ! check whether this cell is truly outflowing.
                       ! Don't count inflowing cells.
                       if( (ind_surf==1 .and. vel > 0.0) .or. &
                            ( ind_surf==2 .and. vel < 0.0) ) then 
                          outflow(ind_x, ind_y, ind_surf) = &
                               outflow(ind_x, ind_y, ind_surf) + this_outflow
                       endif

                    endif  ! cell intersects surface
                 enddo     ! loop over outflow surfaces

                 ! as a debugging check, count up total gas mass between the two planes
                 if(check_total_mass) then
                    ! does any part of this cell fall between the 2 planes?
                    if( (z - z_surf(1) < 0.5*dx_loc) .and. &
                         (z_surf(2) - z < 0.5*dx_loc) ) then

                       ! mass of this cell in grams
                       cell_mass = (uold(ind_cell(i),1) * scale_d) * (dx_loc * scale_l)**3.0

                       ! fraction of this cell that lies between the boundaries
                       fraction = 1.0

                       ! is this cell entirely within the boundaries?
                       ! Check upper boundary
                       if( (z+0.5*dx_loc) > z_surf(1)) then
                          ! what fraction of this cell is inside the bounds?
!                          part_out  =   (z + 0.5*dx_loc) - z_surf(1)
                          part_in = z_surf(1) - (z - 0.5*dx_loc)
                          fraction = fraction - part_in / dx_loc

                       endif

                       ! Does cell cross lower boundary?
                       if( (z-0.5*dx_loc) < z_surf(2)) then
                          if(fraction < 1.0) write(*,*) "UH OH>>>>>>>>>>>>"
                          
                          ! what fraction of this cell is above the lower boundary?
                          part_in = (z + 0.5*dx_loc) - z_surf(2)
                          fraction = fraction - part_in / dx_loc

                       endif

!!!                       write(*,*) fraction, dx_loc

                       mass_inside = mass_inside + cell_mass * fraction
                    endif
                 endif
              endif        ! cell is ok (==a leaf cell)
           end do          ! loop over grids           
        end do
        ! End loop over cells
     end do
     ! End loop over grids
  end do
  ! End loop over levels

#ifndef WITHOUTMPI
! Careful here!  The multi-dimensional "outflow" arrays might not be 
! contiguous blocks of memory, in which case the MPI calls will not
! work correctly.  JMG
  call MPI_ALLREDUCE(outflow, outflow_all, nx_outflow*ny_outflow*n_surf, MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
  call MPI_ALLREDUCE(outflow_net, outflow_net_all, nx_outflow*ny_outflow*n_surf, MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
  outflow=outflow_all
  outflow_net=outflow_net_all

  if(check_total_mass) then 
     call MPI_ALLREDUCE(mass_inside, mass_inside_all, 1, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, info)
     mass_inside = mass_inside_all
  endif
#endif

  ! outflows through the top surface have positive values
  ! outflows through the lower surface have negative values.  Let's
  ! change the sign convention so all outflows are positive, inflows
  ! negative
  outflow(1:nx_outflow, 1:ny_outflow, 2) = -1.* outflow(1:nx_outflow, 1:ny_outflow, 2)
  outflow_net(1:nx_outflow, 1:ny_outflow, 2) = -1.* outflow_net(1:nx_outflow, 1:ny_outflow, 2)

  ! now print out some results
  if(myid == 1) then
     ! add up all the outflows
     total_outflow = 0; total_outflow_net = 0;
     do ind_surf=1, 2
        do ind_x=1, nx_outflow
           do ind_y=1, ny_outflow
              total_outflow = total_outflow + &
                   outflow(ind_x,ind_y,ind_surf)
              total_outflow_net = total_outflow_net + &
                   outflow_net(ind_x, ind_y,ind_surf)
           enddo
        enddo
     enddo

     ! convert units from g / s to Msun / yr
     scale_outflow = 2d33 / 3d7
     total_outflow = total_outflow / scale_outflow
     total_outflow_net = total_outflow_net / scale_outflow
     
     ! print the results
!!!     if(debug2) write(*,*) "..." , scale_d, scale_v, scale_l, rho, vel / 1d5, sqrt(dA) / 3d18
     write(*,*) "Outflows (Msun/yr):", total_outflow, total_outflow_net, "net"

     if(check_total_mass) then
        
        write(*,'("Total mass:", ES10.3, "  Avg outflow rate:", F9.2)'), mass_inside / 2d33, &
             -1.0 * (mass_inside - mass_inside_old)/2d33  / &
             ( (t - time_old)* 0.470430312423675E+15 / 31556926.0 )

        mass_inside_old = mass_inside
        time_old = t
     endif
    
  endif


end subroutine measure_outflow

!--> updated subroutines wrt Kintherm and Kintherm_movie from here
!################################################################
!################################################################
!################################################################
!################################################################
subroutine measure_particle_mass(part_type)
  use pm_commons
  use amr_commons
  implicit none
#ifndef WITHOUTMPI
  include 'mpif.h'
#endif
  integer::part_type
  !------------------------------------------------------------------------
  ! Measure the total mass in particles within a sphere
  ! surrounding a sink particle. Part type is a flag
  ! telling which kinds of particles to count.  Options
  ! are 0 -- all particles
  !     1 -- only "old" particles/dark matter particles, based on age 'tp'
  !     -1 -- only sink particles and cloud particles corresponding to
  !           the sink of interest.
  !     2 -- all particles EXCEPT sink and cloud particles.  We don't
  !          count ANY cloud particles, even if they are associated with
  !          a separate sink from the sink of interest.
  !
  ! This function sets the global variables xpart_tot and vpart_tot,
  ! the COM and COM velocity of the local particles, along with the total
  ! particle mass masspart_tot.  It measures particles
  ! within all radii specified by radii_measure.
  !------------------------------------------------------------------------
  integer::igrid,jgrid,ipart,jpart,next_part,idim,info
  integer::i,ig,ip,npart1,npart2,icpu,nx_loc,isink
!!!!  integer,dimension(1:nvector),save::ind_grid,ind_part,ind_grid_part
  real(dp)::r2,dx_loc,dx_min,scale,dr
  real(dp)::scale_nH,scale_T2,scale_l,scale_d,scale_t,scale_v,scale_m


  integer::iradius, counter, counter_all
  real(dp),dimension(1:MAX_NRADII) :: rmax2
  real(dp) :: mass_this_part, max_radius
  real(dp), dimension(ndim) :: v_thispart, x_thispart
  integer :: my_sink_id

  ! Conversion factor from user units to cgs units
  call units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)
  scale_m=scale_d*scale_l**3

  ! radius within which we'll measure the mass of particles
  max_radius = 0.0
  do iradius=1, nradii_measure
     rmax2(iradius) = radii_measure(iradius) * radii_measure(iradius)
     if(radii_measure(iradius) > max_radius) &
          max_radius = radii_measure(iradius)
  enddo

  ! Reset summation variables to zero
  masspart_tot = 0.0
  masspart_all = 0.0
  counter = 0
  vpart_tot = 0.0
  vpart_all = 0.0
  xpart_tot = 0.0
  xpart_all = 0.0

  do ipart = 1, npartmax
     if(mp(ipart) <= 0.0) cycle

     ! include only old, dark matter particles
     ! (those with tp=0).  This includes sink and cloud parts.
     if(part_type == 1) then
        if(tp(ipart) > 0) cycle
     endif

     ! include only sink and cloud particles
     if(part_type == -1) then
        my_sink_id = -idp(ipart)     ! sink ID of this particle
        if(my_sink_id < 0 .or. my_sink_id > nsinkmax) cycle
     endif

     ! exclude sink/cloud particles
     if(part_type == 2) then
        my_sink_id = -idp(ipart)     ! sink ID of this particle
        if(my_sink_id > 0 .and. my_sink_id < nsinkmax) cycle
     endif

!!     if(idp(ipart) < 0) write(*,*) "ID less than zero ", levelp(ipart), idp(ipart)
!!     if(levelp(ipart) <0) cycle
     counter = counter+1

     ! loop over sinks, checking whether the particle is close
     ! to each sink
     do isink=1, nsink

        if(part_type == -1) then
           ! count only cloud particles belonging to this sink
           if(my_sink_id /= isink) cycle
        endif

        ! is this particle within the designated radius?
        r2=0.0
        do idim=1,ndim
           dr = (xp(ipart,idim)-xsink(isink,idim))
           r2=r2+dr*dr
        end do

        ! check if this particle is close enough to count at all
        if(r2 < max_radius * max_radius) then

           mass_this_part = mp(ipart)
           ! mass-weighted velocity and position of this part
           do idim=1,ndim
              v_thispart(idim) = mass_this_part * vp(ipart,idim)
              x_thispart(idim) = mass_this_part * xp(ipart,idim)
           enddo

           if(debug2) write(*,*) "measure part", idp(ipart), mass_this_part*scale_m/2d33, (v_thispart/mass_this_part)*scale_v/1e5, xp(ipart,1:ndim)

           ! loop over search radii
           do iradius=1, nradii_measure
              ! if it's close, add its mass to the total
              if(r2 < rmax2(iradius))then
                 npart2=npart2+1
                 ! sum the particle mass
                 masspart_tot(isink, iradius) = masspart_tot(isink,iradius) + mass_this_part
                 !!                             if(radii_measure(iradius) < 150.0) write(*,*) "Masspart", mass_this_part

                 ! sum mass-weighted particle velocity, and position
                 do idim=1,ndim
                    vpart_tot(isink, iradius, idim) = vpart_tot(isink,iradius, idim) + &
                         & v_thispart(idim)
                    xpart_tot(isink,iradius, idim) = xpart_tot(isink,iradius,idim) + &
                         & x_thispart(idim)

#ifdef NEW_CODE
                    ! 1D velocity dispersion in each direction
                    if(measure_part_veldisp) then
                       temp = mass_this_part + mass_sum
                       delta = v_thispart(1) - test_mean
                       R = delta * mass_this_part / temp
                       test_mean = test_mean+R
                       veldisp2(isink, iradius, idim) = veldisp2 + mass_sum*delta*R
                       mass_sum = temp
                    endif
#endif

                 enddo

              end if
           enddo  ! loop over radii
        endif

     end do  ! loop over sinks
  enddo  ! loop over particles

! Finish calculating the velocity dispersion

  ! Share the results among procs -- add up the masses.
#ifndef WITHOUTMPI
  call MPI_ALLREDUCE(masspart_tot, masspart_all, nsinkmax*nradii_measure, MPI_DOUBLE_PRECISION, MPI_SUM,&
       MPI_COMM_WORLD, info)
  call MPI_ALLREDUCE(vpart_tot, vpart_all, nsinkmax*nradii_measure*ndim, MPI_DOUBLE_PRECISION, MPI_SUM,&
       MPI_COMM_WORLD, info)
  call MPI_ALLREDUCE(xpart_tot, xpart_all, nsinkmax*nradii_measure*ndim, MPI_DOUBLE_PRECISION, MPI_SUM,&
       MPI_COMM_WORLD, info)

  ! store the shared value locally
  masspart_tot = masspart_all

  ! debug
  if(debug2 .and. myid==1) write(*,*) "vpart_all", vpart_all(1,1,1:ndim) / masspart_all(1,1)*scale_v / 1e5, &
       & xpart_all(1,1,1:ndim)

  ! Get the velocity of the center of mass.  We have to divide by
  ! the total local mass, since we did a mass-weighting of velocites
  ! earlier.
  do isink=1, nsink
     do idim=1, ndim
        do iradius=1, nradii_measure
           vpart_tot(isink, iradius, idim) = vpart_all(isink, iradius, idim) / masspart_tot(isink,iradius)
           xpart_tot(isink,iradius,idim) = xpart_all(isink,iradius,idim) / masspart_tot(isink,iradius)
        end do
     enddo
  enddo

  call MPI_ALLREDUCE(counter, counter_all, 1, MPI_INTEGER, MPI_SUM,&
       MPI_COMM_WORLD, info)
#endif

  if(debug2) write(*,*) "measure part", myid, counter

  if(myid == 1 .and. (part_type == 0 .or. debug2)) then
     write(*,'("Radii part  ", 10F12.1)') radii_measure(1:nradii_measure) * 1000.  ! in pc
     do isink=1,nsink
        write(*,'("Part mass   ", 10ES12.3)') masspart_tot(isink, 1:nradii_measure) * scale_m / 2d33
        write(*,'("V_COM       ", 10ES12.3)') vpart_tot(isink, 1:nradii_measure,1:ndim) * scale_v / 1e5
        write(*,'("X_COM       ", 10ES12.5)') xpart_tot(isink, 1:nradii_measure,1:ndim)
     end do
     write(*,*) "counter ",counter_all, ncloud_sink
  endif


end subroutine measure_particle_mass

!################################################################
!################################################################
!################################################################
!################################################################
subroutine measure_gas_mass
  use amr_commons
  use pm_commons
  use hydro_commons
  implicit none
#ifndef WITHOUTMPI
  include 'mpif.h'
#endif
  !======================================================
  ! Measure gas mass within a sphere centered on each
  ! sink particle.  Also measure angular momentum
  !======================================================
  ! local constants
  integer::igrid, ind, i,j,k, isink
  integer::info,ilevel, ix,iy,iz, iskip, ngrid, ncache
  real(dp)::scale_nH,scale_T2,scale_l,scale_d,scale_t,scale_v,scale_m
  real(dp)::scale,x,y,z
  real(dp),dimension(1:3)::skip_loc
  real(dp),dimension(1:twotondim,1:3)::xc
  integer::nx_loc
  real(dp) :: dx, dx_loc, vol_loc
  integer,dimension(1:nvector),save::ind_cell,ind_grid
  integer::iradius
  real(dp),dimension(1:MAX_NRADII) :: rmax2
  real(dp) :: mass_this_cell, max_radius, dxx,dyy,dzz, dr_sink2, dr_cell
  logical,dimension(1:nvector), save::ok
  real(dp), dimension(3) :: relative_momentum, separation, ang_momentum
  real(dp) :: this_density

  if(.not. hydro)return
  if(ndim.ne.3)return

  ! Mesh spacing in that level
  nx_loc=(icoarse_max-icoarse_min+1)
  skip_loc=(/0.0d0,0.0d0,0.0d0/)
  skip_loc(1)=dble(icoarse_min)
  skip_loc(2)=dble(jcoarse_min)
  skip_loc(3)=dble(kcoarse_min)
  scale=boxlen/dble(nx_loc)

  ! Conversion factor from user units to cgs units
  call units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)
  scale_m=scale_d*scale_l**3

  ! Radii within which to measure the mass.
  max_radius = 0.0
  do iradius=1, nradii_measure
     rmax2(iradius) = radii_measure(iradius) * radii_measure(iradius)
     if(radii_measure(iradius) > max_radius) &
          max_radius = radii_measure(iradius)
  enddo

  ! Initialize the averaged variables
  mass_summed = 0.0
  angmom_summed = 0.0

  ! Loop over levels
  do ilevel=levelmin,nlevelmax
     ! Computing local volume (important for averaging hydro quantities)
     dx=0.5D0**ilevel
     dx_loc=dx*scale
     vol_loc=dx_loc**ndim

!!!     write(*,*) vol_loc, dx_loc, ndim, dx, scale, boxlen

     ! Cells center position relative to grid center position
     do ind=1,twotondim
        iz=(ind-1)/4
        iy=(ind-1-4*iz)/2
        ix=(ind-1-2*iy-4*iz)
        xc(ind,1)=(dble(ix)-0.5D0)*dx
        xc(ind,2)=(dble(iy)-0.5D0)*dx
        xc(ind,3)=(dble(iz)-0.5D0)*dx
     end do

     ! Loop over grids
     ncache=active(ilevel)%ngrid
     do igrid=1,ncache,nvector
        ngrid=MIN(nvector,ncache-igrid+1)
        do i=1,ngrid
           ind_grid(i)=active(ilevel)%igrid(igrid+i-1)
        end do

        ! Loop over cells
        do ind=1,twotondim
           iskip=ncoarse+(ind-1)*ngridmax
           do i=1,ngrid
              ind_cell(i)=iskip+ind_grid(i)
           end do

           ! Flag leaf cells
           do i=1,ngrid
              ok(i)=son(ind_cell(i))==0
           end do

           do i=1,ngrid
              if(ok(i))then
                 ! Get gas cell position
                 x=(xg(ind_grid(i),1)+xc(ind,1)-skip_loc(1))*scale
                 y=(xg(ind_grid(i),2)+xc(ind,2)-skip_loc(2))*scale
                 z=(xg(ind_grid(i),3)+xc(ind,3)-skip_loc(3))*scale
                 do isink=1,nsink
                    ! Check if the cell lies within the given distance of the sink
                    dxx=x-xsink(isink,1)
                    dyy=y-xsink(isink,2)
                    dzz=z-xsink(isink,3)
                    dr_sink2=dxx*dxx+dyy*dyy+dzz*dzz
                    dr_cell=MAX(ABS(dxx),ABS(dyy),ABS(dzz))

                    !! DEBUG
!                    if(dr_sink2 > max_radius*max_radius) then
!                       WRITE(*,*) "Debug", dr_sink2, max_radius, xsink(isink,1:3), x,y,z
!                    endif

                    ! If we're within one of the radii of interest, then
                    ! we should calculate the mass and angular momentum of
                    ! this cell
                    if(dr_sink2 < max_radius*max_radius) then
                       ! calculate gas cell properties
                       mass_this_cell = uold(ind_cell(i),1)*vol_loc

!!                       write(*,*) "AA", dr_sink2, max_radius, mass_this_cell*1d9, ilevel, nradii_measure

                       ! calculate the angular momentum of this cell
                       separation = (/dxx, dyy, dzz/)
                       this_density = uold(ind_cell(i),1)
                       do k=1, ndim
                          relative_momentum(k) = mass_this_cell * (uold(ind_cell(i),k+1)/this_density - vsink(isink,k))
                       enddo
                       call cross_product(separation, relative_momentum, ang_momentum)

!! account for grid effects here?  e.g. if grid cell is on the spherical border?

                       ! loop over selected radii of interest
                       do iradius=1, nradii_measure
if(dr_sink2.lt.rmax2(iradius))then
                             ! This cell lies within this radius.  Add its mass
                             ! and angular momenta to the running totals
                             mass_summed(isink, iradius) = mass_summed(isink, iradius) + mass_this_cell

                             do k=1,ndim
                                angmom_summed(isink, iradius, k) = angmom_summed(isink, iradius, k) + &
                                     ang_momentum(k)
                             enddo
                          endif  ! if we're within the radius of interest
                       end do  ! end loop over radii of interest
                    endif      ! if we're within at least 1 radius of interest

                 end do  ! loop over sinks
              endif      ! ok -- leaf cell
           end do        ! loop over ngrid

        end do
        ! End loop over cells
     end do
     ! End loop over grids
  end do
  ! End loop over levels


! Share the results among procs -- add up the masses.

#ifndef WITHOUTMPI
  call MPI_ALLREDUCE(mass_summed, mass_summed_all, nsinkmax*nradii_measure, MPI_DOUBLE_PRECISION, MPI_SUM,&
       MPI_COMM_WORLD, info)
  call MPI_ALLREDUCE(angmom_summed, angmom_summed_all, nsinkmax*nradii_measure*ndim, MPI_DOUBLE_PRECISION, MPI_SUM,&
       MPI_COMM_WORLD, info)
  mass_summed = mass_summed_all
  angmom_summed=angmom_summed_all
#endif

  ! print out results
  if(myid == 1) then
     if(nradii_measure > 0) then
        write(*,'("Radii   ", 10F12.1)') radii_measure(1:nradii_measure) * 1000.  ! in pc
     endif

     do isink=1,nsink
        if(do_measure_gas_mass) then
           write(*,'("Gas mass", 10ES12.3)') mass_summed(isink, 1:nradii_measure) * scale_m / 2d33
           do k=1,ndim
              write(*,'("Angmom ", i1, 10ES12.3)') k, angmom_summed(isink, 1:nradii_measure, k)
           enddo
        endif  ! do measure gas mass
     end do

  endif

end subroutine measure_gas_mass


!################################################################
!################################################################
!################################################################
!################################################################
subroutine move_sink_to_center(ilevel)
  use pm_commons
  use amr_commons
  implicit none
#ifndef WITHOUTMPI
  include 'mpif.h'
#endif
  integer::ilevel
  !------------------------------------------------------------------------
  ! Move sink particle to the center of the simulation box,
  ! or the local center of mass (if reposition_sink_com is true)
  !------------------------------------------------------------------------
  integer::igrid,jgrid,ipart,jpart,next_part,info
  integer::i,ig,ip,npart1,npart2,icpu,nx_loc
  integer,dimension(1:nvector),save::ind_grid,ind_part,ind_grid_part

  real(dp), dimension(1:nsinkmax, 1:ndim) :: position
  integer :: idim, isink

  if(reposition_sink_com) then
     ! measure local COM of "old" particles only
     call measure_particle_mass(1)
  endif

  ! reset sink position (on all processors)
  do isink=1, nsink
     do idim=1, ndim
        ! this should be temporary!
        ! Move sink to center of the box
        position(isink,idim) = boxlen / 2.0

        ! move sink to the local center of mass
        if( reposition_sink_com) then
           position(isink,idim) = xpart_tot(isink, 1, idim)
        endif

        ! actually change the sink position now
        xsink(isink,idim) = position(isink,idim)

        write(*,*) "Moving sink to center:", myid, position(1,idim)

        ! also reset velocity to zero
        vsink(isink,idim) = 0.0

        ! actually, reset sink velocity to that of the local COM
        if(reposition_sink_com) then
           vsink(isink,idim) = vpart_tot(isink, 1, idim)
        endif
     end do
  end do


  ! Now we have to move the actual particle that represents the sink.
  ! Gather sink particles only.
  ! Loop over cpus
  do icpu=1,ncpu
     igrid=headl(icpu,ilevel)
     ig=0
     ip=0
     ! Loop over grids
     do jgrid=1,numbl(icpu,ilevel)
        npart1=numbp(igrid)  ! Number of particles in the grid
        npart2=0

        ! Count sink particles
        if(npart1>0)then
           ipart=headp(igrid)
           ! Loop over particles
           do jpart=1,npart1
              ! Save next particle   <--- Very important !!!
              next_part=nextp(ipart)
              if(idp(ipart).lt.0 .and. idp(ipart) .gt. -1*nsinkmax)then
                 npart2=npart2+1
              endif

              ipart=next_part  ! Go to next particle
           end do
        endif

        ! Gather sink particles
        if(npart2>0)then
           ig=ig+1
           ind_grid(ig)=igrid
           ipart=headp(igrid)
           ! Loop over particles
           do jpart=1,npart1
              ! Save next particle   <--- Very important !!!
              next_part=nextp(ipart)
              ! Select only sink particles
              if(idp(ipart).lt.0 .and. idp(ipart) .gt. -1*nsinkmax)then
                 if(ig==0)then
                    ig=1
                    ind_grid(ig)=igrid
                 end if
                 ip=ip+1
                 ind_part(ip)=ipart
                 ind_grid_part(ip)=ig
              endif
              if(ip==nvector)then
!!                 call mk_cloud(ind_grid,ind_part,ind_grid_part,ig,ip,ilevel)
                 call move_sink(ind_grid,ind_part,ind_grid_part,ig,ip,ilevel, position, vsink)
                 ip=0
                 ig=0
              end if
              ipart=next_part  ! Go to next particle
           end do
           ! End loop over particles
        end if

        igrid=next(igrid)   ! Go to next grid
     end do

     ! End loop over grids
     if(ip>0)call move_sink(ind_grid,ind_part,ind_grid_part,ig,ip,ilevel, position, vsink)
  end do
  ! End loop over cpus


end subroutine move_sink_to_center
!################################################################
!################################################################
!################################################################
!################################################################
subroutine move_sink(ind_grid,ind_part,ind_grid_part,ng,np,ilevel, new_position, new_vel)
  use amr_commons
  use pm_commons
  use hydro_commons
  implicit none
  integer::ng,np,ilevel
  integer,dimension(1:nvector)::ind_grid
  integer,dimension(1:nvector)::ind_grid_part,ind_part
  real(dp), dimension(1:nsinkmax,1:ndim) :: new_position, new_vel
  !-----------------------------------------------------------------------
  ! Change the position of a sink particle "by hand." Change velocity also
  !-----------------------------------------------------------------------
  logical::error
  integer::j,isink,ii,jj,kk,ind,idim,nx_loc
  real(dp)::dx_loc,scale,dx_min,xx,yy,zz,rr,r2,r2_eps
  ! Particle-based arrays
  logical,dimension(1:nvector),save::ok

  real(dp), dimension(1:nsinkmax, 1:ndim) :: position

  ! Mesh spacing in that level
!!$  dx_loc=0.5D0**ilevel
!!$  nx_loc=(icoarse_max-icoarse_min+1)
!!$  scale=boxlen/dble(nx_loc)
!!$!  dx_min=scale*0.5D0**nlevelmax/aexp
!!$  dx_min=scale*0.5D0**level_agn/aexp
!!$  r2_eps=(1d-15*dx_min)**2
!!$

  do j=1,np

     isink=-idp(ind_part(j))

     ! reset sink particle's position
     do idim=1, ndim
        ! this should be temporary!
        ! Move sink to center of the box
!!!        position(isink,idim) = boxlen / 2.0
        position(isink,idim) = new_position(isink,idim)

!!!        xsink(isink,idim) = position(isink,idim)
        xp(ind_part(j),idim) = position(isink,idim)

        write(*,*) "Moving part to center:", myid, position(isink,idim)

        ! also reset velocity to zero
!!        vsink(isink,idim) = 0.0
!!        vp(ind_part(j),idim) = 0.0
        vp(ind_part(j),idim) = new_vel(isink,idim)
     end do

  end do

end subroutine move_sink
!################################################################
!################################################################
!################################################################
!################################################################
subroutine cross_product(r, v, my_result)
  use amr_parameters
  implicit none

  real(dp), dimension(3), INTENT(in) :: r, v
  real(dp), dimension(3), INTENT(out) :: my_result

  my_result(1) = r(2)*v(3) - r(3)*v(2)
  my_result(2) = r(3)*v(1) - r(1)*v(3)
  my_result(3) = r(1)*v(2) - r(2)*v(1)

end subroutine cross_product


!################################################################
!################################################################
!################################################################
!################################################################
subroutine dynamical_friction_accel(ilevel)
  use amr_commons
  use pm_commons
  implicit none

  integer, intent(in) :: ilevel
  integer :: isink, iradius, idim

  real(dp), parameter :: pi = 3.14159
  real(dp) :: sink_mass, star_radius, star_density, big_G, Coulomb_log
  real(dp), dimension(3) :: vrel
  real(dp) :: vrel_mag, X_dyn


  ! loop over sinks
  do isink=1, nsink

     sink_mass = msink(isink)

     ! determine density of surrounding stars
     iradius = 1    ! which radius to use?
     star_radius = radii_measure(iradius)
     star_density = masspart_tot(isink,iradius) / &
          (4.0/3.0*pi * star_radius*star_radius*star_radius)

     ! big_G = gravitational constant.  In simulation units this is 1 I think.
     big_G = 1.0

     ! Coulomb log.  Values from 5 to 15 seem reasonable???
     Coulomb_log = 10.0

     ! Find velocity of BH relative to local background
     ! (both the vector and the magnitude)
     vrel_mag = 0
     do idim=1, ndim
        vrel(idim) = vsink(isink,idim) - vpart_tot(isink,iradius,idim)
        vrel_mag = vrel_mag + vrel(idim)*vrel(idim)
     enddo
     vrel_mag = sqrt(vrel_mag)

     ! Find the "X" parameter for dynamical friction formula
     ! The ratio of relative velocity of the massive body to the
     ! peak in the Maxwellian
!!!     X_dyn = vrel_mag / (sqrt(2)*veldisp_part(isink, iradius, idim))
!!! To make my life a little easier (not having to calculate the veldisp), set X_dyn=1.
     X_dyn = 1.0

     ! loop over 3 dimensions
     do idim=1, ndim
        ! dynamical friction force formula... actually do the
        ! acceleration, not the force
        fdyn_accel(isink, idim) = -4.0*pi*Coulomb_log * big_G**2 * &
             sink_mass * star_density * &
             vrel(idim) / (vrel_mag*vrel_mag*vrel_mag) * &
             (ERF(X_dyn) - 2*X_dyn/ sqrt(pi) * EXP(-1.0*X_dyn*X_dyn))

        fdyn_accel(isink,idim) = fdyn_accel(isink,idim) * fdyn_factor

        ! Need to cap acceleration to prevent "overshooting" during a single timestep.
        ! Essentially, the black hole should never accelerate so that its velocity goes
        ! from +100 km/s to -100 km/s during a single timestep.  It would go just to zero.
!!        write(*,*) "CORRECT", vrel(idim), vsink(isink,idim), vpart_tot(isink,iradius,idim), fdyn_accel(isink,idim)

        if( abs(fdyn_accel(isink,idim) * dtnew(ilevel)) > abs(vrel(idim)) .and. &
             vrel(idim) * fdyn_accel(isink,idim) < 0.0) then
!!           if(myid == 1) write(*,*) "CORRECT", vrel(idim)*65.6, vsink(isink,idim)*65.6, vpart_tot(isink,iradius,idim)*65.6, &
!!                fdyn_accel(isink,idim)/(14.9**2), fdyn_accel(isink,idim)*dtnew(ilevel)*65.6

           fdyn_accel(isink,idim) = -1.0*vrel(idim) / dtnew(ilevel)

!!           if(myid == 1) write(*,*) "CORR2", fdyn_accel(isink,idim) / (14.9**2), dtnew(ilevel)*14.9, &
!!                fdyn_accel(isink,idim)*dtnew(ilevel)*65.6
        endif

        ! write out acceleration (convert to kpc/Myr^2), vrel in km/s, star_density in H/cc
!!        if( myid == 1) write(*,"(A, 2I3, ES12.3, 4F8.1)") "Dynamical Friction", isink,idim, fdyn_accel(isink,idim)/(14.9**2), &
!!             vrel(idim)*65.6, star_density*6.8d-23/1.66d-24*0.76, vsink(isink,idim)*65.5, vpart_tot(isink,iradius,idim)*65.5
     enddo
  enddo


end subroutine dynamical_friction_accel


!################################################################
!################################################################
!################################################################
! JMG
! Find the separation between 2 points.
! Does not work for periodic boxes (yet)
!
subroutine get_separation(position1, position2,  &
     separation, sep_vector, separation2)
  use amr_parameters
  implicit none

  real(dp), intent(IN), dimension(1:3) :: position1, position2
  real(dp), intent(OUT) :: separation, separation2
  real(dp), intent(OUT), dimension(1:3) :: sep_vector
  integer :: idim

  separation2 = 0.0
  do idim=1, ndim
     sep_vector(idim) = position1(idim) - position2(idim)
     separation2 = separation2 + sep_vector(idim)*sep_vector(idim)
  enddo
  separation = sqrt(separation2)

end subroutine get_separation


!################################################################
!################################################################
!################################################################
!################################################################
subroutine sink_nbody_force_fine(input_level)
  use amr_commons
  use pm_commons
  use hydro_commons
  implicit none
#ifndef WITHOUTMPI
  include 'mpif.h'
#endif
  integer, intent(IN) :: input_level
  real(dp) :: dx, dx_agn, dx_loc, softening, volume_loc, vol_loc
  integer::ip,icpu,igrid,jgrid,npart1,npart2,ipart,jpart,next_part
  integer:: ilevel, ind, iz,iy,ix, ncache, ngrid, iskip
  integer :: i, nx_loc, isink, idim, info
  integer, dimension(1:nvector) :: ind_grid, ind_cell
  real(dp), dimension(1:twotondim, 1:3)::xc
  real(dp), dimension(1:3) :: skip_loc, x_cell, dxx
  real(dp) :: separation, separation2, mcell, factG, scale
  logical, dimension(1:nvector) :: ok

!!!  write(*,*) "Starting function", myid, levelmin

  ! Gravitational constant
  factG = 1d0

  ! Gravitational softening
  dx_agn = boxlen * 0.5D0**level_agn
  softening = 20.0 * dx_agn

  ! Set sink acceleration to zero for all sinks.  We will
  ! calculate this below.
  sinkaccel(1:nsinkmax, 1:ndim) = 0

  ! Loop over all the cells in the simulation box, calculating
  ! the gravitational acceleration of the sink due to each cell.

  ! Loop over all levels
!!  do ilevel = levelmin, nlevelmax
  do ilevel = levelmin, nlevelmax

     ! Calculate cell size
     dx = 0.5D0**ilevel
     dx_loc = dx * boxlen
     volume_loc = dx_loc**ndim  ! in kpc^3
     nx_loc=(icoarse_max-icoarse_min+1)
     skip_loc=(/0.0d0,0.0d0,0.0d0/)
     if(ndim>0)skip_loc(1)=dble(icoarse_min)
     if(ndim>1)skip_loc(2)=dble(jcoarse_min)
     if(ndim>2)skip_loc(3)=dble(kcoarse_min)
     scale=boxlen/dble(nx_loc)

     ! Cells center position relative to grid center position
     do ind=1,twotondim
        iz=(ind-1)/4
        iy=(ind-1-4*iz)/2
        ix=(ind-1-2*iy-4*iz)
        xc(ind,1)=(dble(ix)-0.5D0)*dx
        xc(ind,2)=(dble(iy)-0.5D0)*dx
        xc(ind,3)=(dble(iz)-0.5D0)*dx
     end do

     ! Loop over grids
     ncache = active(ilevel)%ngrid
     do igrid = 1, ncache, nvector
        ngrid = MIN(nvector, ncache-igrid+1)
        do i=1, ngrid
           ind_grid(i) = active(ilevel)%igrid(igrid+i-1)
        enddo

!!!write(*,*) "gridloop", levelmin, ilevel

        ! Loop over cells
        do ind=1, twotondim
           iskip=ncoarse+(ind-1)*ngridmax
           do i=1,ngrid
              ind_cell(i)=iskip + ind_grid(i)
           end do

           ! Flag leaf cells
           do i=1,ngrid
              ok(i)=son(ind_cell(i))==0
           end do

           do i=1,ngrid
              if(ok(i))then
                 ! Get gas cell position
                 x_cell(1)=(xg(ind_grid(i),1)+xc(ind,1)-skip_loc(1))*scale
                 x_cell(2)=(xg(ind_grid(i),2)+xc(ind,2)-skip_loc(2))*scale
                 x_cell(3)=(xg(ind_grid(i),3)+xc(ind,3)-skip_loc(3))*scale

!!                 write(*,*) "OKOK", levelmin, ilevel

                 do isink=1,nsink
                    ! Calculate distance between this cell and the sink.
                    call get_separation(x_cell(1:3), xsink(isink,1:3),&
                         separation, dxx, separation2)

!!!                    write(*,*) "SEPA", separation

                    ! Mass of gas in this cell.  First term is the
                    ! gas density.
                    mcell = uold(ind_cell(i),1) * volume_loc

!!!                    write(*,*) "MCELL", mcell, x_cell(1), xsink(isink,1), dxx(1)

                    ! Acceleration of the sink due to this cell.
                    ! factG is the gravitational constant, big G
                    do idim = 1, ndim
                       sinkaccel(isink, idim) = sinkaccel(isink,idim) + &
                            factG * &
                            mcell * dxx(idim) / &
                            (separation2 + softening**2.0)**(1.5)
                    enddo
!!!                    write(*,*) "TESTING111", x_cell(1), xsink(isink,1), dxx(1), sinkaccel(isink,1)


                 end do ! loop over sinks
              end if  ! this is a leaf cell
           end do     ! loop over grids
        end do        ! loop over cells
     end do           ! loop over grids...?
  end do              ! loop over levels
  !

!!!  write(*,*) "DONE GAS CELLS", myid, levelmin

  ! Now do the same thing for particles -- add up the forces
  ! from the particles on the sink.
  ! Loop over cpus
  do icpu=1,ncpu
     !!! JMG - by starting at levelmin, I think this counts all particles
     ! see feedback.f90 for similar loops
     igrid=headl(icpu,levelmin)

     ! Loop over grids
     do jgrid=1,numbl(icpu,levelmin)
        npart1=numbp(igrid)  ! Number of particles in the grid
        npart2 = 0

!!        write(*,*) "LOOP GRID", myid, npart1, numbl(icpu,levelmin), levelmin

        ! Calculate force from particles in this grid
        if(npart1>0)then

!!!           write(*,*) "FOUND npart1 = ", npart1
           ipart=headp(igrid)

           ! Loop over particles
           do jpart=1,npart1
              ! Save next particle   <--- Very important !!!
              next_part=nextp(ipart)

!!              write(*,*) "PARTLOOP"

              ! Is this a sink or cloud partice? If so, then skip it
              ! for the force calculation.  The sink doesn't apply gravity
              ! to itself!  (This shouldn't be necessary since the cloud
              ! particles are in a sphere around the BH... they should
              ! cancel out.)
              if(idp(ipart).lt.0 .and. idp(ipart) .gt. -1*nsinkmax)then
                 cycle
              else
                 ! loop over sinks
                 do isink=1, nsink
                    ! dxx and separation are outputs here
                    call get_separation(xp(ipart,1:3), xsink(isink, 1:3), &
                         separation, dxx, separation2)

                    ! Acceleration of the sink due to this particle.
                    ! factG is the gravitational constant, big G
                    do idim = 1, ndim

! ZZZZZZZZZZZZZZshould be += not =
                       sinkaccel(isink, idim) = sinkaccel(isink, idim) + &
                            factG * &
                            mp(ipart) * dxx(idim) / &
                            (separation2 + softening**2.0)**(1.5)
                    enddo
!!!                    write(*,*) "TESTINGXXX", xp(ipart,1), xsink(isink,1), dxx(1), separation, mp(ipart), mp(ipart) * dxx(1) / (separation2+softening**2.0)**(1.5)

                 enddo
              endif

              ipart=next_part  ! Go to next particle
           end do
        endif

        igrid=next(igrid)   ! Go to next grid
     end do
     ! End loop over grids
  enddo  ! CPUs

!!  write(*,*) "ALMOST DONE... MPI next"

  ! do isink=1,nsink
  !    write(*,*) "Sinkaccel:", sinkaccel(isink, 1:3)
  ! enddo

  ! Now add up accelerations from all CPUs
  if(nsink > 0) then
#ifndef WITHOUTMPI
     call MPI_ALLREDUCE(sinkaccel, sinkaccel_all, nsinkmax*ndim, &
          MPI_DOUBLE_PRECISION, MPI_SUM, &
          MPI_COMM_WORLD,info)
#endif
     sinkaccel = sinkaccel_all
  endif

end subroutine sink_nbody_force_fine
