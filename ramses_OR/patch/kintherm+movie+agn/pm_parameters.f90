module pm_parameters
  use amr_parameters, ONLY: dp
  integer::npartmax=0               ! Maximum number of particles
  integer::nsinkmax=10000           ! Maximum number of sinks
! JMG
! Sink IDs are negative, and that's how they are flagged as being
! sink particles.  But when kinetic star-formation feedback is used (abs(f_w)>0),

!!OR
!OR modified amr_step.f90, feedback.f90 and star_formation.f90 to use both
!kinetic and thermal SNe feedback with f_ek > 0 and f_w < 0 ---> set abs(f_w).
!!OR

! the "GMC" or "debris" particles use negative IDs as a flag also. 
! min_debris_ID allows both sinks and kinetic feedback simultaneously by
! requiring the |idp(debris)| > min_sink_id.  Min_debris_id must be larger than
! (the absolute value of) the largest sink particle index, so choosing 
! min_debris_id == nsinkmax is good.  GMC IDs are set to 
! be equal to the number of stars formed in a given cell during a single
! timestep -- this should usually be small, typically 1.
  integer::min_debris_id=10000      ! debris particle IDs start at -1* this value
!JMG
  integer::npart=0                  ! Actual number of particles
  integer::nsink=0                  ! Actual number of sinks
  integer::iseed=0                  ! Seed for stochastic star formation
  integer::nstar_tot=0              ! Total number of star particle
  integer::ir_cloud=4               ! Radius of cloud region in unit of grid spacing
  real(dp)::mstar_tot=0             ! Total star mass
  real(dp)::mstar_lost=0            ! Missing star mass
end module pm_parameters
