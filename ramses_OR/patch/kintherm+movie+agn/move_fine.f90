subroutine move_fine(ilevel)
  use amr_commons
  use pm_commons
  implicit none
#ifndef WITHOUTMPI
  include 'mpif.h' 
#endif
  integer::ilevel
  !----------------------------------------------------------------------
  ! Update particle position and time-centred velocity at level ilevel. 
  ! If particle sits entirely in level ilevel, then use fine grid force
  ! for CIC interpolation. Otherwise, use coarse grid (ilevel-1) force.
  !----------------------------------------------------------------------
  integer::igrid,jgrid,ipart,jpart,next_part,ig,ip,npart1,info,isink
  integer,dimension(1:nvector),save::ind_grid,ind_part,ind_grid_part
  integer :: idim, iradius, part_type
  real(dp) :: dvsink
  real(dp)::scale_nH,scale_T2,scale_l,scale_d,scale_t,scale_v,scale_m

  if(numbtot(1,ilevel)==0)return
  if(verbose)write(*,111)ilevel

  ! Conversion factor from user units to cgs units
  call units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)

  ! Set new sink variables to old ones
  if(sink)then
     vsink_new=0d0
     oksink_new=0d0
     ind_sink_part = -1
  endif

  ! Update particles position and velocity
  ig=0
  ip=0
  ! Loop over grids
  igrid=headl(myid,ilevel)
  do jgrid=1,numbl(myid,ilevel)
     npart1=numbp(igrid)  ! Number of particles in the grid
!!     if(myid==1) write(*,*) "move fine nparts grid", myid, npart1, ilevel
     if(npart1>0)then        
        ig=ig+1
        ind_grid(ig)=igrid
        ipart=headp(igrid)
        ! Loop over particles
        do jpart=1,npart1
           ! Save next particle  <---- Very important !!!
           next_part=nextp(ipart)
           if(ig==0)then
              ig=1
              ind_grid(ig)=igrid
           end if
           ip=ip+1
           ind_part(ip)=ipart
           ind_grid_part(ip)=ig   
           if(ip==nvector)then
              call move1(ind_grid,ind_part,ind_grid_part,ig,ip,ilevel)
              ip=0
              ig=0
           end if
           ipart=next_part  ! Go to next particle
        end do
        ! End loop over particles
     end if
     igrid=next(igrid)   ! Go to next grid
  end do
  ! End loop over grids
  if(ip>0)call move1(ind_grid,ind_part,ind_grid_part,ig,ip,ilevel)

  if(sink)then
     if(nsink>0)then

#ifndef WITHOUTMPI
        call MPI_ALLREDUCE(oksink_new,oksink_all,nsinkmax     ,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
        call MPI_ALLREDUCE(vsink_new ,vsink_all ,nsinkmax*ndim,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
#else
        oksink_all=oksink_new
        vsink_all=vsink_new
#endif

!!!JMG-----------

        if(bh_dynamical_friction) then
           ! Find the local particle mass, and the center-of-mass position
           ! and velocity of the particle mass.  We want to exclude sink
           ! particles in this calculation.
           part_type = 2
           call measure_particle_mass(part_type)

           ! calculate the dynamical friction for each sink
           call dynamical_friction_accel(ilevel)
        endif

        ! DEBUG
        if(debug2) write(*,*) "Default vsink", ilevel, oksink_all(1), vsink_all(1,1:ndim)*3d21 / 1e5

!!$        ! Sink takes on the average velocity of its cloud particles
!!$        if(vsink_avg_cloud) then
!!$#ifndef WITHOUTMPI
!!$           ! add up all cloud velocities across procs
!!$           call MPI_ALLREDUCE(vcloud_new,vcloud_all,nsinkmax*ndim,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
!!$#else
!!$           vcloud_all=vcloud_new
!!$#endif
!!$           ! sink velocity is (unweighted) average of cloud particle velocities
!!$           do isink=1,nsink
!!$              vsink_all(isink,1:ndim) = vcloud_all(isink,1:ndim) / ncloud_sink
!!$           enddo
!!$
!!$           write(*,*) "Cloud vsink", ilevel, oksink_all(1), vsink_all(1,1:ndim)
!!$        end if

        ! Sink takes on the center-of-mass velocity within some measured radius
        if(sink_track_vcom .or. vsink_avg_cloud) then

           if(sink_track_vcom) part_type = 1   ! DM and old stars only
           if(vsink_avg_cloud) part_type = -1  ! cloud parts only

           ! This sets "masspart_tot" and "vpart_tot" global variables on all
           ! processes.  Also "xpart_tot"
           call measure_particle_mass(part_type)

           ! assign sink velocity to that of the local COM.
           iradius = 1  ! which of the radii in radii_measure do we want to use?
           do isink=1,nsink
              do idim=1, ndim
                 vsink_all(isink,idim) = vpart_tot(isink, iradius, idim)

                 if(sink_track_com) then
                    ! add a velocity boost to the sink toward the center of mass
                    ! This boost is proportional to the distance between COM
                    ! and sink.  Constant is 500 km/s per 1kpc of separation.
                    ! Assumes length units are already in kpc.
                    dvsink = (500.0*1e5 / scale_v)*(xpart_tot(isink, iradius, idim) - xsink(isink,idim))
                    vsink_all(isink,idim) = vsink_all(isink,idim) + dvsink
                 endif

              enddo  ! dim
           enddo  ! sink

        endif  ! sink_track_vcom
!!!JMG-----------
     endif   ! nsink > 0

     ! Move the sink particle.
     do isink=1,nsink
        if(oksink_all(isink)==1d0)then
           vsink(isink,1:ndim)=vsink_all(isink,1:ndim)
           if(static_sink) vsink(isink,1:ndim) = 0.0
           xsink(isink,1:ndim)=xsink(isink,1:ndim)+vsink(isink,1:ndim)*dtnew(ilevel)

           !DEBUG
!           write(*,"(A, 2I5, 6ES15.5)") "move sink", myid, isink, vsink(isink,1:ndim), xsink(isink,1:ndim)

!!! JMG
           ! If the sink particle moves based on an average velocity, we
           ! still need it to have the same position/velocity as one of
           ! the cloud particles.  Otherwise the sink won't be found during some later routines.
           ! Find the nearest cloud particle and
           ! re-assign its position and velocity.
           if(vsink_avg_cloud .or. sink_track_vcom .or. static_sink) then
              ! Did we find the cloud particle closest to the sink
              ! on this processor?  ind_sink_part is the index
              ! to the xp, mp, etc. arrays that gives this particle
              if(ind_sink_part(isink) > 0) then
                 do idim=1, ndim
!                    write(*,*) "Artificially moving cloud particle ", xp(ind_sink_part(isink),idim), xsink(isink,idim)
                    xp(ind_sink_part(isink), idim) = xsink(isink,idim)
                    vp(ind_sink_part(isink), idim) = vsink(isink,idim)
                 enddo
              endif
           endif
!!!JMG

        endif
     end do
  endif
  
111 format('   Entering move_fine for level ',I2)

end subroutine move_fine
!#########################################################################
!#########################################################################
!#########################################################################
!#########################################################################
subroutine move1(ind_grid,ind_part,ind_grid_part,ng,np,ilevel)
  use amr_commons
  use pm_commons
  use poisson_commons
  use hydro_commons, ONLY: uold
  implicit none
  integer::ng,np,ilevel
  integer,dimension(1:nvector)::ind_grid
  integer,dimension(1:nvector)::ind_grid_part,ind_part
  !------------------------------------------------------------
  ! This routine computes the force on each particle by
  ! inverse CIC and computes new positions for all particles.
  ! If particle sits entirely in fine level, then CIC is performed
  ! at level ilevel. Otherwise, it is performed at level ilevel-1.
  ! This routine is called by move_fine.
  !------------------------------------------------------------
  logical::error
  integer::i,j,ind,idim,nx_loc,isink
  real(dp)::dx,length,dx_loc,scale,vol_loc,r2
  ! Grid-based arrays
  integer ,dimension(1:nvector),save::father_cell
  real(dp),dimension(1:nvector,1:ndim),save::x0
  integer ,dimension(1:nvector,1:threetondim),save::nbors_father_cells
  integer ,dimension(1:nvector,1:twotondim),save::nbors_father_grids
  ! Particle-based arrays
  logical ,dimension(1:nvector),save::ok
  real(dp),dimension(1:nvector,1:ndim),save::x,ff,new_xp,new_vp,dd,dg
  integer ,dimension(1:nvector,1:ndim),save::ig,id,igg,igd,icg,icd
  real(dp),dimension(1:nvector,1:twotondim),save::vol
  integer ,dimension(1:nvector,1:twotondim),save::igrid,icell,indp,kg
  real(dp),dimension(1:3)::skip_loc

  ! Mesh spacing in that level
  dx=0.5D0**ilevel 
  nx_loc=(icoarse_max-icoarse_min+1)
  skip_loc=(/0.0d0,0.0d0,0.0d0/)
  if(ndim>0)skip_loc(1)=dble(icoarse_min)
  if(ndim>1)skip_loc(2)=dble(jcoarse_min)
  if(ndim>2)skip_loc(3)=dble(kcoarse_min)
  scale=boxlen/dble(nx_loc)
  dx_loc=dx*scale
  vol_loc=dx_loc**3

  ! Lower left corner of 3x3x3 grid-cube
  do idim=1,ndim
     do i=1,ng
        x0(i,idim)=xg(ind_grid(i),idim)-3.0D0*dx
     end do
  end do
  
  ! Gather neighboring father cells (should be present anytime !)
  do i=1,ng
     father_cell(i)=father(ind_grid(i))
  end do
  call get3cubefather(father_cell,nbors_father_cells,nbors_father_grids,&
       & ng,ilevel)

  ! Rescale particle position at level ilevel
  do idim=1,ndim
     do j=1,np
        x(j,idim)=xp(ind_part(j),idim)/scale+skip_loc(idim)
     end do
  end do
  do idim=1,ndim
     do j=1,np
        x(j,idim)=x(j,idim)-x0(ind_grid_part(j),idim)
     end do
  end do
  do idim=1,ndim
     do j=1,np
        x(j,idim)=x(j,idim)/dx
     end do
  end do

  ! Check for illegal moves
  error=.false.
  do idim=1,ndim
     do j=1,np
        if(x(j,idim)<0.5D0.or.x(j,idim)>5.5D0)error=.true.
     end do
  end do
  if(error)then
     write(*,*)'problem in move'
     do idim=1,ndim
        do j=1,np
           if(x(j,idim)<0.5D0.or.x(j,idim)>5.5D0)then
              write(*,*)x(j,1:ndim)
           endif
        end do
     end do
     stop
  end if

  ! CIC at level ilevel (dd: right cloud boundary; dg: left cloud boundary)
  do idim=1,ndim
     do j=1,np
        dd(j,idim)=x(j,idim)+0.5D0
        id(j,idim)=dd(j,idim)
        dd(j,idim)=dd(j,idim)-id(j,idim)
        dg(j,idim)=1.0D0-dd(j,idim)
        ig(j,idim)=id(j,idim)-1
     end do
  end do

   ! Compute parent grids
  do idim=1,ndim
     do j=1,np
        igg(j,idim)=ig(j,idim)/2
        igd(j,idim)=id(j,idim)/2
     end do
  end do
#if NDIM==1
  do j=1,np
     kg(j,1)=1+igg(j,1)
     kg(j,2)=1+igd(j,1)
  end do
#endif
#if NDIM==2
  do j=1,np
     kg(j,1)=1+igg(j,1)+3*igg(j,2)
     kg(j,2)=1+igd(j,1)+3*igg(j,2)
     kg(j,3)=1+igg(j,1)+3*igd(j,2)
     kg(j,4)=1+igd(j,1)+3*igd(j,2)
  end do
#endif
#if NDIM==3
  do j=1,np
     kg(j,1)=1+igg(j,1)+3*igg(j,2)+9*igg(j,3)
     kg(j,2)=1+igd(j,1)+3*igg(j,2)+9*igg(j,3)
     kg(j,3)=1+igg(j,1)+3*igd(j,2)+9*igg(j,3)
     kg(j,4)=1+igd(j,1)+3*igd(j,2)+9*igg(j,3)
     kg(j,5)=1+igg(j,1)+3*igg(j,2)+9*igd(j,3)
     kg(j,6)=1+igd(j,1)+3*igg(j,2)+9*igd(j,3)
     kg(j,7)=1+igg(j,1)+3*igd(j,2)+9*igd(j,3)
     kg(j,8)=1+igd(j,1)+3*igd(j,2)+9*igd(j,3)
  end do
#endif
  do ind=1,twotondim
     do j=1,np
        igrid(j,ind)=son(nbors_father_cells(ind_grid_part(j),kg(j,ind)))
     end do
  end do

  ! Check if particles are entirely in level ilevel
  ok(1:np)=.true.
  do ind=1,twotondim
     do j=1,np
        ok(j)=ok(j).and.igrid(j,ind)>0
     end do
  end do

  ! If not, rescale position at level ilevel-1
  do idim=1,ndim
     do j=1,np
        if(.not.ok(j))then
           x(j,idim)=x(j,idim)/2.0D0
        end if
     end do
  end do
  ! If not, redo CIC at level ilevel-1
  do idim=1,ndim
     do j=1,np
        if(.not.ok(j))then
           dd(j,idim)=x(j,idim)+0.5D0
           id(j,idim)=dd(j,idim)
           dd(j,idim)=dd(j,idim)-id(j,idim)
           dg(j,idim)=1.0D0-dd(j,idim)
           ig(j,idim)=id(j,idim)-1
        end if
     end do
  end do

 ! Compute parent cell position
  do idim=1,ndim
     do j=1,np
        if(ok(j))then
           icg(j,idim)=ig(j,idim)-2*igg(j,idim)
           icd(j,idim)=id(j,idim)-2*igd(j,idim)
        else
           icg(j,idim)=ig(j,idim)
           icd(j,idim)=id(j,idim)
        end if
     end do
  end do
#if NDIM==1
  do j=1,np
     icell(j,1)=1+icg(j,1)
     icell(j,2)=1+icd(j,1)
  end do
#endif
#if NDIM==2
  do j=1,np
     if(ok(j))then
        icell(j,1)=1+icg(j,1)+2*icg(j,2)
        icell(j,2)=1+icd(j,1)+2*icg(j,2)
        icell(j,3)=1+icg(j,1)+2*icd(j,2)
        icell(j,4)=1+icd(j,1)+2*icd(j,2)
     else
        icell(j,1)=1+icg(j,1)+3*icg(j,2)
        icell(j,2)=1+icd(j,1)+3*icg(j,2)
        icell(j,3)=1+icg(j,1)+3*icd(j,2)
        icell(j,4)=1+icd(j,1)+3*icd(j,2)
     end if
  end do
#endif
#if NDIM==3
  do j=1,np
     if(ok(j))then
        icell(j,1)=1+icg(j,1)+2*icg(j,2)+4*icg(j,3)
        icell(j,2)=1+icd(j,1)+2*icg(j,2)+4*icg(j,3)
        icell(j,3)=1+icg(j,1)+2*icd(j,2)+4*icg(j,3)
        icell(j,4)=1+icd(j,1)+2*icd(j,2)+4*icg(j,3)
        icell(j,5)=1+icg(j,1)+2*icg(j,2)+4*icd(j,3)
        icell(j,6)=1+icd(j,1)+2*icg(j,2)+4*icd(j,3)
        icell(j,7)=1+icg(j,1)+2*icd(j,2)+4*icd(j,3)
        icell(j,8)=1+icd(j,1)+2*icd(j,2)+4*icd(j,3)
     else
        icell(j,1)=1+icg(j,1)+3*icg(j,2)+9*icg(j,3)
        icell(j,2)=1+icd(j,1)+3*icg(j,2)+9*icg(j,3)
        icell(j,3)=1+icg(j,1)+3*icd(j,2)+9*icg(j,3)
        icell(j,4)=1+icd(j,1)+3*icd(j,2)+9*icg(j,3)
        icell(j,5)=1+icg(j,1)+3*icg(j,2)+9*icd(j,3)
        icell(j,6)=1+icd(j,1)+3*icg(j,2)+9*icd(j,3)
        icell(j,7)=1+icg(j,1)+3*icd(j,2)+9*icd(j,3)
        icell(j,8)=1+icd(j,1)+3*icd(j,2)+9*icd(j,3)   
     end if
  end do
#endif
        
  ! Compute parent cell adresses
  do ind=1,twotondim
     do j=1,np
        if(ok(j))then
           indp(j,ind)=ncoarse+(icell(j,ind)-1)*ngridmax+igrid(j,ind)
        else
           indp(j,ind)=nbors_father_cells(ind_grid_part(j),icell(j,ind))
        end if
     end do
  end do

  ! Compute cloud volumes
#if NDIM==1
  do j=1,np
     vol(j,1)=dg(j,1)
     vol(j,2)=dd(j,1)
  end do
#endif
#if NDIM==2
  do j=1,np
     vol(j,1)=dg(j,1)*dg(j,2)
     vol(j,2)=dd(j,1)*dg(j,2)
     vol(j,3)=dg(j,1)*dd(j,2)
     vol(j,4)=dd(j,1)*dd(j,2)
  end do
#endif
#if NDIM==3
  do j=1,np
     vol(j,1)=dg(j,1)*dg(j,2)*dg(j,3)
     vol(j,2)=dd(j,1)*dg(j,2)*dg(j,3)
     vol(j,3)=dg(j,1)*dd(j,2)*dg(j,3)
     vol(j,4)=dd(j,1)*dd(j,2)*dg(j,3)
     vol(j,5)=dg(j,1)*dg(j,2)*dd(j,3)
     vol(j,6)=dd(j,1)*dg(j,2)*dd(j,3)
     vol(j,7)=dg(j,1)*dd(j,2)*dd(j,3)
     vol(j,8)=dd(j,1)*dd(j,2)*dd(j,3)
  end do
#endif
  
  ! Gather 3-force
  ff(1:np,1:ndim)=0.0D0
  if(tracer.and.hydro)then
     do ind=1,twotondim
        do idim=1,ndim
           do j=1,np
              ff(j,idim)=ff(j,idim)+uold(indp(j,ind),idim+1)*vol(j,ind)
           end do
        end do
     end do
  endif
  if(poisson)then
     do ind=1,twotondim
        do idim=1,ndim
           do j=1,np
              ff(j,idim)=ff(j,idim)+f(indp(j,ind),idim)*vol(j,ind)
           end do
        end do
     end do
  endif

  ! Update velocity
  do idim=1,ndim
     if(static.or.tracer)then
        do j=1,np
           new_vp(j,idim)=ff(j,idim)
        end do
     else
        do j=1,np
           new_vp(j,idim)=vp(ind_part(j),idim)+ff(j,idim)*0.5D0*dtnew(ilevel)
        end do
     endif
     ! For sink cloud particle, overwrite velocity with sink velocity
     if(sink)then
        do j=1,np
           isink=-idp(ind_part(j))
           if(isink>0 .and. isink < nsinkmax) then

!JMG
              ! Explicit dynamical friction for black hole particles.
              ! Add the dynmical friction acceleration to the existing
              ! acceleration.
              if(bh_dynamical_friction) then
                    ff(j,idim) = ff(j,idim) + fdyn_accel(isink,idim)
              end if

!! JMG
              ! need to adjust the acceleration by replacing the true sink
              ! mass with the fake one specified by msink_pseudo_dyn.  To do
              ! so, we multiply by (true_sink_mass / msink_pseudo_dyn).
              if(msink_pseudo_dyn > 0.0) then
                 new_vp(j,idim)=vsink(isink,idim)+ff(j,idim)*0.5D0*dtnew(ilevel) * &
                      (msink(isink)/msink_pseudo_dyn)
! FOR DEBUG
!!!                 write(*,*) "Msink/msink_pseudo_dyn", j, vsink(isink,idim), msink(isink) / msink_pseudo_dyn
! END DEBUG
              else   !!! NORMAL BEHAVIOR
                 new_vp(j,idim)=vsink(isink,idim)+ff(j,idim)*0.5D0*dtnew(ilevel)
              endif
!! end JMG
           endif
        end do
     end if
  end do
  ! For sink cloud particle, overwrite mass with sink mass
  if(sink)then
     do j=1,np
        isink=-idp(ind_part(j))
        if(isink>0 .and. isink < nsinkmax)then
           mp(ind_part(j))=msink(isink)/dble(ncloud_sink)
           if(msink_dynamical > 0.0) mp(ind_part(j)) = msink_dynamical/dble(ncloud_sink)
        endif
     end do
  end if
  do idim=1,ndim
     do j=1,np
        vp(ind_part(j),idim)=new_vp(j,idim)
     end do
  end do

  ! Update sink particle velocity using closest cloud particle
  if(sink)then
!!!JMG
!!$     if(vsink_avg_cloud) vcloud_new = 0.0
!!!JMG

     do j=1,np
        isink=-idp(ind_part(j))
        if(isink>0 .and. isink < nsinkmax)then
           r2=(xp(ind_part(j),1)-xsink(isink,1))**2
#if NDIM>1
           r2=(xp(ind_part(j),2)-xsink(isink,2))**2+r2
#endif
#if NDIM>2
           r2=(xp(ind_part(j),3)-xsink(isink,3))**2+r2
#endif
           if(r2==0.0)then
              vsink_new(isink,1:ndim)=vp(ind_part(j),1:ndim)
              oksink_new(isink)=1.0

              if(vsink_avg_cloud .or. sink_track_vcom) ind_sink_part(isink) = ind_part(j)

           end if
!!!JMG
!!$           ! sink velocity will be average of cloud particle velocities.
!!$           ! We must add up cloud particle velocities here
!!$           if(vsink_avg_cloud) then
!!$              vcloud_new(isink,1:ndim)= vcloud_new(isink,1:ndim) + vp(ind_part(j),1:ndim)
!!$           end if
!!!JMG
        endif
     end do
  end if

  ! Update position
  do idim=1,ndim
     if(static)then
        do j=1,np
           new_xp(j,idim)=xp(ind_part(j),idim)
        end do
     else
        do j=1,np
           new_xp(j,idim)=xp(ind_part(j),idim)+new_vp(j,idim)*dtnew(ilevel)
        end do
     endif
  end do
  do idim=1,ndim
     do j=1,np
        xp(ind_part(j),idim)=new_xp(j,idim)
     end do
  end do

end subroutine move1
!#########################################################################
!#########################################################################
!#########################################################################
!#########################################################################
