subroutine cooling_fine(ilevel)
  use amr_commons
  use hydro_commons
  use cooling_module
  implicit none
#ifndef WITHOUTMPI
  include 'mpif.h'
#endif
  integer::ilevel
  !-------------------------------------------------------------------
  ! Compute cooling for fine levels
  !-------------------------------------------------------------------
  integer::ncache,i,igrid,ngrid,info
  integer,dimension(1:nvector),save::ind_grid

  if(numbtot(1,ilevel)==0)return
  if(verbose)write(*,111)ilevel

  ! Compute sink accretion rates
  if(sink)call compute_accretion_rate(0)

  ! Operator splitting step for cooling source term
  ! by vector sweeps
  ncache=active(ilevel)%ngrid
  do igrid=1,ncache,nvector
     ngrid=MIN(nvector,ncache-igrid+1)
     do i=1,ngrid
        ind_grid(i)=active(ilevel)%igrid(igrid+i-1)
     end do
     call coolfine1(ind_grid,ngrid,ilevel)
  end do

  if(cooling.and.ilevel==levelmin.and.cosmo)then
     if(myid==1)write(*,*)'Computing new cooling table'
     call set_table(dble(aexp))
  end if

111 format('   Entering cooling_fine for level',i2)

end subroutine cooling_fine


!###########################################################
!###########################################################
!###########################################################
!###########################################################
!!! FlorentR - Patch EOS
subroutine read_eos_params(eos_type, nH_H_cc_threshold, level_jeans)
  use amr_commons
  implicit none
  character(LEN=FILENAME_MAX)::infile
  character(len=32)::eos_type !'isothermal', 'pseudo_cooling', 'self_shielding', 'self_shielding_floor', 'gamma_support'
  real(dp)::nH_H_cc_threshold
  integer::level_jeans
!!!  integer::jeans_polytrope=1
  
  !--------------------------------------------------
  ! Namelist definitions
  !--------------------------------------------------
  ! jeans_polytrope added by JMG
  namelist/eos_params/eos_type, nH_H_cc_threshold, level_jeans,&
       & jeans_polytrope
  
  CALL getarg(1,infile)
  open(1,file=infile)
  rewind(1)
  read(1,NML=eos_params,END=105)
105 continue
  close(1)

end subroutine read_eos_params
!!! FRenaud

!###########################################################
!###########################################################
!###########################################################
!###########################################################
subroutine coolfine1(ind_grid,ngrid,ilevel)
  use amr_commons
  use hydro_commons
!!! FlorentR - PATCH FBSS
  use pm_commons
!!! FRenaud  
#ifdef ATON
  use radiation_commons, ONLY: Erad
#endif
  use cooling_module
  implicit none
  integer::ilevel,ngrid
  integer,dimension(1:nvector)::ind_grid
!!! FlorentR - PATCH EOS
  logical, save::init_nml=.false.
  character(len=32), save::eos_type='isothermal'  
  real(dp), save::nH_H_cc_threshold=10.0D0
  integer, save::level_jeans=0
!!! FRenaud
  !-------------------------------------------------------------------
  !-------------------------------------------------------------------
  integer::i,ind,iskip,idim,nleaf,nx_loc,ix,iy,iz
  real(dp)::scale_nH,scale_T2,scale_l,scale_d,scale_t,scale_v
  real(kind=8)::dtcool,nISM,nCOM,damp_factor,cooling_switch,t_blast
  real(dp)::polytropic_constant
  integer,dimension(1:nvector),save::ind_cell,ind_leaf
  real(kind=8),dimension(1:nvector),save::nH,T2,delta_T2,ekk
  real(kind=8),dimension(1:nvector),save::T2min,Zsolar,boost
  real(dp),dimension(1:3)::skip_loc
  real(kind=8)::dx,dx_loc,scale,vol_loc
!!! FlorentR - PATCH FBSS
  real(kind=8)::dx_loc2
  real(dp),dimension(1:nvector,1:ndim),save::xx
  real(dp),dimension(1:twotondim, ndim) :: xc
  integer::iHII
  real(dp)::HII_dr2,HII_r2,dxcell,ionfactor, ri
!!! FRenaud
!!! JMG
!  real(dp)::t_ff_factor
  integer :: levelmax_loc
  real(dp), dimension(1:nvector) :: T2max
  real(dp) :: interp_factor

  interface
     logical function in_zoom_region(position, ilevel)
       use amr_commons
       implicit none
       real(dp), DIMENSION(ndim), INTENT(in) :: position
       integer, INTENT(in) :: ilevel
     end function in_zoom_region
     real(dp) function get_poly_const(my_levelmax, extra)
       use amr_commons
       implicit none
       integer, INTENT(in) :: my_levelmax
       real(dp), OPTIONAL, INTENT(in) :: extra
     end function get_poly_const
     real(dp) function get_levelmax_loc( position, ilevel )
       use amr_commons
       implicit none
       real(dp), INTENT(in), DIMENSION(ndim) :: position
       integer, INTENT(in) :: ilevel
     end function get_levelmax_loc
     real(dp) function buffer_interp_factor(position, ilevel, levelmax_zoom)
       use amr_commons
       implicit none       
       real(dp), INTENT(in), DIMENSION(ndim) :: position
       integer, INTENT(in) :: ilevel
       integer, INTENT(in) :: levelmax_zoom
     end function buffer_interp_factor
  end interface

!!! JMG

  ! Mesh spacing in that level
  dx=0.5D0**ilevel 
  nx_loc=(icoarse_max-icoarse_min+1)
  skip_loc=(/0.0d0,0.0d0,0.0d0/)
  if(ndim>0)skip_loc(1)=dble(icoarse_min)
  if(ndim>1)skip_loc(2)=dble(jcoarse_min)
  if(ndim>2)skip_loc(3)=dble(kcoarse_min)
  scale=boxlen/dble(nx_loc)
  dx_loc=dx*scale
  vol_loc=dx_loc**ndim

  ! Conversion factor from user units to cgs units
  call units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)

!!! FlorentR - PATCH EOS
  ! Read user-defined EOS parameters in the namelist
  if (.not. init_nml) then
    call read_eos_params(eos_type, nH_H_cc_threshold, level_jeans)
    init_nml = .true.
  end if
!!! FRenaud

  ! Typical ISM density in H/cc
  nISM = n_star; nCOM=0d0
  if(cosmo)then
     nCOM = del_star*omega_b*rhoc*(h0/100.)**2/aexp**3*X/mH
  endif
  nISM = MAX(nCOM,nISM)

  ! Polytropic constant for Jeans length related polytropic EOS
  if(jeans_ncells>0)then

     ! if level_jeans is different from levelmax, use level_jeans
     if(level_jeans .EQ. 0) then
        levelmax_loc = nlevelmax
     else
        levelmax_loc = level_jeans
     endif

     ! find global polytropic constant.
     ! For zoom simulations, we have to re-calculate it later for
     ! each cell separately, depending on whether it's in the 
     ! zoom region or not.
     polytropic_constant = get_poly_const(levelmax_loc)

  endif

  ! Find position of 8 cells relative to parent grid at this level
  ! This populates the array "xc"
  ! This is only used for a) HII feedback and b) zoom simulations
  call cell_position_relative_to_grid(ilevel, xc)

  ! Loop over cells
  do ind=1,twotondim
!!! FlorentR - PATCH FBSS
     scale=boxlen/dble(nx_loc) ! force scale to be in the right units in order to compute xx
!!! FRenaud

     iskip=ncoarse+(ind-1)*ngridmax
     do i=1,ngrid
        ind_cell(i)=iskip+ind_grid(i)
     end do

     ! Gather leaf cells
     nleaf=0
     do i=1,ngrid
        if(son(ind_cell(i))==0)then
           nleaf=nleaf+1
           ind_leaf(nleaf)=ind_cell(i)
           ! leaf cell position
           do idim=1,ndim
              xx(nleaf,idim)=(xg(ind_grid(i),idim)+xc(ind,idim)-skip_loc(idim))*scale
           end do
        end if
     end do

     ! Compute rho
     do i=1,nleaf
        nH(i)=MAX(uold(ind_leaf(i),1),smallr)
     end do
     
     ! Compute metallicity in solar units
     if(metal)then
        do i=1,nleaf
           Zsolar(i)=uold(ind_leaf(i),imetal)/nH(i)/0.02
        end do
     else
        do i=1,nleaf
           Zsolar(i)=z_ave
        end do
     endif

     ! Compute pressure
     do i=1,nleaf
        T2(i)=uold(ind_leaf(i),ndim+2)
     end do
     do i=1,nleaf
        ekk(i)=0.0d0
     end do
     do idim=1,ndim
        do i=1,nleaf
           ! Is velocity larger than check_velocity, or is density negative?
           if(check_velocity > 0.0) then
              if(abs(uold(ind_leaf(i),idim+1))/uold(ind_leaf(i),1) &
                   * scale_v / 1e5 > check_velocity &
                   .or. uold(ind_leaf(i),1)*scale_nH < 1e-8) then
                 !              write(*,*) "BAD VELOCITY", uold(ind_leaf(i),idim+1)/uold(ind_leaf(i),1)*scale_v/1e5
                 call fix_bad_velocity(ind_leaf(i), scale_v, scale_T2, scale_nH)
              endif
           endif  ! check_velocity

           ekk(i)=ekk(i)+0.5*uold(ind_leaf(i),idim+1)**2/nH(i)
        end do
     end do

     do i=1,nleaf
        T2(i)=(gamma-1.0)*(T2(i)-ekk(i))
     end do

     ! Compute T2=T/mu in Kelvin
     do i=1,nleaf
        T2(i)=T2(i)/nH(i)*scale_T2
     end do

     ! Compute nH in H/cc
     do i=1,nleaf
        nH(i)=nH(i)*scale_nH
     end do

     ! Compute radiation boost factor
     if(self_shielding)then
        do i=1,nleaf
           boost(i)=exp(-nH(i)/0.01)
        end do
#ifdef ATON
     else if (aton) then
        do i=1,nleaf
           boost(i)=MAX(Erad(ind_leaf(i))/J0simple(aexp), &
                &                   J0min/J0simple(aexp) )
        end do
#endif
     else
        do i=1,nleaf
           boost(i)=1.0
        end do
     endif

     !==========================================
     ! Compute temperature from polytrope EOS
     !==========================================
     if(jeans_ncells>0)then
        do i=1,nleaf
           ! if levelmax_lowres is set (for zooms), 
           ! then polytropic_constant (i.e. Tjeans or the Jeans polytrope)
           ! will vary with position (inside the zoom or outside).
           ! We have to find whether
           ! this cell is within the zoom region, and if not, take
           ! the lower of level_jeans and levelmax_lowres
           if( r_refine(ilevel) > 0.0 .and. (jeans_nozoom.eqv. .false.)) then 
              if(levelmax_lowres > 0) then
                 
                 ! find levelmax at this cell's position, xx
                 !!! debug
                 if(xx(i,1) < 0.0 .or. xx(i,1) > boxlen) WRITE(*,*) "*****error coolfine1: 1", xx(i,1:3)
                 levelmax_loc = get_levelmax_loc( xx(i,1:3), ilevel)
                 ! Don't let levelmax_loc ever be above level_jeans
                 ! assumes level_jeans > 0!!!
                 levelmax_loc = min(levelmax_loc, level_jeans)  
                 
                 ! Find the "interpolation factor" for interpolating Tjeans between 
                 ! the zoom and non-zoom regions.
                 ! NB: Here we use levelmax_loc as an indicator of whether this cell
                 ! is within the zoom region.  If rbuffer > rzoom, then we want to 
                 ! interpolate only for cells outside the zoom region.  If
                 ! rbuffer < rzoom, then we interpolate only for cells inside the zoom
                 ! region.
                 interp_factor = 1.0
                 if(r_buffer(ilevel) > 0.0) then
                   if(r_buffer(ilevel) > r_refine(ilevel)) then
                     if(levelmax_loc < level_jeans) then  ! outside of zoom region?
                       interp_factor = buffer_interp_factor( xx(i,1:3), ilevel, level_jeans)
                     endif
                   endif
                   if(r_buffer(ilevel) < r_refine(ilevel))then 
                     if(levelmax_loc == level_jeans) then   ! inside zoom region?
                       interp_factor = buffer_interp_factor( xx(i,1:3), ilevel, level_jeans)
                     endif
                   endif
                endif

                 ! calc polytropic constant for this levelmax
                 polytropic_constant = get_poly_const( levelmax_loc, interp_factor )
              endif
           endif

           ! The normal, no-zoom case
           T2min(i) = nH(i)*polytropic_constant*scale_T2
        end do
     else
        do i=1,nleaf
!!! FR: Gamma support here ?
           T2min(i) = T2_star*(nH(i)/nISM)**(g_star-1.0)
        end do
     endif
     !==========================================
     ! You can put your own polytrope EOS here
     !==========================================

!!! FlorentR - PATCH FBSS
  !------------------------------------------------------------------------
  ! This part checks weither the i-th cell is inside an HII region, according to
  ! the list built by ionizing_sources.
  ! If yes, the temperature is set to a fixed value and the EOS is *not* applied.
  !
  ! Florent Renaud - May 2011
  !------------------------------------------------------------------------
     ! Mesh maximum resolution
     scale=boxlen/dble(icoarse_max-icoarse_min+1)*scale_l
     dx=0.5D0**(nlevelmax)
     dx_loc=dx*scale
     dx_loc2=0.5**(level_jeans)
     dx_loc2=dx_loc2*scale
     if(level_jeans .EQ. 0) dx_loc2=dx_loc


     !do i=1,nleaf ! old loop started here
     
     dxcell=boxlen*0.5D0**ilevel/dble(icoarse_max-icoarse_min+1)/2.0D0 ! half-size of the cell (code units)  
     do i=1,nleaf
       ionfactor=0.0D0
       
       if(nHII>0)then ! photoionization feedback

         !------------------------------------------------------
         ! Find weither the cell is inside an HII region
         !------------------------------------------------------ 
         
         ! Loop over ionizing sources
         do iHII=1, nHII
           ri = HII_xr(iHII,4) * HII_xr(iHII,6)
           !------------------------------------------------------
           ! Case of r_HII > lcell/2
           !------------------------------------------------------ 
           
           if(HII_xr(iHII,4)<0)then
             ! check if the source is inside the current cell
             if(abs(xx(i,1)-HII_xr(iHII,1))<dxcell)then
               if(abs(xx(i,2)-HII_xr(iHII,2))<dxcell)then
                 if(abs(xx(i,3)-HII_xr(iHII,3))<dxcell)then
                   ionfactor=ionfactor+4.0D0/3.0D0*dacos(-1.0D0)*ri**3/(dxcell*2.0D0)**3 ! volume_HII / volume_cell
                   ! exit the loop over ionizing sources
                   goto 555 ! exit
                 endif
               endif
             endif
           else

           !------------------------------------------------------
           ! Case of r_HII > lcell/2
           !------------------------------------------------------ 

             HII_r2 = ri**2
             ! check if the center of the current cell is inside the bubble created by the current source
             HII_dr2 = (xx(i,1)-HII_xr(iHII,1))**2
             if(HII_dr2<HII_r2)then
               HII_dr2 = HII_dr2 + (xx(i,2)-HII_xr(iHII,2))**2
               if(HII_dr2<HII_r2)then
                 HII_dr2 = HII_dr2 + (xx(i,3)-HII_xr(iHII,3))**2
                 if(HII_dr2<HII_r2)then
                   ! the cell is in one HII region
                   ionfactor=1.0D0
                   ! exit the loop over ionizing sources
                   goto 555 ! exit
                 endif
               endif
             endif

           endif
         end do
         ! End loop over ionizing sources
         555 continue

         !------------------------------------------------------
         ! Set the temperature
         !------------------------------------------------------ 
         
         if(ionfactor>0.0)then

            if(boost_HII_feedback) then 
               ! Just add the Jeans polytrope temperature to the feedback temperature.  This way
               ! we're never lower than Tjeans, and gas in HII regions will be above Tjeans
               T2min(i)=T2min(i) +  min(ionfactor,1.0D0)*abs(THII)
            endif

            ! JMG: max because we need to keep gas at least as hot as jeans polytrope temperature
            T2min(i)=max(T2min(i), min(ionfactor,1.0D0)*abs(THII) ) ! min might not be needed because of goto 555
!!!JMG
!!$           if(boost_HII_feedback) then 
!!$              ! if gas cell is on the Jeans polytrope, we want it to be a little hotter
!!$              ! than Tjeans for feedback to be doing anything
!!$              if( T2min(i) > abs(THII) ) then 
!!$                 T2min(i) = T2min(i) + abs(THII)
!!$              endif
!!$
!!$           endif
!!!end JMG
           ! do not apply EOS, check Jeans' criterion, and go to the next leaf cell (loop on i)
           goto 666 ! cycle
         endif
       endif
       !------------------------------------------------------
       ! If not in an HII region, apply EOS
       !------------------------------------------------------       
!!! FRenaud



!!! FlorentR - Patch EOS
       if(nH(i) .LT. 1.0D-3) then ! Low-density gamma=5/3 polytropic EOS 
         T2min(i) = max(T2min(i), 4.0D6*(nH(i) / 1.0D-3)**(gamma - 1.0D0))
       else
         select case (eos_type)
           case ('isothermal') ! Isothermal EOS
             T2min(i) = max(T2min(i), T2_star)
           case ('pseudo_cooling') ! Pseudo-coolong EOS
             if(nH(i) .LT. 10.0D0**(-0.5D0)) then ! Isothermal T2_star EOS
               T2min(i) = max(T2min(i), T2_star)
             else ! Cooling EOS
               T2min(i) = max(T2min(i), T2_star * (nH(i)/10.0D0**(-0.5D0))**(-1.0D0/2.0D0))
             end if
           case ('self_shielding') ! Self-shielding EOS
             ! from 1e-3 to 1e-1: Isothermal
             ! from 1e-1 to 1e1 : steep polytrop
             ! from 1e1 to Jeans polytrop : shallow polytrop
             if(nH(i) .LT. 1.0D-1) then ! Isothermal T2_star EOS
               T2min(i) = max(T2min(i), T2_star)
             else ! Self-shielding
               if(nH(i) .LT. 1.0D1) then ! steep
                 T2min(i) = max(T2min(i), T2_star * (nH(i)/1.0D-1)**(-1.0D0))
               else ! shallow
                 T2min(i) = max(T2min(i), T2_star * 1.0D-2 * (nH(i)/1.0D1)**(-1.0D0/5.0D0)) ! 1.0D-2 = (nH/1.0D-1)**(-1.0D0)  (see above) for nH = 1.0D1
               endif
             end if
           case ('self_shielding_floor') ! Self-shielding EOS with temperature floor
             ! from 1e-3 to 1e-1: Isothermal
             ! from 1e-1 to 1e1 : steep polytrop
             ! from 1e1 to Jeans polytrop : isothermal
             if(nH(i) .LT. 1.0D-1) then ! Isothermal T2_star EOS
               T2min(i) = max(T2min(i), T2_star)
             else ! Self-shielding
               if(nH(i) .LT. 1.0D1) then ! steep
                 T2min(i) = max(T2min(i), T2_star * (nH(i)/1.0D-1)**(-1.0D0))
               else ! isothermal
                 T2min(i) = max(T2min(i), T2_star * 1.0D-2)
               endif
             end if
           case ('gamma_support') ! Adiabatic gas
             if(nH(i) .LT. nH_H_cc_threshold) then ! Isothermal T2_star EOS
               T2min(i) = max(T2min(i), T2_star)
             else ! (Gamma-1) polytropic EOS
               T2min(i) = max(T2min(i), T2_star * (nH(i)/nH_H_cc_threshold)**(g_star-1.0D0))
             end if
           case default ! Isothermal EOS
             T2min(i) = max(T2min(i), T2_star)
         end select
       end if

!!! FlorentR - PATCH FBSS
666    continue ! goto next leaf cell
!!! FRenaud

!!! FR ?
       if(cooling)T2min(i)=T2min(i)+T2_min_fix
     end do
!!! FRenaud


     ! Compute cooling time step in second
     dtcool = dtnew(ilevel)*scale_t

     ! Compute net cooling at constant nH
     if(cooling)then
        ! Compute "thermal" temperature by substracting polytrope
        do i=1,nleaf
           T2(i) = max(T2(i)-T2min(i),T2_min_fix)
        end do
        call solve_cooling(nH,T2,Zsolar,boost,dtcool,delta_T2,nleaf)
     endif

     ! Compute rho
     do i=1,nleaf
        nH(i) = nH(i)/scale_nH
     end do

     ! Compute net energy sink
     if(cooling)then
        do i=1,nleaf
           delta_T2(i) = delta_T2(i)*nH(i)/scale_T2/(gamma-1.0)
        end do
        ! Turn off cooling in blast wave regions
        if(delayed_cooling)then
           do i=1,nleaf
              cooling_switch=uold(ind_leaf(i),idelay)/uold(ind_leaf(i),1)
              if(cooling_switch>1d-3)then
!!                 write(*,*) 'DELAY COOLING', cooling_switch, uold(ind_leaf(i),idelay)
                 delta_T2(i)=0
              endif
           end do
        endif
     endif

     ! Compute minimal total energy from polytrope
     do i=1,nleaf
        T2min(i) = T2min(i)*nH(i)/scale_T2/(gamma-1.0) + ekk(i)

!!! JMG
        if( Tmax_global > 0.0 ) then 
           T2max(i) = Tmax_global *nH(i)/scale_T2/(gamma-1.0) + ekk(i)
        endif
!!! end JMG
     end do

     ! Update total fluid energy
     do i=1,nleaf
        T2(i) = uold(ind_leaf(i),ndim+2)
     end do
     if(cooling)then
        do i=1,nleaf
           T2(i) = T2(i)+delta_T2(i)
        end do
     endif
     if(isothermal)then
        do i=1,nleaf
           uold(ind_leaf(i),ndim+2) = T2min(i)
        end do
     else
        do i=1,nleaf
           !! DEBUG
           if(T2(i) < 0.0) write(*,*) "BAD cell temp < 0", T2(i)
           if(T2min(i) < 0.0) write(*,*) "BAD min cell temp < 0!!!!!!", T2min(i)
           !! end DEBUG
           uold(ind_leaf(i),ndim+2) = max(T2(i),T2min(i))
           
!!!JMG 
           ! set maximum temperature -- no higher than Tmax(i)
           ! Ensure that we don't go below T2min.
           if( Tmax_global > 0.0 ) then
              uold(ind_leaf(i),ndim+2) = max(min(T2(i), T2max(i)), T2min(i))
           endif
!!! end JMG
        end do
     endif

     ! Update delayed cooling switch
     if(delayed_cooling)then
!!!JMG -- this is 20Myrs        t_blast=20d0*1d6*(365.*24.*3600.)
        ! For high res, let's try 2 Myrs instead
        t_blast= delaycool_time*1d6*(365.*24.*3600.)
        
        damp_factor=exp(-dtcool/t_blast)
        do i=1,nleaf
! Adaptive cooling delay by JMG
           if(adaptive_cooling_delay) then
              ! First parenthetical expression is 1Myr in seconds
              ! Second expression is a density scaling, scaled to 1.0 H/cc
              ! This is inspired by Stinson et al. 2006, but it's different
              t_blast = (1d6*(365.*24.*3600.)) * (uold(ind_leaf(i),1)*scale_nH / 1.0)**(0.35)
              damp_factor = exp(-dtcool/t_blast)
           endif

 
          uold(ind_leaf(i),idelay)=uold(ind_leaf(i),idelay)*damp_factor
        end do
     endif

  end do
  ! End loop over cells

end subroutine coolfine1



!###########################################################
!###########################################################
!###########################################################
!###########################################################
!!! JMG 
!!! position -- a 3-element array giving the x,y,z position
!!!     of interest.  The units of position should be
!!!     xg(ind_grid(i))*scale, i.e. the position in kpc
!!!     
logical function in_zoom_region( position, ilevel )
  use amr_commons
  implicit none

  real(dp), INTENT(in), DIMENSION(ndim) :: position
  integer, INTENT(in) :: ilevel
  !--------------------------------------------------
  ! Determine whether a point is within the zoom region
  ! defined by the user.  Copied from "geometry_refine"
  ! in flag_utils.f90
  !--------------------------------------------------
  real(dp)::er,xr,yr,zr,rr,xn,yn,zn,r,aa,bb
  integer ::i

  ! grab variables from amr_commons
  er=exp_refine(ilevel) ! Exponent defining norm
  xr=x_refine  (ilevel) ! Region centre
  yr=y_refine  (ilevel)
  zr=z_refine  (ilevel)
  rr=r_refine  (ilevel) ! Region DIAMETER (beware !)
  aa=a_refine  (ilevel) ! Ellipticity (Y/X)
  bb=b_refine  (ilevel) ! Ellipticity (Z/X)

! DEBUG
  if(position(1) > boxlen .or. position(1) < 0.0) &
       WRITE(*,*) "in_zoom_region problem: ", position

  ! calculate the radius normalized to the zoom region radius
  ! (actually working with diameters)
  xn=0.0d0; yn=0.0d0; zn=0.0d0
  xn=2.0d0*abs(position(1)-xr)/rr
#if NDIM > 1
  yn=2.0d0*abs(position(2)-yr)/(aa*rr)
#endif
#if NDIM >2
  zn=2.0d0*abs(position(3)-zr)/(bb*rr)
#endif

  ! er=2 for normal flat space
  if(er<10)then
     r=(xn**er+yn**er+zn**er)**(1.0/er)
  else
     r=max(xn,yn,zn)
  end if
  
  ! return .true. if within the zoom region
  if(r < 1.0) then
     in_zoom_region = .true.
  else
     in_zoom_region = .false.
  endif

end function in_zoom_region

!###########################################################
!###########################################################
!###########################################################
!###########################################################
!!! JMG 
!!! Calculate the polytropic constant for any given levelmax
!!!
!!!   extra_factor -- multiply the result times this factor
!!!
real(dp) function get_poly_const( my_levelmax, extra_factor )
  use amr_commons
  use cooling_module, ONLY: twopi
  implicit none

!!!  real(dp) :: get_poly_constant
  integer, INTENT(in) :: my_levelmax
  real(dp), OPTIONAL, INTENT(in) :: extra_factor
  !---------------------------------------------------------
  ! Get the polytropic constant based on a "local" value of 
  ! levelmax.
  !---------------------------------------------------------
  real(dp) :: polytropic_constant
  real(dp)::scale_nH,scale_T2,scale_l,scale_d,scale_t,scale_v
  real(dp) :: t_ff_factor, extra

  ! Conversion factor from user units to cgs units
  call units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)

  ! Is the optional "extra_factor" argument present?
  if(present(extra_factor) ) then
     extra = extra_factor
  else
     extra = 1.0
  endif

  ! User may  choose the effective polytrope using "jeans_polytrope"
  ! NB this t_ff factor is not quite the same as the one in plot_eos... there's
  ! an extra factor of sqrt(32.) to worry about
  select case (jeans_polytrope) !from JMG
  case (1)
     t_ff_factor = 1.0
     polytropic_constant=2d0* &
          (boxlen*jeans_ncells*0.5d0**dble(my_levelmax)*scale_l/aexp)**2/ &
          & (twopi)*6.67e-8*scale_d*(scale_t/scale_l)**2 * t_ff_factor * extra

  case (2)
     t_ff_factor = 1.0 / sqrt(32.)
     polytropic_constant=2d0* &
          (boxlen*jeans_ncells*0.5d0**dble(my_levelmax)*scale_l/aexp)**2/ &
          & (twopi)*6.67e-8*scale_d*(scale_t/scale_l)**2 * t_ff_factor * extra

  case (3)
     t_ff_factor = 1.0 / sqrt(32.) * (3.0/ 32.0)
     polytropic_constant=2d0* &
          (boxlen*jeans_ncells*0.5d0**dble(my_levelmax)*scale_l/aexp)**2/ &
          & (twopi)*6.67e-8*scale_d*(scale_t/scale_l)**2 * t_ff_factor * extra

  case (4)
     ! Totally different way.
     ! Consistent with refinement based on jeans criterion (jeans_refine)
     ! I think this is equivalent to case 1 divided by scale_nH
     polytropic_constant = &
          (jeans_ncells * boxlen * 0.5d0**dble(my_levelmax) / aexp)**2 / &
          (3.14159 * scale_nH)

  case default
     ! same as case 1
     t_ff_factor = 1.0
     polytropic_constant=2d0* &
          (boxlen*jeans_ncells*0.5d0**dble(my_levelmax)*scale_l/aexp)**2/ &
          & (twopi)*6.67e-8*scale_d*(scale_t/scale_l)**2 * t_ff_factor * extra

  end select

!!$  write(*,*) polytropic_constant, 2d0* &
!!$          (boxlen*jeans_ncells*0.5d0**dble(my_levelmax)*scale_l/aexp)**2/ &
!!$          & (twopi)*6.67e-8*scale_d*(scale_t/scale_l)**2, &
!!$          (boxlen*jeans_ncells*0.5d0**dble(my_levelmax)*scale_l/aexp)**2, &
!!$          boxlen, my_levelmax, scale_l, aexp
!!$

  
  get_poly_const = polytropic_constant
end function get_poly_const

!###########################################################
!###########################################################
!###########################################################
!###########################################################
!!! JMG 
!!! Determine the levelmax for a local position
!!! position must be units of kpc (e.g. xg(i) * scale)
!!!     
real(dp) function get_levelmax_loc( position, ilevel )
  use amr_commons
  use cooling_module, ONLY: twopi
  implicit none

  real(dp), INTENT(in), DIMENSION(ndim) :: position
  integer, INTENT(in) :: ilevel
  !---------------------------------------------------------
  ! Choose a value for levelmax_local
  !---------------------------------------------------------
  real(dp) :: my_levelmax
  real(dp)::scale_nH,scale_T2,scale_l,scale_d,scale_t,scale_v
  real(dp) :: t_ff_factor

  interface
     logical function in_zoom_region(position, ilevel)
       use amr_commons
       real(dp), DIMENSION(ndim), INTENT(in) :: position
       integer, INTENT(in) :: ilevel
     end function in_zoom_region
  end interface

  ! default levelmax
  my_levelmax = nlevelmax

  ! In case of zoom simulation, levelmax varies with position!
  ! We must prevent my_levelmax from being higher than the
  ! local levelmax
  if(r_refine(ilevel) > 0.0) then
     if(levelmax_lowres > 0) then
        ! find out where this grid is located
        if( in_zoom_region( position , ilevel) ) then
           ! do nothing
        else
           ! we're outside the zoom region.  Make sure
           ! levelmax_loc is not larger than levelmax_lowres
           if(my_levelmax > levelmax_lowres) my_levelmax = levelmax_lowres
        endif  !! in zoom region
     
     endif
  endif
  
  get_levelmax_loc = my_levelmax
end function get_levelmax_loc
!###########################################################
!###########################################################
!###########################################################
!###########################################################
!!! JMG 
!!!   Determine the 'buffer interpolation factor' which
!!!   interpolates Tjeans between the zoom and no-zoom
!!!   regions.  
!!!
!!! assume no ellipticity -- must be a sphere
real(dp) function buffer_interp_factor(position, ilevel, levelmax_zoom)
  use amr_commons
  use cooling_module, ONLY: twopi
  implicit none

  real(dp), INTENT(in), DIMENSION(ndim) :: position
  integer, INTENT(in) :: ilevel
  integer, INTENT(in) :: levelmax_zoom
  !---------------------------------------------------------
  ! Choose a value for levelmax_local
  !---------------------------------------------------------
  real(dp)::er,xr,yr,zr,rr,xn,yn,zn,r,aa,bb
  integer ::i
  real(dp) :: interp_factor, rbuf
  integer :: level_low

  ! grab variables from amr_commons
  xr=x_refine  (ilevel) ! Region centre
  yr=y_refine  (ilevel)
  zr=z_refine  (ilevel)
  rr=r_refine  (ilevel) / 2.0 ! Region RADIUS!!!
  rbuf = r_buffer(ilevel) / 2.0 ! Buffer region RADIUS

  ! calculate the radius of this cell
  xn = position(1) - xr
  yn = position(2) - yr
  zn = position(3) - zr
  r = sqrt(xn*xn + yn*yn + zn*zn)

  ! are we in the buffer zone?
  !  r has to be greater than r_inner=min(rbuf,rr) and less than
  !  r_outer = max(rbuf, rr)
  if( r > min(rbuf, rr) .and. r < max(rbuf, rr)) then 
!!  if( r > rr .and. r < rbuf ) then  ! assumes rbuf > rzoom
!!  if( r < rr .and. r > rbuf ) then  ! assumes rbuf < rzoom
     level_low = levelmax_lowres

     ! interpolation
     ! this assumes rbuf > r_refine, ie the buffer zone is
     ! outside the zoom region
     if(rbuf > rr) then 
        interp_factor = (r - rbuf) / (rbuf - rr) * &
             (1 - 0.5**(2.0*(levelmax_zoom - level_low))) &
             + 1.0
     endif

     ! this assumes rbuf < r_refine, ie the buffer zone is
     ! within the zoom region
     if(rbuf < rr) then 
        interp_factor = (1.0 / (rr - rbuf)) * &
             (rr - r + (r - rbuf)* 2.0**(2.0*(levelmax_zoom-level_low)) )
     endif

!!! DEBUG
!     if(r > 0.95) write(*,*) level_low, levelmax_zoom, interp_factor, rr, rbuf, r
  else
     interp_factor = 1.0
  endif

  buffer_interp_factor = interp_factor
end function buffer_interp_factor

!###########################################################
!###########################################################
!###########################################################
!###########################################################
!!! JMG 
!!! Determine the levelmax for a local position
!!! position must be units of kpc (e.g. xg(i) * scale)
!!!     
subroutine cell_position_relative_to_grid(ilevel, xc)
  use amr_commons, ONLY: twotondim, dp, ndim
  implicit none
  
  integer, INTENT(in) :: ilevel
  real(dp), DIMENSION(twotondim, ndim), INTENT(out) :: xc
  !---------------------------------------------------------
  ! Find position of cell centers relative to their parent
  ! grid position.
  !---------------------------------------------------------  
  integer :: ind, iz, iy, ix
  real(dp) :: dx

  ! cell size
  dx = 0.5D0**ilevel

  ! loop over 8 cells
  do ind=1, twotondim
     iz=(ind-1)/4
     iy=(ind-1-4*iz)/2
     ix=(ind-1-2*iy-4*iz)
     xc(ind,1)=(dble(ix)-0.5D0)*dx
     xc(ind,2)=(dble(iy)-0.5D0)*dx
     xc(ind,3)=(dble(iz)-0.5D0)*dx
  end do

end subroutine cell_position_relative_to_grid

!###########################################################
!###########################################################
!###########################################################
!###########################################################
!!! JMG
!!! Fix velocities that have gotten crazy
!!!
subroutine fix_bad_velocity(cell_index, scale_v, scale_T2, scale_nH)
  use amr_commons, ONLY: ndim, dp, check_velocity
  use hydro_commons, ONLY: uold, smallr, gamma
  implicit none

  integer, INTENT(in) :: cell_index
  real(dp), INTENT(in) :: scale_v, scale_T2, scale_nH
  !---------------------------------------------------------
  ! Find position of cell centers relative to their parent
  ! grid position.
  !---------------------------------------------------------
  integer :: idim
  real(dp) :: nH, ekk

  ! fix the gas density to some small value
!  uold(cell_index,1) = MAX(uold(cell_index,1),smallr)
  nH = uold(cell_index,1)

  ! fix the density
  if(nH * scale_nH < 1e-8) then
!     WRITE(*,*) "VERY LOW DENSITY", nH*scale_nH
     uold(cell_index, 1) = 1e-8 / scale_nH
  endif
  nH = MAX(uold(cell_index,1),smallr)

  ! fix the velocities to 1e4 km/s
  ekk = 0
  do idim=1, ndim
     if(abs(uold(cell_index,idim+1)/nH * scale_v/1e5) > check_velocity)  then
        ! velocity is 90% of 10^3 km/s in the same direction as before
        uold(cell_index, idim+1) = 0.9*check_velocity * 1e5/scale_v * nH * (uold(cell_index,idim+1)/abs(uold(cell_index,idim+1)))
     end if
     ekk = ekk + 0.5*uold(cell_index,idim+1)**2 / nH
  enddo

  ! fix the temperature (energy) to 1e5 Kelvin
  uold(cell_index, ndim+2) = 1e5 / scale_T2 * nH / (gamma-1.0) + ekk

end subroutine fix_bad_velocity !from JMG. not in Kintherm and kintherm_movie
