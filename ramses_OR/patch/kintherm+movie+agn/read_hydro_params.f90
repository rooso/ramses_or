subroutine read_hydro_params(nml_ok)
  use amr_commons
  use hydro_commons
  use pm_parameters, ONLY: ir_cloud
  implicit none
#ifndef WITHOUTMPI
  include 'mpif.h'
#endif
  logical::nml_ok
  !--------------------------------------------------
  ! Local variables  
  !--------------------------------------------------
  integer::i,idim,nboundary_true=0
  integer ,dimension(1:MAXBOUND)::bound_type
  real(dp)::scale,ek_bound

  !--------------------------------------------------
  ! Namelist definitions
  !--------------------------------------------------
  namelist/init_params/filetype,initfile,multiple,nregion,region_type &
       & ,x_center,y_center,z_center,aexp_ini &
       & ,length_x,length_y,length_z,exp_region &
       & ,d_region,u_region,v_region,w_region,p_region
  namelist/hydro_params/gamma,courant_factor,smallr,smallc &
       & ,niter_riemann,slope_type,difmag &
       & ,pressure_fix,beta_fix,scheme,riemann
  namelist/refine_params/x_refine,y_refine,z_refine,r_refine &
       & ,a_refine,b_refine,exp_refine,jeans_refine,mass_cut_refine &
       & ,m_refine,mass_sph,err_grad_d,err_grad_p,err_grad_u &
       & ,floor_d,floor_u,floor_p,ivar_refine,var_cut_refine &
       & ,interpol_var,interpol_type &
!!! JMG
       & , do_sink_refine &  
       & , levelmax_lowres, r_buffer, rand_buffer, zoom_sink
!!!end JMG
  namelist/boundary_params/nboundary,bound_type &
       & ,ibound_min,ibound_max,jbound_min,jbound_max &
       & ,kbound_min,kbound_max &
       & ,d_bound,u_bound,v_bound,w_bound,p_bound
  namelist/physics_params/cooling,haardt_madau,metal,isothermal,bondi &
       & ,m_star,t_star,n_star,T2_star,g_star,del_star,eps_star,jeans_ncells &
       & ,eta_sn,yield,rbubble,f_ek,ndebris,f_w,mass_gmc &
       & ,J21,a_spec,z_ave,z_reion,n_sink,bondi,delayed_cooling &
       & ,self_shielding,smbh,agn,rsink_max,msink_max &
       & ,units_density,units_time,units_length &
!!! JMG
       & , adaptive_cooling_delay &
       & , tdelay_SN, delaycool_time, ignore_delaycool_nvar_error &
       & , reset_accreted_mass, reset_accreted_mass_on_startup, expand_agn_blast &
       & , increase_rblast &
       & , do_accretion, check_smbh_formation_sites &
       & , lookup_acc_rate &
       & , Tmin_AGN, Tmax_AGN, beta_accretion &
       & , inject_agn_coldgas, level_agn, mass_weight_acc &
       & , msink_fix, static_sink, reposition_sink, reposition_sink_com &
       & , ir_cloud &   !!! This is in pm_parameters
       & , msink_dynamical, msink_pseudo_dyn &
       & , bh_dynamical_friction, fdyn_factor &
       & , radii_measure, do_measure_part_mass, sink_track_vcom, sink_track_com &
       & , vsink_avg_cloud, drag_simple &
       & , do_sink_nbody &
       & , Tmax_global, check_velocity &
       & , max_sf_eff &
!!! JMG
!!! FlorentR - PATCH NLEV_SF
       & ,nlev_sf &
!!! FRenaud
!!! JMG
       &, nHII_skip, boost_HII_feedback &
!!! end JMG
!!! FlorentR - PATCH FBSS
       & ,THII,rhominHII,rhodelayHII,multiscat
!!! RenaudF


  ! Read namelist file
  rewind(1)
  read(1,NML=init_params,END=101)
  goto 102
101 write(*,*)' You need to set up namelist &INIT_PARAMS in parameter file'
  call clean_stop
102 rewind(1)
  if(nlevelmax>levelmin)read(1,NML=refine_params)
  rewind(1)
  if(hydro)read(1,NML=hydro_params)
  rewind(1)
  read(1,NML=boundary_params,END=103)
  simple_boundary=.true.
  goto 104
103 simple_boundary=.false.
104 if(nboundary>MAXBOUND)then
    write(*,*) 'Error: nboundary>MAXBOUND'
    call clean_stop
  end if
  rewind(1)
  read(1,NML=physics_params,END=105)
105 continue
#ifdef ATON
  if(aton)call read_radiation_params(1)
#endif

  !--------------------------------------------------
  ! Check for star formation
  !--------------------------------------------------
  if(t_star>0)then
     star=.true.
     pic=.true.
  else if(eps_star>0)then
     t_star=0.1635449*(n_star/0.1)**(-0.5)/eps_star
     star=.true.
     pic=.true.
  endif

  !--------------------------------------------------
  ! Check for metal
  !--------------------------------------------------
  if(metal.and.nvar<(ndim+3))then
     if(myid==1)write(*,*)'Error: metals need nvar >= ndim+3'
     if(myid==1)write(*,*)'Modify hydro_parameters.f90 and recompile'
     nml_ok=.false.
  endif

  !-------------------------------------------------
  ! This section deals with hydro boundary conditions
  !-------------------------------------------------
  if(simple_boundary.and.nboundary==0)then
     simple_boundary=.false.
  endif

  if (simple_boundary)then

     ! Compute new coarse grid boundaries
     do i=1,nboundary
        if(ibound_min(i)*ibound_max(i)==1.and.ndim>0.and.bound_type(i)>0)then
           nx=nx+1
           if(ibound_min(i)==-1)then
              icoarse_min=icoarse_min+1
              icoarse_max=icoarse_max+1
           end if
           nboundary_true=nboundary_true+1
        end if
     end do
     do i=1,nboundary
        if(jbound_min(i)*jbound_max(i)==1.and.ndim>1.and.bound_type(i)>0)then
           ny=ny+1
           if(jbound_min(i)==-1)then
              jcoarse_min=jcoarse_min+1
              jcoarse_max=jcoarse_max+1
           end if
           nboundary_true=nboundary_true+1
        end if
     end do
     do i=1,nboundary
        if(kbound_min(i)*kbound_max(i)==1.and.ndim>2.and.bound_type(i)>0)then
           nz=nz+1
           if(kbound_min(i)==-1)then
              kcoarse_min=kcoarse_min+1
              kcoarse_max=kcoarse_max+1
           end if
           nboundary_true=nboundary_true+1
        end if
     end do

     ! Compute boundary geometry
     do i=1,nboundary
        if(ibound_min(i)*ibound_max(i)==1.and.ndim>0.and.bound_type(i)>0)then
           if(ibound_min(i)==-1)then
              ibound_min(i)=icoarse_min+ibound_min(i)
              ibound_max(i)=icoarse_min+ibound_max(i)
              if(bound_type(i)==1)boundary_type(i)=1
              if(bound_type(i)==2)boundary_type(i)=11
              if(bound_type(i)==3)boundary_type(i)=21
           else
              ibound_min(i)=icoarse_max+ibound_min(i)
              ibound_max(i)=icoarse_max+ibound_max(i)
              if(bound_type(i)==1)boundary_type(i)=2
              if(bound_type(i)==2)boundary_type(i)=12
              if(bound_type(i)==3)boundary_type(i)=22
           end if
           if(ndim>1)jbound_min(i)=jcoarse_min+jbound_min(i)
           if(ndim>1)jbound_max(i)=jcoarse_max+jbound_max(i)
           if(ndim>2)kbound_min(i)=kcoarse_min+kbound_min(i)
           if(ndim>2)kbound_max(i)=kcoarse_max+kbound_max(i)
        else if(jbound_min(i)*jbound_max(i)==1.and.ndim>1.and.bound_type(i)>0)then
           ibound_min(i)=icoarse_min+ibound_min(i)
           ibound_max(i)=icoarse_max+ibound_max(i)
           if(jbound_min(i)==-1)then
              jbound_min(i)=jcoarse_min+jbound_min(i)
              jbound_max(i)=jcoarse_min+jbound_max(i)
              if(bound_type(i)==1)boundary_type(i)=3
              if(bound_type(i)==2)boundary_type(i)=13
              if(bound_type(i)==3)boundary_type(i)=23
           else
              jbound_min(i)=jcoarse_max+jbound_min(i)
              jbound_max(i)=jcoarse_max+jbound_max(i)
              if(bound_type(i)==1)boundary_type(i)=4
              if(bound_type(i)==2)boundary_type(i)=14
              if(bound_type(i)==3)boundary_type(i)=24
           end if
           if(ndim>2)kbound_min(i)=kcoarse_min+kbound_min(i)
           if(ndim>2)kbound_max(i)=kcoarse_max+kbound_max(i)
        else if(kbound_min(i)*kbound_max(i)==1.and.ndim>2.and.bound_type(i)>0)then
           ibound_min(i)=icoarse_min+ibound_min(i)
           ibound_max(i)=icoarse_max+ibound_max(i)
           jbound_min(i)=jcoarse_min+jbound_min(i)
           jbound_max(i)=jcoarse_max+jbound_max(i)
           if(kbound_min(i)==-1)then
              kbound_min(i)=kcoarse_min+kbound_min(i)
              kbound_max(i)=kcoarse_min+kbound_max(i)
              if(bound_type(i)==1)boundary_type(i)=5
              if(bound_type(i)==2)boundary_type(i)=15
              if(bound_type(i)==3)boundary_type(i)=25
           else
              kbound_min(i)=kcoarse_max+kbound_min(i)
              kbound_max(i)=kcoarse_max+kbound_max(i)
              if(bound_type(i)==1)boundary_type(i)=6
              if(bound_type(i)==2)boundary_type(i)=16
              if(bound_type(i)==3)boundary_type(i)=26
           end if
        end if
     end do
     do i=1,nboundary
        ! Check for errors
        if( (ibound_min(i)<0.or.ibound_max(i)>(nx-1)) .and. (ndim>0) .and.bound_type(i)>0 )then
           if(myid==1)write(*,*)'Error in the namelist'
           if(myid==1)write(*,*)'Check boundary conditions along X direction',i
           nml_ok=.false.
        end if
        if( (jbound_min(i)<0.or.jbound_max(i)>(ny-1)) .and. (ndim>1) .and.bound_type(i)>0)then
           if(myid==1)write(*,*)'Error in the namelist'
           if(myid==1)write(*,*)'Check boundary conditions along Y direction',i
           nml_ok=.false.
        end if
        if( (kbound_min(i)<0.or.kbound_max(i)>(nz-1)) .and. (ndim>2) .and.bound_type(i)>0)then
           if(myid==1)write(*,*)'Error in the namelist'
           if(myid==1)write(*,*)'Check boundary conditions along Z direction',i
           nml_ok=.false.
        end if
     end do
  end if
  nboundary=nboundary_true
  if(simple_boundary.and.nboundary==0)then
     simple_boundary=.false.
  endif

  !--------------------------------------------------
  ! Compute boundary conservative variables
  !--------------------------------------------------
  do i=1,nboundary
     boundary_var(i,1)=MAX(d_bound(i),smallr)
     boundary_var(i,2)=d_bound(i)*u_bound(i)
#if NDIM>1
     boundary_var(i,3)=d_bound(i)*v_bound(i)
#endif
#if NDIM>2
     boundary_var(i,4)=d_bound(i)*w_bound(i)
#endif
     ek_bound=0.0d0
     do idim=1,ndim
        ek_bound=ek_bound+0.5d0*boundary_var(i,idim+1)**2/boundary_var(i,1)
     end do
     boundary_var(i,ndim+2)=ek_bound+P_bound(i)/(gamma-1.0d0)
  end do

  !-----------------------------------
  ! Rearrange level dependent arrays
  !-----------------------------------
  do i=nlevelmax,levelmin,-1
     jeans_refine(i)=jeans_refine(i-levelmin+1)
!!! FlorentR - Patch MSA
     mass_sph(i) = mass_sph(i-levelmin+1)
!!! FRenaud
  end do
  do i=1,levelmin-1
     jeans_refine(i)=-1.0
!!! FlorentR - Patch MSA
     mass_sph(i)=0.0
!!! FRenaud
  end do

  !-----------------------------------
  ! Sort out passive variable indices
  !-----------------------------------
  imetal=ndim+3
  idelay=imetal
  if(metal)idelay=imetal+1
  ixion=idelay
  if(delayed_cooling)ixion=idelay+1
  ichem=ixion
  if(aton)ichem=ixion+1


!!test OR
!#ifndef NVAR
!  write(*,*), 'not def'
!  write(*,*), nvar
!#else
!  write(*,*), 'def'
!  write(*,*), nvar
!#endif

!!!JMG
  ! check whether delayed cooling is set, and whether we have allocated enough variables in
  ! the arrays (especially uold) to hold the required data.  If not, quit the simulation.
  if(delayed_cooling .and. nvar < 6) then
     write(*,*) "ERROR: delayed_cooling is set, but NVAR<6.  NVAR must be >=6 in Makefile"
     if(ignore_delaycool_nvar_error .eqv. .false.) stop
  end if

!   !! MORE GENERAL CASE OF THE ABOVE...?
!   if( ichem > nvar) then
!      write(*,*) "ERROR: NVAR is too small in Makefile to hold metals, delayed cooling, etc...", nvar, ichem
!      stop
!   end if


  !-----------------------------------
  ! Stuff for measuring mass around sink particles.
  !-----------------------------------
  ! Count the number of radii in radii_measure
  nradii_measure = 0
  do i=0, MAX_NRADII
     if(radii_measure(i) > 0.0) nradii_measure = nradii_measure + 1
  enddo

  ! If we use a modified vsink, then radii_measure must be set.
  ! Choose a default of 100 pc.
  if((sink_track_vcom .or. sink_track_com .or. vsink_avg_cloud .or. reposition_sink_com)) then
     if(nradii_measure == 0) then
        radii_measure(1) = 0.100
        nradii_measure = 1
     endif

     ! if sink_track_com is set, then sink_track_vcom must be set
     ! as well.  (sink_track_com is an extension to sink_track_vcom).
     if(sink_track_com) sink_track_vcom = .true.
endif

  ! if reposition_sink_com is set, then automatically set reposition_sink also
  if(reposition_sink_com) reposition_sink = .true.

  ! If radii_measure is set, the measure gas.  If it's not set,
  ! make sure do_measure_part_mass is not set so that we don't
  ! have errors in the code.
  if(nradii_measure > 0) then
     do_measure_gas_mass=.true.
  else
     do_measure_part_mass=.false.
  endif

  if(msink_fix > 0.0) then
     fix_msink=.true.
  endif

!   if(msink_dynamical > 0.0) then
!   endif

!------------- Deal w/ refinement around sink
  if(do_sink_refine) then
     call set_sink_refine_radius()
  endif
!!!JMG
end subroutine read_hydro_params

!
! Define the radius around sink particles within which
! cells should be refined to the highest level.
!
subroutine set_sink_refine_radius()
  use amr_commons
  use pm_parameters, ONLY: ir_cloud
implicit none

  real(dp) :: dx_min, scale
  integer :: nx_loc

  nx_loc = (icoarse_max - icoarse_min + 1)
  scale = boxlen / dble(nx_loc)
  dx_min = scale * 0.5D0**level_agn / aexp

  ! define the sink radius
  sink_refine_radius = dble(ir_cloud) * dx_min

end subroutine set_sink_refine_radius


