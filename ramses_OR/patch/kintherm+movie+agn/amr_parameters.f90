module amr_parameters

  ! Define real types
  integer,parameter::sp=kind(1.0E0)
#ifndef NPRE
  integer,parameter::dp=kind(1.0E0) ! default
#else
#if NPRE==4
  integer,parameter::dp=kind(1.0E0) ! real*4
#else
  integer,parameter::dp=kind(1.0D0) ! real*8
#endif
#endif
#ifdef QUADHILBERT
  integer,parameter::qdp=kind(1.0_16) ! real*16
#else
  integer,parameter::qdp=kind(1.0_8) ! real*8
#endif
  integer,parameter::MAXOUT=1000
  integer,parameter::MAXLEVEL=100
  
  ! Number of dimensions
#ifndef NDIM
  integer,parameter::ndim=1
#else
  integer,parameter::ndim=NDIM
#endif
  integer,parameter::twotondim=2**ndim
  integer,parameter::threetondim=3**ndim
  integer,parameter::twondim=2*ndim

  ! Vectorization parameter
#ifndef NVECTOR
  integer,parameter::nvector=500  ! Size of vector sweeps
#else
  integer,parameter::nvector=NVECTOR
#endif

  integer, parameter :: nstride = 65536

  !! JMG
  integer, parameter :: FILENAME_MAX = 1000
  integer, parameter :: MAX_NRADII = 10
  !! JMG
  

  ! Run control
  logical::verbose =.false.   ! Write everything
  logical::hydro   =.false.   ! Hydro activated
  logical::pic     =.false.   ! Particle In Cell activated
  logical::poisson =.false.   ! Poisson solver activated
  logical::cosmo   =.false.   ! Cosmology activated
  logical::star    =.false.   ! Star formation activated
  logical::sink    =.false.   ! Sink particles activated
  logical::debug   =.false.   ! Debug mode activated
  logical::debug2  =.false.   ! Debug mode activated. Added JMG 2/2012
  logical::static  =.false.   ! Static mode activated
  logical::tracer  =.false.   ! Tracer particles activated
  logical::lightcone=.false.  ! Enable lightcone generation
  logical::clumpfind=.false.  ! Enable clump finder
  logical::aton=.false.       ! Enable ATON coarse grid radiation transfer
! JMG
  logical::init_black_hole=.false. ! Turn on initial BHs
  logical::verbose_agn = .false.   ! print out agn info at every fine timestep
! JMG

  ! Mesh parameters
  integer::geom=1             ! 1: cartesian, 2: cylindrical, 3: spherical
  integer::nx=1,ny=1,nz=1     ! Number of coarse cells in each dimension
  integer::levelmin=1         ! Full refinement up to levelmin
  integer::nlevelmax=1        ! Maximum number of level
  integer::ngridmax=0         ! Maximum number of grids
  integer,dimension(1:MAXLEVEL)::nexpand=1 ! Number of mesh expansion
  integer::nexpand_bound=1    ! Number of mesh expansion for virtual boundaries
  real(dp)::boxlen=1.0D0      ! Box length along x direction
  character(len=128)::ordering='hilbert'
  logical::cost_weighting=.true. ! Activate load balancing according to cpu time
  ! Recursive bisection tree parameters
  integer::nbilevelmax=1      ! Max steps of bisection partitioning
  integer::nbinodes=3         ! Max number of internal nodes
  integer::nbileafnodes=2     ! Max number of leaf (terminal) nodes
  real(dp)::bisec_tol=0.05d0  ! Tolerance for bisection load balancing

  ! Step parameters
  integer::nrestart=0         ! New run or backup file number
  integer::nstepmax=1000000   ! Maximum number of time steps
  integer::ncontrol=1         ! Write control variables
  integer::fbackup=1000000    ! Backup data to disk
  integer::nremap=0           ! Load balancing frequency (0: never)

  ! Output parameters
  integer::iout=1             ! Increment for output times
  integer::ifout=1            ! Increment for output files
  integer::iback=1            ! Increment for backup files
  integer::noutput=1          ! Total number of outputs
  integer::foutput=1000000    ! Frequency of outputs
  integer::output_mode=0      ! Output mode (for hires runs)

  ! Lightcone parameters
  real(dp)::thetay_cone=12.5
  real(dp)::thetaz_cone=12.5
  real(dp)::zmax_cone=2.0

  ! Cosmology and physical parameters
  real(dp)::boxlen_ini        ! Box size in h-1 Mpc
  real(dp)::omega_b=0.0D0     ! Omega Baryon
  real(dp)::omega_m=1.0D0     ! Omega Matter
  real(dp)::omega_l=0.0D0     ! Omega Lambda
  real(dp)::omega_k=0.0D0     ! Omega Curvature
  real(dp)::h0     =1.0D0     ! Hubble constant in km/s/Mpc
  real(dp)::aexp   =1.0D0     ! Current expansion factor
  real(dp)::hexp   =0.0D0     ! Current Hubble parameter
  real(dp)::n_sink =1D30      ! Sink particle density threshold in H/cc
  real(dp)::m_star =-1.0      ! Star particle mass in units of mass_sph
  real(dp)::n_star =0.1D0     ! Star formation density threshold in H/cc
  real(dp)::t_star =0.0D0     ! Star formation time scale in Gyr
  real(dp)::eps_star=0.0D0    ! Star formation efficiency (0.02 at n_star=0.1 gives t_star=8 Gyr)
  real(dp)::T2_star=0.0D0     ! Typical ISM polytropic temperature
  real(dp)::g_star =1.6D0     ! Typical ISM polytropic index
  real(dp)::jeans_ncells=-1   ! Jeans polytropic EOS
  real(dp)::del_star=2.D2     ! Minimum overdensity to define ISM
  real(dp)::eta_sn =0.0D0     ! Supernova mass fraction
  real(dp)::yield  =0.0D0     ! Supernova yield
  real(dp)::f_ek   =1.0D0     ! Supernovae kinetic energy fraction (only between 0 and 1)
  real(dp)::rbubble=0.0D0     ! Supernovae superbubble radius in pc
  real(dp)::f_w    =0.0D0     ! Supernovae mass loading factor
  integer ::ndebris=1         ! Supernovae debris particle number
  real(dp)::mass_gmc=-1.0     ! Stochastic exploding GMC mass
  real(dp)::z_ave  =0.0D0     ! Average metal abundance
  real(dp)::B_ave  =0.0D0     ! Average magnetic field
  real(dp)::z_reion=8.5D0     ! Reionization redshift
  real(dp)::T2_start          ! Starting gas temperature
  real(dp)::t_delay=1.0D1     ! Feedback time delay in Myr
  real(dp)::J21    =0.0D0     ! UV flux at threshold in 10^21 units
  real(dp)::a_spec =1.0D0     ! Slope of the UV spectrum
  real(dp)::beta_fix=0.0D0    ! Pressure fix parameter
  real(dp)::rsink_max=10      ! Sink isolation criterion in kpc
  real(dp)::msink_max=1d5     ! Maximum seed mass in solar masses
  logical ::self_shielding=.false.
  logical ::pressure_fix=.false.
  logical ::nordlund_fix=.true.
  logical ::cooling=.false.
  logical ::isothermal=.false.
  logical ::metal=.false.
  logical ::bondi=.true.      ! Activate Bondi accretion onto sink particle 
  logical ::haardt_madau=.false.
  logical ::delayed_cooling=.false.
  logical ::smbh=.false.      ! JMG - should be obsolete now with check_smbh_formation_sites
  logical ::agn=.false.
! JMG ---> different from kintherm and kintherm_movie but not directly related to AGN
  logical :: adaptive_cooling_delay = .false.
  real(dp):: tdelay_SN=10.0    ! Delay time before SN explode after star-formation (Myrs)
  real(dp):: delaycool_time=20.0   ! Timescale for cooling to be delayed, in Myrs
  logical :: ignore_delaycool_nvar_error=.false.
! --->

  real(dp):: Tmin_AGN=1d7     ! AGN stores up enough energy until it can heat gas to this temp
  real(dp):: Tmax_AGN=1d9     ! AGN should not heat gas to temperatures above this
  integer :: level_agn=0      ! AGN blast radius is ir_cloud (=4) cells in this level. Def: nlevelmax
  real(dp):: beta_accretion=2 ! power-law index for the "alpha" factor that increases bondi acc. rate
  logical :: reset_accreted_mass=.false.   ! reset accreted mass if it stays too high for too long
  integer :: reset_accreted_mass_on_startup = 0   ! reset the accreted mass when re-starting a simulation for this many calls of create_sink().  A normal value would be 1 (or zero!).
  logical :: inject_agn_coldgas=.false.    ! only inject AGN energy to gas with T < Tmin_AGN
                                           ! ***NB: Now only inject if T(after heating) < Tmax_AGN
  logical :: do_accretion=.true.   ! After calculating accretion rates, actually move mass from the gas to the sink
  logical :: check_smbh_formation_sites=.false.  ! check local stellar mass and velocity dispersion

  logical :: lookup_acc_rate=.false.  ! look up dM/dt instead of calculating it for real
  character(LEN=FILENAME_MAX) :: acc_lookup_file = ' '
  
  logical :: expand_agn_blast=.false. ! enlarge AGN blast radius to get rid of excess "stored" AGN energy
  integer :: increase_rblast = 0      ! force larger AGN blast radius.  Default radius is 4 (cells)
  logical :: mass_weight_acc=.false.  ! use a mass weighting when removing accreted gas from cells

  logical :: fix_msink=.false.        ! fix msink to value "msink_fix" every time accrete_bondi is called (each timestep)
  real(dp) :: msink_fix=0.0           ! fixed value of msink
  real(dp) :: msink_dynamical = 0.0     ! Sink mass used for dynamics calculations.  If ==0, then just use the normal sink mass.  Otherwise, the true sink mass is decoupled from the effective dynamical sink mass.
  real(dp) :: msink_pseudo_dyn = 0.0   ! Sink massed used for pseudo-dynamics.  In this case the sink velocities are effectively divided by this mass instead of the real sink mass when computing dynamics, but the effects on surrounding gas and particles is as though the real sink mass is in effect.
  logical :: bh_dynamical_friction=.false.
  real(dp) :: fdyn_factor=1.0          ! extra factor to increase/suppress dynamical friction on the BH

  logical :: static_sink=.false.      ! sink particle always has zero velocity
  logical :: reposition_sink=.false.  ! At each coarse step, move sink particle back to center of sim. box
  logical :: reposition_sink_com = .false.  ! move sink to local center-of-mass of old stars measured on-the-fly
  logical :: vsink_avg_cloud=.false.  ! Sink velocity is set to the average of its "cloud" particles, instead of the velocity of the nearest cloud particle

  logical :: do_measure_gas_mass=.false.      ! measure gas mass in a sphere around each sink particle
  real(dp), dimension(1:MAX_NRADII) :: radii_measure=0  ! radii at which to measure the gas mass (in sim units:kpc).  If this is set to something then do_measure_gas_mass and nradii_measure are automatically set
  integer :: nradii_measure=0           ! number of radii at which to measure gas/particle mass. Automatically set w/ radii_measure.

  logical :: do_measure_part_mass=.false.     ! measure particle mass as well as gas mass within radii
  logical :: measure_part_veldisp=.true.      ! measure particle velocity dispersion (only works if above is set)
  logical :: sink_track_vcom=.false.          ! sink particle velocity follows the local center-of-mass velocity
  logical :: sink_track_com=.false.           ! sink particle velocity is always towards local COM, proportional to distance from that COM

  logical :: drag_simple=.false.      ! Introduce a sub-resolution drag force on (accreting) sink particles
!!!  logical :: sink_track_radius=
  logical :: do_sink_nbody = .false.  ! Use a crude N-body solver for sink particle motion
  !--------------- !

  real(dp) :: max_sf_eff=-1.0         ! Maximum star-formation efficiency (try 0.3) -- suppresses SF in very dense regions

  logical :: movie=.false. ! JMG patch for movies
! JMG

!!! FlorentR - PATCH FBSS
  real(dp)::THII=0.0           ! temperature of the HII regions (try 2e4)
  real(dp)::rhominHII=10.0     ! minimum density to grow a HII bubble in (in H/cc)
  real(dp)::rhodelayHII=100.0  ! density at which the free-fall time is the delay before the HII feedback is turned on (in H/cc)
  real(dp)::multiscat=2.0      ! number of multiple scattering for the radiative pressure
!!! RenaudF
!!! JMG
  integer :: nHII_skip=1       ! Make only 1 out of nHII_skip young stars be feedback sources.  Multiply the energy per source by this amount too, so total feedabck energy is the same
  logical :: boost_HII_feedback=.false.  ! Make sure that thermal HII feedback puts gas *above* the Jeans polytrope, even if the Tjeans is above THII

!!! end JMG

!!! FlorentR - PATCH NLEV_SF
  real(dp)::nlev_sf = 12
!!! FRenaud
!!! JMG
  integer::jeans_polytrope=1   ! flag to choose how high the jeans polytrope is.  1=default(highest Tjeans), 2=middle (spherical collapse), 3=lowest
  real(dp):: Tmax_global=-1.0  ! Maximum temperature.  Don't allow gas to go above this, unless it's at the temperature floor (set by Jeans polytrope)
  real(dp):: check_velocity = -1.0 ! If >0, check whether velocites are getting too big, and fix the big velocities to this value.  Also check density and temperature

!!! end JMG


  ! Output times
  real(dp),dimension(1:MAXOUT)::aout=1.1       ! Output expansion factors
  real(dp),dimension(1:MAXOUT)::tout=0.0       ! Output times

  ! Refinement parameters for each level
  real(dp),dimension(1:MAXLEVEL)::m_refine =-1.0 ! Lagrangian threshold
  real(dp),dimension(1:MAXLEVEL)::r_refine =-1.0 ! Radius of refinement region
  real(dp),dimension(1:MAXLEVEL)::x_refine = 0.0 ! Center of refinement region
  real(dp),dimension(1:MAXLEVEL)::y_refine = 0.0 ! Center of refinement region
  real(dp),dimension(1:MAXLEVEL)::z_refine = 0.0 ! Center of refinement region
  real(dp),dimension(1:MAXLEVEL)::exp_refine = 2.0 ! Exponent for distance
  real(dp),dimension(1:MAXLEVEL)::a_refine = 1.0 ! Ellipticity (Y/X)
  real(dp),dimension(1:MAXLEVEL)::b_refine = 1.0 ! Ellipticity (Z/X)
  real(dp)::var_cut_refine=-1.0 ! Threshold for variable-based refinement
  real(dp)::mass_cut_refine=-1.0 ! Mass threshold for particle-based refinement
  integer::ivar_refine=-1 ! Variable index for refinement
!!! JMG stuff for zoom-ins
  integer :: levelmax_lowres = -1   ! Effective maximum refinement level in the "no-zoom" region
  logical :: jeans_nozoom = .false.   ! Use a jeans polytrope w/ a lower level_jeans in the "no-zoom" region
  real(dp), dimension(1:MAXLEVEL)::r_buffer = -1.0 ! radius of buffer region for smooth transition from zoom to no-zoom
  logical :: rand_buffer=.false.     ! within the buffer region, use random num to decide whether to refine each cell
  integer :: zoom_sink = -1           ! Zoom in on the position of a sink particle.  Specify sink ID with this variable.

  logical :: do_sink_refine = .false.  ! Ensure that the region around sink particles is refined to level_agn
  real(dp) :: sink_refine_radius = 0.0  ! Size of the region around each sink that should be refined to the highest level
!!! end JMG

  ! Initial condition files for each level
  logical::multiple=.false.
  character(LEN=FILENAME_MAX),dimension(1:MAXLEVEL)::initfile=' '
  character(LEN=20)::filetype='ascii'

  ! Initial condition regions parameters
  integer,parameter::MAXREGION=100
  integer                           ::nregion=0
  character(LEN=10),dimension(1:MAXREGION)::region_type='square'
  real(dp),dimension(1:MAXREGION)   ::x_center=0.
  real(dp),dimension(1:MAXREGION)   ::y_center=0.
  real(dp),dimension(1:MAXREGION)   ::z_center=0.
  real(dp),dimension(1:MAXREGION)   ::length_x=1.E10
  real(dp),dimension(1:MAXREGION)   ::length_y=1.E10
  real(dp),dimension(1:MAXREGION)   ::length_z=1.E10
  real(dp),dimension(1:MAXREGION)   ::exp_region=2.0

  ! Boundary conditions parameters
  integer,parameter::MAXBOUND=100
  logical                           ::simple_boundary=.false.
  integer                           ::nboundary=0
  integer                           ::icoarse_min=0
  integer                           ::icoarse_max=0
  integer                           ::jcoarse_min=0
  integer                           ::jcoarse_max=0
  integer                           ::kcoarse_min=0
  integer                           ::kcoarse_max=0
  integer ,dimension(1:MAXBOUND)    ::boundary_type=0
  integer ,dimension(1:MAXBOUND)    ::ibound_min=0
  integer ,dimension(1:MAXBOUND)    ::ibound_max=0
  integer ,dimension(1:MAXBOUND)    ::jbound_min=0
  integer ,dimension(1:MAXBOUND)    ::jbound_max=0
  integer ,dimension(1:MAXBOUND)    ::kbound_min=0
  integer ,dimension(1:MAXBOUND)    ::kbound_max=0

end module amr_parameters
