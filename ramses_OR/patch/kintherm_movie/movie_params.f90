!
! Things to add
!XX - put the "typ" options back in  -- units on velocities??? OK, but need to fix temps,etc.
!XX - sink particle positions in header
!XX - option for filename additives
! - particle maps
!   -
module movie_params
  use amr_parameters, ONLY: dp, FILENAME_MAX

  integer, parameter :: MAX_NMOVIES = 10
  integer :: nmovies=0
  real(dp), dimension(MAX_NMOVIES) :: dt_movie=0.0, dt_movie_ramsesunits=0.0, t_last_movie=0.0  ! dt_movie in Myrs, others in ramses units

  integer, dimension(MAX_NMOVIES) :: lmax_movie   ! maximum level for the movie images
  real(dp), dimension(MAX_NMOVIES) :: xmin_m, xmax_m, ymin_m, ymax_m, zmin_m, zmax_m   ! limits of the image in coords
  character(LEN=1), dimension(MAX_NMOVIES) :: dir_m    ! direction defining viewing angle
  integer, dimension(MAX_NMOVIES) :: typ_m               ! what sort of map to make (column density, velocity field...?)
  character(LEN=10), dimension(MAX_NMOVIES) :: gas_or_parts   ! make an image of gas or particles?
  character(LEN=FILENAME_MAX), dimension(MAX_NMOVIES) :: suffix_m  ! suffix to output filename

  integer, dimension(MAX_NMOVIES) :: movie_sink=0   ! Center movie on sink particle with this ID number

  integer, dimension(MAX_NMOVIES) :: age_flag=0  ! include only certain particles (0) all particles, (1) old, IC particles only, (2) particles younger than max_particle_age, always excluding old particles
  real(dp), dimension(MAX_NMOVIES) :: max_particle_age=0.0   ! in Myrs
  
end module movie_params

!*****************************************************
!*****************************************************
!*****************************************************
! Read the movie parameters from the namelist file.
!
subroutine read_movie_params
  use amr_commons, only:FILENAME_MAX, myid
  use movie_params
  implicit none
  
  integer :: i, info
  character(LEN=FILENAME_MAX) :: filedir, filecmd

  real(dp) :: scale_l, scale_t, scale_d, scale_v, scale_nH, scale_T2

  namelist/movie_parameters/ xmin_m, xmax_m, ymin_m, ymax_m, zmin_m, zmax_m &
       & , dir_m,  typ_m, gas_or_parts, lmax_movie, suffix_m &
       & , dt_movie, max_particle_age, age_flag, movie_sink

  ! get units for conversion of dt_movie to ramses units 
  call units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)

  ! zero out everything
  xmin_m = 0; xmax_m=0;
  ymin_m = 0; ymax_m=0;
  zmin_m = 0; zmax_m=0;
  dir_m = ''
  typ_m = 1
  gas_or_parts = ''
  suffix_m = ''
 
  dt_movie = 0.0
  dt_movie_ramsesunits = 0.0
  age_flag = 0
  max_particle_age = 0.0
  movie_sink = 0

  ! read the parameter file
  rewind(1)
  read(1, NML=movie_parameters)
  
  ! Count the number of movies to be made
  ! based on the number of elements provided for gas or parts
  nmovies = 0
  do i=1, MAX_NMOVIES
     if(TRIM(gas_or_parts(i)) == 'gas' .or. TRIM(gas_or_parts(i)) == 'parts') then
        nmovies = nmovies+1

        ! If values are not provided for all movies in the param file, then 
        ! inherit values of dt_movie from the first entry
        if(i > 1) then
           ! is dt_movie zero?  if so, inherit previous value in the array
           if(dt_movie(i) < 1e-10) dt_movie = dt_movie(i-1)
        endif
        ! convert dt_movie to ramses units
        ! 3.15d13 is number of seconds per Myr
        dt_movie_ramsesunits(i) = dt_movie(i) / scale_t * 3.15576D13 
     endif
  end do

  ! create a "MOVIE" directory if it doesn't already exist
  filedir='MOVIE'
  filecmd='mkdir -p '//TRIM(filedir)
#ifdef NOSYSTEM
  if(myid==1)call PXFMKDIR(TRIM(filedir),LEN(TRIM(filedir)),O'755',info)
#else
  if(myid==1)call system(filecmd)
#endif

end subroutine read_movie_params
