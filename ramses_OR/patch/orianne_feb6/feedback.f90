!################################################################
!################################################################
!################################################################
!################################################################
subroutine thermal_feedback(ilevel,icount)
  use pm_commons
  use amr_commons
  implicit none
  integer::ilevel,icount
  !------------------------------------------------------------------------
  ! This routine computes the thermal energy, the kinetic energy and 
  ! the metal mass dumped in the gas by stars (SNII, SNIa, winds).
  ! This routine is called every fine time step.
  !------------------------------------------------------------------------
  real(dp)::scale_nH,scale_T2,scale_l,scale_d,scale_t,scale_v
  real(dp)::t0,scale,dx_min,vsn,rdebris,ethermal
  integer::igrid,jgrid,ipart,jpart,next_part
  integer::i,ig,ip,npart1,npart2,icpu,nx_loc
  real(dp),dimension(1:3)::skip_loc
  integer,dimension(1:nvector),save::ind_grid,ind_part,ind_grid_part

  if(numbtot(1,ilevel)==0)return
  if(verbose)write(*,111)ilevel
  if(icount==2)return

  ! Gather star particles only.

#if NDIM==3
  ! Loop over cpus
  do icpu=1,ncpu
     igrid=headl(icpu,ilevel)
     ig=0
     ip=0
     ! Loop over grids
     do jgrid=1,numbl(icpu,ilevel)
        npart1=numbp(igrid)  ! Number of particles in the grid
        npart2=0
        
        ! Count star particles
        if(npart1>0)then
           ipart=headp(igrid)
           ! Loop over particles
           do jpart=1,npart1
              ! Save next particle   <--- Very important !!!
              next_part=nextp(ipart)
              if(idp(ipart).gt.0.and.tp(ipart).ne.0)then
                 npart2=npart2+1
              endif
              ipart=next_part  ! Go to next particle
           end do
        endif
        
        ! Gather star particles
        if(npart2>0)then        
           ig=ig+1
           ind_grid(ig)=igrid
           ipart=headp(igrid)
           ! Loop over particles
           do jpart=1,npart1
              ! Save next particle   <--- Very important !!!
              next_part=nextp(ipart)
              ! Select only star particles
              if(idp(ipart).gt.0.and.tp(ipart).ne.0)then
                 if(ig==0)then
                    ig=1
                    ind_grid(ig)=igrid
                 end if
                 ip=ip+1
                 ind_part(ip)=ipart
                 ind_grid_part(ip)=ig   
              endif
              if(ip==nvector)then
                 call feedbk(ind_grid,ind_part,ind_grid_part,ig,ip,ilevel)
                 ip=0
                 ig=0
              end if
              ipart=next_part  ! Go to next particle
           end do
           ! End loop over particles
        end if
        igrid=next(igrid)   ! Go to next grid
     end do
     ! End loop over grids
     if(ip>0)call feedbk(ind_grid,ind_part,ind_grid_part,ig,ip,ilevel)
  end do 
  ! End loop over cpus

#endif

111 format('   Entering thermal_feedback for level ',I2)

end subroutine thermal_feedback
!################################################################
!################################################################
!################################################################
!################################################################
subroutine feedbk(ind_grid,ind_part,ind_grid_part,ng,np,ilevel)
  use amr_commons
  use pm_commons
  use hydro_commons
  use random
  implicit none
  integer::ng,np,ilevel
  integer,dimension(1:nvector)::ind_grid
  integer,dimension(1:nvector)::ind_grid_part,ind_part
  !-----------------------------------------------------------------------
  ! This routine is called by subroutine feedback. Each debris particle
  ! dumps mass, momentum and energy in the nearest grid cell using array
  ! uold.
  !-----------------------------------------------------------------------
  integer::i,j,idim,nx_loc
  real(kind=8)::RandNum
  real(dp)::SN_BOOST,mstar,dx_min,vol_min
  real(dp)::xxx,mmm,t0,ESN,mejecta,zloss
  real(dp)::dx,dx_loc,scale,vol_loc,birth_time
  real(dp)::scale_nH,scale_T2,scale_l,scale_d,scale_t,scale_v
  logical::error
  ! Grid based arrays
  real(dp),dimension(1:nvector,1:ndim),save::x0
  integer ,dimension(1:nvector),save::ind_cell
  integer ,dimension(1:nvector,1:threetondim),save::nbors_father_cells
  integer ,dimension(1:nvector,1:twotondim),save::nbors_father_grids
  ! Particle based arrays
  integer,dimension(1:nvector),save::igrid_son,ind_son
  integer,dimension(1:nvector),save::list1
  logical,dimension(1:nvector),save::ok
  real(dp),dimension(1:nvector),save::mloss,mzloss,ethermal,ekinetic,dteff
  real(dp),dimension(1:nvector,1:ndim),save::x
  integer ,dimension(1:nvector,1:ndim),save::id,igd,icd
  integer ,dimension(1:nvector),save::igrid,icell,indp,kg
  real(dp),dimension(1:3)::skip_loc
!!!JMG
  real(dp) :: nHtp
!!!JMG

  ! Conversion factor from user units to cgs units
  call units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)

  ! Mesh spacing in that level
  dx=0.5D0**ilevel
  nx_loc=(icoarse_max-icoarse_min+1)
  skip_loc=(/0.0d0,0.0d0,0.0d0/)
  if(ndim>0)skip_loc(1)=dble(icoarse_min)
  if(ndim>1)skip_loc(2)=dble(jcoarse_min)
  if(ndim>2)skip_loc(3)=dble(kcoarse_min)
  scale=boxlen/dble(nx_loc)
  dx_loc=dx*scale
  vol_loc=dx_loc**ndim
  dx_min=(0.5D0**nlevelmax)*scale
  vol_min=dx_min**ndim

  ! Minimum star particle mass
!!!FredB to make compatible with mass_sph_array
  mstar=n_star/(scale_nH*aexp**3)*vol_min
!  if(m_star < 0d0)then
!     mstar=n_star/(scale_nH*aexp**3)*vol_min
!  else
!     mstar=m_star*mass_sph
!  endif

  ! Compute stochastic boost to account for target GMC mass
  SN_BOOST=MAX(mass_gmc*2d33/(scale_d*scale_l**3)/mstar,1d0)

  ! Massive star lifetime from Myr to code units
  t0=10.*1d6*(365.*24.*3600.)/scale_t

  ! Type II supernova specific energy from cgs to code units
  ESN=1d51/(10.*2d33*(10.**.4))/scale_v**2
 ! print*, 'ESN (thermal) : ', ESN


#if NDIM==3
  ! Lower left corner of 3x3x3 grid-cube
  do idim=1,ndim
     do i=1,ng
        x0(i,idim)=xg(ind_grid(i),idim)-3.0D0*dx
     end do
  end do

  ! Gather 27 neighboring father cells (should be present anytime !)
  do i=1,ng
     ind_cell(i)=father(ind_grid(i))
  end do
  call get3cubefather(ind_cell,nbors_father_cells,nbors_father_grids,ng,ilevel)

  ! Rescale position at level ilevel
  do idim=1,ndim
     do j=1,np
        x(j,idim)=xp(ind_part(j),idim)/scale+skip_loc(idim)
     end do
  end do
  do idim=1,ndim
     do j=1,np
        x(j,idim)=x(j,idim)-x0(ind_grid_part(j),idim)
     end do
  end do
  do idim=1,ndim
     do j=1,np
        x(j,idim)=x(j,idim)/dx
     end do
  end do

  ! Check for illegal moves
  error=.false.
  do idim=1,ndim
     do j=1,np
        if(x(j,idim)<=2.0D0.or.x(j,idim)>=4.0D0)error=.true.
     end do
  end do
  if(error)then
     write(*,*)'problem in sn2'
     write(*,*)ilevel,ng,np
  end if

  ! NGP at level ilevel
  do idim=1,ndim
     do j=1,np
        id(j,idim)=x(j,idim)
     end do
  end do

   ! Compute parent grids
  do idim=1,ndim
     do j=1,np
        igd(j,idim)=id(j,idim)/2
     end do
  end do
  do j=1,np
     kg(j)=1+igd(j,1)+3*igd(j,2)+9*igd(j,3)
  end do
  do j=1,np
     igrid(j)=son(nbors_father_cells(ind_grid_part(j),kg(j)))
  end do

  ! Check if particles are entirely in level ilevel
  ok(1:np)=.true.
  do j=1,np
     ok(j)=ok(j).and.igrid(j)>0
  end do

  ! Compute parent cell position
  do idim=1,ndim
     do j=1,np
        if(ok(j))then
           icd(j,idim)=id(j,idim)-2*igd(j,idim)
        end if
     end do
  end do
  do j=1,np
     if(ok(j))then
        icell(j)=1+icd(j,1)+2*icd(j,2)+4*icd(j,3)
     end if
  end do

  ! Compute parent cell adresses
  do j=1,np
     if(ok(j))then
        indp(j)=ncoarse+(icell(j)-1)*ngridmax+igrid(j)
     end if
  end do

  ! Compute individual time steps
  ! WARNING: the time step is always the coarser level time step
  ! since we do not have feedback for icount=2
  if(ilevel==levelmin)then
     do j=1,np
        if(ok(j))then
           dteff(j)=dtold(levelp(ind_part(j)))
        end if
     end do
  else
     do j=1,np
        if(ok(j))then
           dteff(j)=dtold(levelp(ind_part(j))-1)
        end if
     end do
  endif

  ! Reset ejected mass, metallicity, thermal energy
  do j=1,np
     if(ok(j))then
        mloss(j)=0d0
        mzloss(j)=0d0
        ethermal(j)=0d0
     endif
  end do

  ! Compute stellar mass loss and thermal feedback due to supernovae
  if(f_w==0)then
     do j=1,np
        if(ok(j))then
           birth_time=tp(ind_part(j))
           ! Make sure that we don't count feedback twice
           if(birth_time.lt.(t-t0).and.birth_time.ge.(t-t0-dteff(j)))then
              ! Stellar mass loss
              mejecta=eta_sn*mp(ind_part(j))
              mloss(j)=mloss(j)+mejecta/vol_loc
              print*, 'mejecta (thermal) : ', mejecta
              ! Thermal energy
              ethermal(j)=ethermal(j)+mejecta*ESN/vol_loc
              print*, 'ethermal (thermal) : ', ethermal(j)
              ! Metallicity
              if(metal)then
                 zloss=yield+(1d0-yield)*zp(ind_part(j))
                 mzloss(j)=mzloss(j)+mejecta*zloss/vol_loc
              endif
              ! Reduce star particle mass
              mp(ind_part(j))=mp(ind_part(j))-mejecta
              ! Boost SNII energy and depopulate accordingly
              if(SN_BOOST>1d0)then
                 call ranf(localseed,RandNum)
                 if(RandNum<1d0/SN_BOOST)then
                    mloss(j)=SN_BOOST*mloss(j)
                    mzloss(j)=SN_BOOST*mzloss(j)
                    ethermal(j)=SN_BOOST*ethermal(j)
                 else
                    mloss(j)=0d0
                    mzloss(j)=0d0
                    ethermal(j)=0d0
                 endif
              endif
           endif
        end if
     end do
  endif

  ! Update hydro variables due to feedback
  do j=1,np
     if(ok(j))then
        ! Specific kinetic energy of the star
        ekinetic(j)=0.5*(vp(ind_part(j),1)**2 &
             &          +vp(ind_part(j),2)**2 &
             &          +vp(ind_part(j),3)**2)

!!! JMG, based on Frederic's suggestion (email Oct 29 2012)
        if(Tmax_global > 0.0) then
           nHtp=uold(indp(j),1)*scale_nH  ! density of this cell
           if ( ethermal(j) > Tmax_global *nHtp/scale_T2/(gamma-1.0) ) then
              write(*,*) 'reduced SN thermal dump from...to...', &
                   ethermal(j)/ (nHtp/scale_T2/(gamma-1.0)), Tmax_global
              ethermal(j) = Tmax_global *nHtp/scale_T2/(gamma-1.0)
           endif
        endif
!!! end JMG

        ! Update hydro variable in NGP cell
        print*, 'Thermal feedback... hydro variables are updated'
        uold(indp(j),1)=uold(indp(j),1)+mloss(j)
        uold(indp(j),2)=uold(indp(j),2)+mloss(j)*vp(ind_part(j),1)
        uold(indp(j),3)=uold(indp(j),3)+mloss(j)*vp(ind_part(j),2)
        uold(indp(j),4)=uold(indp(j),4)+mloss(j)*vp(ind_part(j),3)
        uold(indp(j),5)=uold(indp(j),5)+mloss(j)*ekinetic(j)+ethermal(j)
        print*, 'total energy of gas (thermal fb) : ', uold(indp(j),5)
        print*, 'mass density in the cell (thermal fb) : ',  uold(indp(j),1)
     endif
  end do

  ! Add metals
  if(metal)then
     do j=1,np
        if(ok(j))then
           uold(indp(j),imetal)=uold(indp(j),imetal)+mzloss(j)
        endif
     end do
  endif

  ! Add delayed cooling switch variable
  if(delayed_cooling)then
     do j=1,np
        if(ok(j))then
           uold(indp(j),idelay)=uold(indp(j),idelay)+mloss(j)
        endif
     end do
  endif

#endif
  
end subroutine feedbk
!################################################################
!################################################################
!################################################################
!################################################################
subroutine kinetic_feedback
  use amr_commons
  use pm_commons
  use hydro_commons
  use cooling_module, ONLY: XH=>X, rhoc, mH 
  implicit none
#ifndef WITHOUTMPI
  include 'mpif.h'
  integer::nSN_tot_all
  integer,dimension(1:ncpu)::nSN_icpu_all
  real(dp),dimension(:),allocatable::mSN_all,sSN_all,ZSN_all
  real(dp),dimension(:,:),allocatable::xSN_all,vSN_all
#endif
  !----------------------------------------------------------------------
  ! This subroutine compute the kinetic feedback due to SNII and
  ! imolement this using exploding GMC particles. 
  ! This routine is called only at coarse time step.
  ! Yohan Dubois
  !----------------------------------------------------------------------
  ! local constants
  integer::ip,icpu,igrid,jgrid,npart1,npart2,ipart,jpart,next_part
  integer::nSN,nSN_loc,nSN_tot,info,iSN,ilevel,ivar
  integer,dimension(1:ncpu)::nSN_icpu
  real(dp)::scale_nH,scale_T2,scale_l,scale_d,scale_t,scale_v,t0
  real(dp)::scale,dx_min,vol_min,nISM,nCOM,d0,mstar
  integer::nx_loc
  integer,dimension(:),allocatable::ind_part,ind_grid
  logical,dimension(:),allocatable::ok_free
  integer ,dimension(:),allocatable::indSN
  real(dp),dimension(:),allocatable::mSN,sSN,ZSN,m_gas,vol_gas,ekBlast
  real(dp),dimension(:,:),allocatable::xSN,vSN,u_gas,dq

  if(.not. hydro)return
  if(ndim.ne.3)return

  if(verbose)write(*,*)'Entering make_sn'

  ! Conversion factor from user units to cgs units
  call units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)

  ! Mesh spacing in that level
  nx_loc=(icoarse_max-icoarse_min+1)
  scale=boxlen/dble(nx_loc)
  dx_min=(0.5D0**nlevelmax)*scale
  vol_min=dx_min**ndim

  ! Initial star particle mass
  mstar=n_star/(scale_nH*aexp**3)*vol_min

  ! Lifetime of Giant Molecular Clouds from Myr to code units
  t0=10.*(1d6*365.*24.*3600.)/scale_t

  !------------------------------------------------------
  ! Gather GMC particles eligible for disruption
  !------------------------------------------------------
  nSN_loc=0
  ! Loop over levels
  do icpu=1,ncpu
  ! Loop over cpus
     igrid=headl(icpu,levelmin)
     ! Loop over grids
     do jgrid=1,numbl(icpu,levelmin)
        npart1=numbp(igrid)  ! Number of particles in the grid
        npart2=0
        ! Count old enough GMC particles
        if(npart1>0)then
           ipart=headp(igrid)
           ! Loop over particles
           do jpart=1,npart1
              ! Save next particle   <--- Very important !!!
              next_part=nextp(ipart)
!!! JMG -- debris particle IDs have to be greater than min_debris_id
              if(idp(ipart).le. -1*min_debris_id .and. tp(ipart).lt.(t-t0))then
                 npart2=npart2+1
              endif
              ipart=next_part  ! Go to next particle
            end do
        endif
        nSN_loc=nSN_loc+npart2   ! Add SNe to the total
        igrid=next(igrid)   ! Go to next grid
     end do
  end do
  ! End loop over levels
  nSN_icpu=0
  nSN_icpu(myid)=nSN_loc
#ifndef WITHOUTMPI
  ! Give an array of number of SN on each cpu available to all cpus
  call MPI_ALLREDUCE(nSN_icpu,nSN_icpu_all,ncpu,MPI_INTEGER,MPI_SUM,MPI_COMM_WORLD,info)
  nSN_icpu=nSN_icpu_all
#endif

  nSN_tot=sum(nSN_icpu(1:ncpu))

  if (nSN_tot .eq. 0) return
  
  if(myid==1)then
     write(*,*)'-----------------------------------------------'
     write(*,*)'Number of GMC to explode=',nSN_tot
     write(*,*)'-----------------------------------------------'
  endif

  ! Allocate arrays for the position and the mass of the SN
  allocate(xSN(1:nSN_tot,1:3),vSN(1:nSN_tot,1:3))
  allocate(mSN(1:nSN_tot),sSN(1:nSN_tot),ZSN(1:nSN_tot))
  xSN=0.;vSN=0.;mSN=0.;sSN=0.;ZSN=0.
  ! Allocate arrays for particles index and parent grid
  if(nSN_loc>0)then
     allocate(ind_part(1:nSN_loc),ind_grid(1:nSN_loc),ok_free(1:nSN_loc))
  endif

  !------------------------------------------------------
  ! Store position and mass of the GMC into the SN array
  !------------------------------------------------------
  if(myid==1)then
     iSN=0
  else
     iSN=sum(nSN_icpu(1:myid-1))
  endif
  ! Loop over levels
  ip=0
  do icpu=1,ncpu
     igrid=headl(icpu,levelmin)
     ! Loop over grids
     do jgrid=1,numbl(icpu,levelmin)
        npart1=numbp(igrid)  ! Number of particles in the grid
        ! Count old enough star particles that have not exploded
        if(npart1>0)then
           ipart=headp(igrid)
           ! Loop over particles
           do jpart=1,npart1
              ! Save next particle   <--- Very important !!!
              next_part=nextp(ipart)
!!! JMG -- debris particle IDs have to be greater than min_debris_id
              if(idp(ipart).le. -1*min_debris_id .and. tp(ipart).lt.(t-t0))then
                 iSN=iSN+1
                 xSN(iSN,1)=xp(ipart,1)
                 xSN(iSN,2)=xp(ipart,2)
                 xSN(iSN,3)=xp(ipart,3)
                 vSN(iSN,1)=vp(ipart,1)
                 vSN(iSN,2)=vp(ipart,2)
                 vSN(iSN,3)=vp(ipart,3)
                 mSN(iSN)=mp(ipart)
                 sSN(iSN)=dble(-idp(ipart) - min_debris_id) * mstar

                 if(metal)ZSN(iSN)=zp(ipart)
                 ip=ip+1
                 ind_grid(ip)=igrid
                 ind_part(ip)=ipart
              endif
              ipart=next_part  ! Go to next particle
           end do
        endif
        igrid=next(igrid)   ! Go to next grid
     end do
  end do 
  ! End loop over levels

  ! Remove GMC particle
  IF(nSN_loc>0)then
     ok_free=.true.
     call remove_list(ind_part,ind_grid,ok_free,nSN_loc)
     call add_free_cond(ind_part,ok_free,nSN_loc)
     deallocate(ind_part,ind_grid,ok_free)
  endif


#ifndef WITHOUTMPI
  allocate(xSN_all(1:nSN_tot,1:3),vSN_all(1:nSN_tot,1:3),mSN_all(1:nSN_tot),sSN_all(1:nSN_tot),ZSN_all(1:nSN_tot))
  call MPI_ALLREDUCE(xSN,xSN_all,nSN_tot*3,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
  call MPI_ALLREDUCE(vSN,vSN_all,nSN_tot*3,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
  call MPI_ALLREDUCE(mSN,mSN_all,nSN_tot  ,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
  call MPI_ALLREDUCE(sSN,sSN_all,nSN_tot  ,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
  call MPI_ALLREDUCE(ZSN,ZSN_all,nSN_tot  ,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
  xSN=xSN_all
  vSN=vSN_all
  mSN=mSN_all
  sSN=sSN_all
  ZSN=ZSN_all
  deallocate(xSN_all,vSN_all,mSN_all,sSN_all,ZSN_all)
#endif

  nSN=nSN_tot
  allocate(m_gas(1:nSN),u_gas(1:nSN,1:3),vol_gas(1:nSN),dq(1:nSN,1:3),ekBlast(1:nSN))
  allocate(indSN(1:nSN))

  ! Compute the grid discretization effects
  call average_SN(xSN,vol_gas,dq,ekBlast,indSN,nSN)

  ! Modify hydro quantities to account for a Sedov blast wave
  call Sedov_blast(xSN,vSN,mSN,sSN,ZSN,indSN,vol_gas,dq,ekBlast,nSN)
 
  deallocate(xSN,vSN,mSN,sSN,ZSN,indSN,m_gas,u_gas,vol_gas,dq,ekBlast)

  ! Update hydro quantities for split cells
  do ilevel=nlevelmax,levelmin,-1
     call upload_fine(ilevel)
     do ivar=1,nvar
        call make_virtual_fine_dp(uold(1,ivar),ilevel)
     enddo
  enddo

end subroutine kinetic_feedback
!################################################################
!################################################################
!################################################################
!################################################################
subroutine average_SN(xSN,vol_gas,dq,ekBlast,ind_blast,nSN)
  use pm_commons
  use amr_commons
  use hydro_commons
  implicit none
#ifndef WITHOUTMPI
  include 'mpif.h'
#endif
  !------------------------------------------------------------------------
  ! This routine average the hydro quantities inside the SN bubble
  !------------------------------------------------------------------------
  integer::ilevel,ncache,nSN,j,iSN,ind,ix,iy,iz,ngrid,iskip
  integer::i,nx_loc,igrid,info
  integer,dimension(1:nvector),save::ind_grid,ind_cell
  real(dp)::x,y,z,dr_SN,d,u,v,w,ek,u2,v2,w2,dr_cell
  real(dp)::scale,dx,dxx,dyy,dzz,dx_min,dx_loc,vol_loc,rmax2,rmax
  real(dp)::scale_nH,scale_T2,scale_l,scale_d,scale_t,scale_v
  real(dp),dimension(1:3)::skip_loc
  real(dp),dimension(1:twotondim,1:3)::xc
  integer ,dimension(1:nSN)::ind_blast
  real(dp),dimension(1:nSN)::mSN,m_gas,vol_gas,ekBlast
  real(dp),dimension(1:nSN,1:3)::xSN,vSN,u_gas,dq,u2Blast
#ifndef WITHOUTMPI
  real(dp),dimension(1:nSN)::m_gas_all,vol_gas_all,ekBlast_all
  real(dp),dimension(1:nSN,1:3)::u_gas_all,dq_all,u2Blast_all
#endif
  logical ,dimension(1:nvector),save::ok

  if(nSN==0)return
  if(verbose)write(*,*)'Entering average_SN'

  ! Mesh spacing in that level
  nx_loc=(icoarse_max-icoarse_min+1)
  skip_loc=(/0.0d0,0.0d0,0.0d0/)
  skip_loc(1)=dble(icoarse_min)
  skip_loc(2)=dble(jcoarse_min)
  skip_loc(3)=dble(kcoarse_min)
  scale=boxlen/dble(nx_loc)
  dx_min=scale*0.5D0**nlevelmax

  ! Conversion factor from user units to cgs units
  call units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)

  ! Maximum radius of the ejecta
  rmax=MAX(2.0d0*dx_min*scale_l/aexp,rbubble*3.08d18)
  rmax=rmax/scale_l
  rmax2=rmax*rmax

  ! Initialize the averaged variables
  vol_gas=0.0;dq=0.0;u2Blast=0.0;ekBlast=0.0;ind_blast=-1

  ! Loop over levels
  do ilevel=levelmin,nlevelmax
     ! Computing local volume (important for averaging hydro quantities) 
     dx=0.5D0**ilevel 
     dx_loc=dx*scale
     vol_loc=dx_loc**ndim
     ! Cells center position relative to grid center position
     do ind=1,twotondim  
        iz=(ind-1)/4
        iy=(ind-1-4*iz)/2
        ix=(ind-1-2*iy-4*iz)
        xc(ind,1)=(dble(ix)-0.5D0)*dx
        xc(ind,2)=(dble(iy)-0.5D0)*dx
        xc(ind,3)=(dble(iz)-0.5D0)*dx
     end do

     ! Loop over grids
     ncache=active(ilevel)%ngrid
     do igrid=1,ncache,nvector
        ngrid=MIN(nvector,ncache-igrid+1)
        do i=1,ngrid
           ind_grid(i)=active(ilevel)%igrid(igrid+i-1)
        end do

        ! Loop over cells
        do ind=1,twotondim  
           iskip=ncoarse+(ind-1)*ngridmax
           do i=1,ngrid
              ind_cell(i)=iskip+ind_grid(i)
           end do

           ! Flag leaf cells
           do i=1,ngrid
              ok(i)=son(ind_cell(i))==0
           end do

           do i=1,ngrid
              if(ok(i))then
                 ! Get gas cell position
                 x=(xg(ind_grid(i),1)+xc(ind,1)-skip_loc(1))*scale
                 y=(xg(ind_grid(i),2)+xc(ind,2)-skip_loc(2))*scale
                 z=(xg(ind_grid(i),3)+xc(ind,3)-skip_loc(3))*scale
                 do iSN=1,nSN
                    ! Check if the cell lies within the SN radius
                    dxx=x-xSN(iSN,1)
                    dyy=y-xSN(iSN,2)
                    dzz=z-xSN(iSN,3)
                    dr_SN=dxx**2+dyy**2+dzz**2
                    dr_cell=MAX(ABS(dxx),ABS(dyy),ABS(dzz))
                    if(dr_SN.lt.rmax2)then
                       vol_gas(iSN)=vol_gas(iSN)+vol_loc
                       ! Take account for grid effects on the conservation of the
                       ! normalized linear momentum
                       u=dxx/rmax
                       v=dyy/rmax
                       w=dzz/rmax
                       ! Add the local normalized linear momentum to the total linear
                       ! momentum of the blast wave (should be zero with no grid effect)
                       dq(iSN,1)=dq(iSN,1)+u*vol_loc
                       dq(iSN,2)=dq(iSN,2)+v*vol_loc
                       dq(iSN,3)=dq(iSN,3)+w*vol_loc
                       u2Blast(iSN,1)=u2Blast(iSN,1)+u*u*vol_loc
                       u2Blast(iSN,2)=u2Blast(iSN,2)+v*v*vol_loc
                       u2Blast(iSN,3)=u2Blast(iSN,3)+w*w*vol_loc
                    endif
                    if(dr_cell.le.dx_loc/2.0)then
                       ind_blast(iSN)=ind_cell(i)
                       ekBlast  (iSN)=vol_loc
                    endif
                 end do
              endif
           end do
           
        end do
        ! End loop over cells
     end do
     ! End loop over grids
  end do
  ! End loop over levels



#ifndef WITHOUTMPI
  call MPI_ALLREDUCE(vol_gas,vol_gas_all,nSN  ,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
  call MPI_ALLREDUCE(dq     ,dq_all     ,nSN*3,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
  call MPI_ALLREDUCE(u2Blast,u2Blast_all,nSN*3,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
  call MPI_ALLREDUCE(ekBlast,ekBlast_all,nSN  ,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
  vol_gas=vol_gas_all
  dq     =dq_all
  u2Blast=u2Blast_all
  ekBlast=ekBlast_all
#endif
  do iSN=1,nSN
     if(vol_gas(iSN)>0d0)then
        dq(iSN,1)=dq(iSN,1)/vol_gas(iSN)
        dq(iSN,2)=dq(iSN,2)/vol_gas(iSN)
        dq(iSN,3)=dq(iSN,3)/vol_gas(iSN)
        u2Blast(iSN,1)=u2Blast(iSN,1)/vol_gas(iSN)
        u2Blast(iSN,2)=u2Blast(iSN,2)/vol_gas(iSN)
        u2Blast(iSN,3)=u2Blast(iSN,3)/vol_gas(iSN)
        u2=u2Blast(iSN,1)-dq(iSN,1)**2
        v2=u2Blast(iSN,2)-dq(iSN,2)**2
        w2=u2Blast(iSN,3)-dq(iSN,3)**2
        ekBlast(iSN)=max(0.5d0*(u2+v2+w2),0.0d0)
     endif
  end do

  if(verbose)write(*,*)'Exiting average_SN'

end subroutine average_SN
!################################################################
!################################################################
!################################################################
!################################################################
subroutine Sedov_blast(xSN,vSN,mSN,sSN,ZSN,indSN,vol_gas,dq,ekBlast,nSN)
  use pm_commons
  use amr_commons
  use hydro_commons
  implicit none
#ifndef WITHOUTMPI
  include 'mpif.h'
#endif
  !------------------------------------------------------------------------
  ! This routine merges SN using the FOF algorithm.
  !------------------------------------------------------------------------
  integer::ilevel,j,iSN,nSN,ind,ix,iy,iz,ngrid,iskip
  integer::i,nx_loc,igrid,info,ncache
  integer,dimension(1:nvector),save::ind_grid,ind_cell
  real(dp)::x,y,z,dx,dxx,dyy,dzz,dr_SN,d,u,v,w,ek,u_r,ESN
  real(dp)::scale,dx_min,dx_loc,vol_loc,rmax2,rmax
  real(dp)::scale_nH,scale_T2,scale_l,scale_d,scale_t,scale_v
  real(dp),dimension(1:3)::skip_loc
  real(dp),dimension(1:twotondim,1:3)::xc
  real(dp),dimension(1:nSN)::mSN,sSN,ZSN,m_gas,p_gas,d_gas,d_metal,vol_gas,uSedov,ekBlast
  real(dp),dimension(1:nSN,1:3)::xSN,vSN,u_gas,dq
  integer ,dimension(1:nSN)::indSN
  logical ,dimension(1:nvector),save::ok

  if(nSN==0)return
  if(verbose)write(*,*)'Entering Sedov_blast'

  ! Mesh spacing in that level
  nx_loc=(icoarse_max-icoarse_min+1)
  skip_loc=(/0.0d0,0.0d0,0.0d0/)
  skip_loc(1)=dble(icoarse_min)
  skip_loc(2)=dble(jcoarse_min)
  skip_loc(3)=dble(kcoarse_min)
  scale=boxlen/dble(nx_loc)
  dx_min=scale*0.5D0**nlevelmax

  ! Conversion factor from user units to cgs units
  call units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)

  ! Maximum radius of the ejecta
  rmax=MAX(2.0d0*dx_min*scale_l/aexp,rbubble*3.08d18)
  rmax=rmax/scale_l
  rmax2=rmax*rmax
  
  ! Supernova specific energy from cgs to code units
  ESN=(1d51/(10d0*2d33*(10.**.4)))/scale_v**2

  do iSN=1,nSN
     if(vol_gas(iSN)>0d0)then
        d_gas(iSN)=mSN(iSN)/vol_gas(iSN)
        if(metal)d_metal(iSN)=ZSN(iSN)*mSN(iSN)/vol_gas(iSN)
        if(ekBlast(iSN)==0d0)then
           p_gas(iSN)=eta_sn*sSN(iSN)*ESN/vol_gas(iSN)
           uSedov(iSN)=0d0
        else
           p_gas(iSN)=(1d0-f_ek)*eta_sn*sSN(iSN)*ESN/vol_gas(iSN)
           uSedov(iSN)=sqrt(f_ek*eta_sn*sSN(iSN)*ESN/mSN(iSN)/ekBlast(iSN))
        endif
     else
        d_gas(iSN)=mSN(iSN)/ekBlast(iSN)
        p_gas(iSN)=eta_sn*sSN(iSN)*ESN/ekBlast(iSN)
        if(metal)d_metal(iSN)=ZSN(iSN)*mSN(iSN)/ekBlast(iSN)
     endif
  end do

  ! Loop over levels
  do ilevel=levelmin,nlevelmax
     ! Computing local volume (important for averaging hydro quantities) 
     dx=0.5D0**ilevel 
     dx_loc=dx*scale
     vol_loc=dx_loc**ndim
     ! Cells center position relative to grid center position
     do ind=1,twotondim  
        iz=(ind-1)/4
        iy=(ind-1-4*iz)/2
        ix=(ind-1-2*iy-4*iz)
        xc(ind,1)=(dble(ix)-0.5D0)*dx
        xc(ind,2)=(dble(iy)-0.5D0)*dx
        xc(ind,3)=(dble(iz)-0.5D0)*dx
     end do

     ! Loop over grids
     ncache=active(ilevel)%ngrid
     do igrid=1,ncache,nvector
        ngrid=MIN(nvector,ncache-igrid+1)
        do i=1,ngrid
           ind_grid(i)=active(ilevel)%igrid(igrid+i-1)
        end do

        ! Loop over cells
        do ind=1,twotondim  
           iskip=ncoarse+(ind-1)*ngridmax
           do i=1,ngrid
              ind_cell(i)=iskip+ind_grid(i)
           end do

           ! Flag leaf cells
           do i=1,ngrid
              ok(i)=son(ind_cell(i))==0
           end do

           do i=1,ngrid
              if(ok(i))then
                 ! Get gas cell position
                 x=(xg(ind_grid(i),1)+xc(ind,1)-skip_loc(1))*scale
                 y=(xg(ind_grid(i),2)+xc(ind,2)-skip_loc(2))*scale
                 z=(xg(ind_grid(i),3)+xc(ind,3)-skip_loc(3))*scale
                 do iSN=1,nSN
                    ! Check if the cell lies within the SN radius
                    dxx=x-xSN(iSN,1)
                    dyy=y-xSN(iSN,2)
                    dzz=z-xSN(iSN,3)
                    dr_SN=dxx**2+dyy**2+dzz**2
                    if(dr_SN.lt.rmax2)then

                       print*, 'Kinetic feedback... hydro variables are updated'
                       ! Compute the mass density in the cell
                       uold(ind_cell(i),1)=uold(ind_cell(i),1)+d_gas(iSN)
                       ! Compute the metal density in the cell
                       if(metal)uold(ind_cell(i),imetal)=uold(ind_cell(i),imetal)+d_metal(iSN)
                       ! Velocity at a given dr_SN linearly interpolated between zero and uSedov
                       u=uSedov(iSN)*(dxx/rmax-dq(iSN,1))+vSN(iSN,1)
                       v=uSedov(iSN)*(dyy/rmax-dq(iSN,2))+vSN(iSN,2)
                       w=uSedov(iSN)*(dzz/rmax-dq(iSN,3))+vSN(iSN,3)
                       ! Add each momentum component of the blast wave to the gas
                       uold(ind_cell(i),2)=uold(ind_cell(i),2)+d_gas(iSN)*u
                       uold(ind_cell(i),3)=uold(ind_cell(i),3)+d_gas(iSN)*v
                       uold(ind_cell(i),4)=uold(ind_cell(i),4)+d_gas(iSN)*w
                       ! Finally update the total energy of the gas
                       uold(ind_cell(i),5)=uold(ind_cell(i),5)+0.5*d_gas(iSN)*(u*u+v*v+w*w)+p_gas(iSN)
print*, 'total energy of gas (kinetic fb) : ', uold(ind_cell(i),5)
print*, 'mass density in the cell (kinetic fb) : ',  uold(ind_cell(i),1)


                    endif
                 end do
              endif
           end do
           
        end do
        ! End loop over cells
     end do
     ! End loop over grids
  end do
  ! End loop over levels

  do iSN=1,nSN
     if(vol_gas(iSN)==0d0)then
        u=vSN(iSN,1)
        v=vSN(iSN,2)
        w=vSN(iSN,3)
        if(indSN(iSN)>0)then
           uold(indSN(iSN),1)=uold(indSN(iSN),1)+d_gas(iSN)
           uold(indSN(iSN),2)=uold(indSN(iSN),2)+d_gas(iSN)*u
           uold(indSN(iSN),3)=uold(indSN(iSN),3)+d_gas(iSN)*v
           uold(indSN(iSN),4)=uold(indSN(iSN),4)+d_gas(iSN)*w
           uold(indSN(iSN),5)=uold(indSN(iSN),5)+d_gas(iSN)*0.5*(u*u+v*v+w*w)+p_gas(iSN)
           if(metal)uold(indSN(iSN),imetal)=uold(indSN(iSN),imetal)+d_metal(iSN)
        endif
     endif
  end do

print*, 'ESN (kinetic) : ', ESN
!print*, 'mejecta (kinetic) : ', mejecta

  if(verbose)write(*,*)'Exiting Sedov_blast'

end subroutine Sedov_blast
!###########################################################
!###########################################################
!###########################################################
!###########################################################

!!! FlorentR - PATCH FBSS

!###########################################################
!###########################################################
!###########################################################
recursive subroutine ionizing_sources(ilevel, icount)
  use pm_commons
  use amr_commons
  use hydro_commons
  implicit none
#ifndef WITHOUTMPI
  include 'mpif.h'
#endif
  integer::ilevel
  !------------------------------------------------------------------------
  ! This routine computes a list of ionizing sources and the size of
  ! the corresponding HII regions. This list is then passed to
  ! cooling_fine which determines the affected cells and sets the hydro 
  ! variable accordingly.
  !
  ! IS = ionizing source = young stellar particle that creates an
  !   HII region around itself.
  !
  ! Florent Renaud - May 2011
  !------------------------------------------------------------------------
  real(dp)::scale_nH, scale_T2, scale_l, scale_d, scale_t, scale_v
  real(dp)::t0, stromgren_time_factor, dx_loc, dx, mydx, scale, dxcell, ri, rj
  integer::ix, iy, iz, icount
  real(dp),dimension(1:twotondim,1:3)::xc
  integer::ilev, ilev2, igrid, jgrid, ipart, jpart, next_part, idim, ind, icpu, info
  integer::npart1, nperlev, noffset, old_offsetfiner, offsetcoarser
  integer::nis, iHII, jHII, myigrid, dist2mini
  real(dp)::HII_dr2, HII_r2, overlap, dist2min, dist2tocell, rho2
  real(dp),save::tis, t1, t2, L0, radiative0, rhominHII2
  logical,save::init_is=.false.
  integer::idinner, ninner
  real::xrandp

  if(numbtot(1,ilevel)==0)return
  if(verbose)write(*,112)ilevel  

  if(icount==2)return ! suggested by Romain to avoid update troubles in the amr_step resursive loop
 
  !------------------------------------------------------
  ! Initialisation (at startup and restart)
  !------------------------------------------------------ 

  ! eta_sn and THII set in the namelist
  !   eta_sn = 0               ==> no feedback
  !   eta_sn > 0   THII = 0    ==> only SN feedback
  !   eta_sn > 0   THII < 0    ==> photo-ionisation, radiative pressure (using -1*THII) and no SN feedback
  !   eta_sn > 0   THII > 0    ==> photo-ionisation, radiative pressure and SN feedback
  
  
  if(.not. init_is) then
    ! reset counters
    if(myid==1)then
      nHII_perlev=0
    endif
    
    ! Conversion factor from user units to cgs units
    call units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)

!!! PHYSICAL PARAMETERS HERE:
    rhominHII2 = (rhominHII/scale_nH)**2
! L = 3.15D13   = 6.3e46 [photons.s^(-1).Msun^(-1)] = number of ionizing photons per Msun
! R = 2.05D-10  = 2.0e-16 [m^3.s^(-1).K^(3/4)] = recombination rate
    L0=4.6968D-9*(eta_sn/scale_d)**(1./3.)*abs(THII)**(0.25) ! constant initial stromgren factor. Must be multiplied by (time dependence of the luminosity)**(1/3)
!      = ( 3.0D0/4.0D0/dacos(-1.0D0)*(1.66D-24)**2*L/R )**(1./3.) * (eta_sn / scale_d)**(1./3.) * abs(THII)**(0.25D0)
    radiative0=1.40299D-8*eta_sn*scale_t**2/scale_l*multiscat
!!!!!!!!      = L * eta_sn * h * nu_LymannAlpha / c
!!! PHYSICAL PARAMETER HERE: max age of ionizing sources (= 10 Myr)
    tis = 10. * 3.15576D13 / scale_t
!!! PHYSICAL PARAMETER HERE: timelapse before igniting luminosity (= Tff at the density given in namelist)
    t1 = (rhodelayHII/scale_nH)**(-0.5) * 0.542700941 ! = sqrt(3pi/32)
!!! PHYSICAL PARAMETER HERE: timelapse before decreasing luminosity as 1/t (= 4 Myr)
    t2 = 4. * 3.15576D13 / scale_t ! 4 Myr

    if(t1 > tis) write(*,*) 'Warning: rhodelayHII is too small!'
    
    nHII=0
    
    ! recursive call at all levels
    if(ilevel>levelmin) call ionizing_sources(ilevel-1,icount)

    init_is=.true.
    if(t==0) return ! recursif call at startup ends here
  endif

  
  !------------------------------------------------------
  ! Compute useful quantities at current level and time
  !------------------------------------------------------ 

  t0 = t - tis ! Earliest time of creation of ionizing stellar particles = now - tis
  ilev=ilevel-levelmin+1
  nis=0

  ! Set position of cell centers relative to grid center
  dx=0.5D0**ilevel 
  scale=boxlen/dble(icoarse_max-icoarse_min+1)
  dx_loc=dx*scale ! size of the cell at current level
  
  do ind=1,twotondim
    iz=(ind-1)/4
    iy=(ind-1-4*iz)/2
    ix=(ind-1-2*iy-4*iz)
    xc(ind,1)=(dble(ix)-0.5D0)
    xc(ind,2)=(dble(iy)-0.5D0)
    xc(ind,3)=(dble(iz)-0.5D0)
  end do

  !------------------------------------------------------
  ! Local list the ionizing sources
  !------------------------------------------------------  

  igrid=headl(myid,ilevel)
  ! Loop over grids
  do jgrid=1,numbl(myid,ilevel)
    npart1=numbp(igrid)
    if(npart1>0)then
      ipart=headp(igrid)

      ninner = 0 ! the first bubble smaller than its grid has not been detected yet

      ! Loop over particles
      do jpart=1,npart1
        next_part=nextp(ipart)
!!!JMG -- Frederic's change        if(idp(ipart).gt.0 .and. tp(ipart).ge.t0 .and. tp(ipart).gt.0 .and. t-tp(ipart).ge.t1)then
!!! Only 1 out of every nHII_skip sources will actually radiat!
        if(idp(ipart).gt.0 .and. tp(ipart).ge.t0 .and. tp(ipart).gt.0 .and. t-tp(ipart).ge.t1 .and. mod(ipart,nHII_skip).eq.0)then
          if(levelp(ipart).NE.ilevel) goto 222 ! next part
          nis=nis+1

          ! Luminosity function: constant between t1 and t2 (~4 Myr) and decreasing as 1/t after.
          stromgren_time_factor = 0.0D0
          if (t-tp(ipart).le.t2)then
            stromgren_time_factor = 1.0D0
          else
            stromgren_time_factor = t2/(t-tp(ipart))
          endif

          ! save the position of the ionizing source
          do idim=1,ndim
            is_xr(nis,idim) = xp(ipart, idim)
          end do
          
          ! compute the radius of the HII region iteratively
          ilev2=ilevel
          myigrid=igrid
          dxcell=dx_loc/2.0D0 ! half size of the cell
          mydx=dx
          is_xr(nis,4)=boxlen ! arbitrary high value for initialization
          ! Loop over levels: we go coarser in the AMR tree
          do while(ilev2.ge.levelmin)
            ! find which of the 2^ndim cell center of the grid is closer to the particle (the particle can be outside of all cells if move_fine has moved it into another grid)
            ! Loop over the cell of the current grid
            do ind=1,twotondim
              dist2tocell=((xg(myigrid,1)+xc(ind,1)*mydx-dble(icoarse_min))*scale-is_xr(nis,1))**2 + & ! squared distance to cell center
                             & ((xg(myigrid,2)+xc(ind,2)*mydx-dble(jcoarse_min))*scale-is_xr(nis,2))**2 + &
                             & ((xg(myigrid,3)+xc(ind,3)*mydx-dble(kcoarse_min))*scale-is_xr(nis,3))**2

              if(dist2tocell<dxcell**2)then ! particle inside the current cell
                dist2mini=ind
                goto 444 ! exit the loop
              endif
              ! find the closest cell
              if(ind==1)then
                dist2min=dist2tocell
                dist2mini=1
              else
                if(dist2tocell<dist2min)then
                  dist2min=dist2tocell
                  dist2mini=ind
                endif
              endif
            end do
            ! End loop over cells
            444 continue

            ! compute the radius using the local density at this level, or the limit chosen in the namelist
            rho2 = uold(myigrid+ncoarse+(dist2mini-1)*ngridmax,1)**2
            
            is_xr(nis,4) = L0 * (stromgren_time_factor * mp(ipart)/max(rho2, rhominHII2))**(1./3.) ! dist2mini is known from the twotondim loop

!!!JMG--Frederic's change -- only 1 out of every nHII_skip sources actually radiate, so we have to increase the luminosity per radiating source
            is_xr(nis,4) = is_xr(nis,4)  * (REAL(nHII_skip,dp))

            ! if the bubble fits in a cell, keep current value of the radius
            if(is_xr(nis,4)<dxcell) goto 333 ! exit the loop
            ! esle, go to upper level
            ilev2=ilev2-1
            ! find the father grid (i.e. the grid of the father cell) of the current grid
            myigrid=father(myigrid)-ncoarse-ngridmax*((father(myigrid)-ncoarse-1)/ngridmax) ! warning: the last term is an integer
            dxcell=dxcell*2.0D0 ! half-size of the cell at coarser level
            mydx=mydx*2.0D0
          end do
          ! End loop over levels
          333 continue

          call random_number(xrandp)
          is_xr(nis,4)=min(.160*(1.+1.*xrandp) , is_xr(nis,4) ) ! max radius 150-300pc - avoid huge sources from outside the disk

!!!!!!!!          print *,is_xr(nis,4)

          
          ! compute the specific momentum, corrected for the limit in density, so that the ionized mass is conserved.
          is_xr(nis,5) = radiative0 * stromgren_time_factor * mp(ipart) * rho2/max(rho2, rhominHII2) * real(nHII_skip)
          
          ! Treat inner-grid bubbles: merge all the bubbles smaller than the current grid into one bigger bubble
          if(ilev2==ilevel) then
            if(ninner==0)then
              ninner = 1
              idinner = nis ! save the id of the inner bubble that will contain the merged ones.
              is_xr(idinner,4) = is_xr(nis,4)**3
            else
              ninner = ninner + 1
              is_xr(idinner,1) = is_xr(idinner,1) + is_xr(nis,1) ! barycenter
              is_xr(idinner,2) = is_xr(idinner,2) + is_xr(nis,2)
              is_xr(idinner,3) = is_xr(idinner,3) + is_xr(nis,3)
              is_xr(idinner,4) = is_xr(idinner,4) + is_xr(nis,4)**3 ! add the volumes first (the radius will be computed at the end of the treatment for this grid)
              is_xr(idinner,5) = is_xr(idinner,5) + is_xr(nis,5) ! add the pressure terms
              nis = nis-1 ! delete the inner bubble (only if it is not the first one in the grid)
            endif
          endif
          
        endif
        222 continue
        ipart=next_part
      end do
      ! End loop over particles
    
      ! compute the inner bubble
      if(ninner>0) then
        is_xr(idinner,1) = is_xr(idinner,1) / ninner
        is_xr(idinner,2) = is_xr(idinner,2) / ninner
        is_xr(idinner,3) = is_xr(idinner,3) / ninner
        is_xr(idinner,4) = is_xr(idinner,4)**(1./3.)
      endif
      
    endif      
    igrid=next(igrid)
  end do
  ! End loop over grids
  
  !------------------------------------------------------
  ! Manage the IS globaly at current level
  !------------------------------------------------------
#ifndef WITHOUTMPI
  if(myid==1)then
    
    old_offsetfiner=sum(nHII_perlev(1:ilev))
    offsetcoarser=old_offsetfiner-nHII_perlev(ilev) ! remains the same until exiting the subroutine

    ! get the number of ISs per CPU at the current level
    nis_percpu=0 ! reset

    nis_percpu(1)=nis
    do icpu=2,ncpu
      call MPI_RECV(nis_percpu(icpu),1,MPI_INTEGER,icpu-1,100,MPI_COMM_WORLD,MPI_STATUS_IGNORE,info) ! receive local nb of ISs from all CPUs
    end do
    nHII_perlev(ilev)=sum(nis_percpu)
#else
    nHII_perlev(ilev)=nis
#endif

    ! shift pre-existing data from finer levels:
    !   coarser levels: do not move
    !   current level: create a gap of nHII_perlev(ilev) rows
    !   finer levels: shift after the gap

    noffset=offsetcoarser
    ! create the gap
    if(ilevel.NE.nlevelmax) HII_xr(noffset+nHII_perlev(ilev)+1:sum(nHII_perlev(ilev+1:nlevelmax-levelmin+1)),:)=HII_xr(old_offsetfiner+1:nHII,:)

    nHII=sum(nHII_perlev) ! total number of ISs for all levels (do not update earlier!)

    ! copy the data from root cpu into the gap
    HII_xr(noffset+1:noffset+nis_percpu(1),:)=is_xr(1:nis_percpu(1),:)
    HII_xr(noffset+1:noffset+nis_percpu(1),6) = 1.0 ! reset the bubble scaling-factor

#ifndef WITHOUTMPI
    noffset=noffset+nis_percpu(1)

    ! concat the local IS arrays into the global array
    do icpu=2,ncpu
      if(nis_percpu(icpu).gt.0) then
        call MPI_RECV(HII_xr(noffset+1:noffset+nis_percpu(icpu),1:5),nis_percpu(icpu)*5,MPI_DOUBLE_PRECISION,icpu-1,101,MPI_COMM_WORLD,MPI_STATUS_IGNORE,info) ! Receive the data of ISs from all CPUs
        HII_xr(noffset+1:noffset+nis_percpu(icpu),6) = 1.0 ! reset the bubble scaling-factor
    noffset=noffset+nis_percpu(icpu)
      endif
    end do ! end loop over CPUs

  else
    call MPI_SEND(nis,1,MPI_INTEGER,0,100,MPI_COMM_WORLD,info) ! send local nb of ISs to root CPU
    
    if(nis>0)then
      call MPI_SEND(is_xr(1:nis,1:5),nis*5,MPI_DOUBLE_PRECISION,0,101,MPI_COMM_WORLD,info) ! send data of local ISs to root CPU
    endif

  endif

  if(myid==1)then
#endif

    !------------------------------------------------------
    ! Create complete catalogue of the effective HII regions
    !------------------------------------------------------  

    ! Manage the overlap of HII bubbles
    
    if(nHII.GT.1)then
      ! Loop over ISs (except last one)
      do iHII=1, nHII-1
        ri = HII_xr(iHII,4)*HII_xr(iHII,6) ! intrinsic radius * scaling factor
        ! Loop over other ISs
        do jHII=iHII+1, nHII
          rj = HII_xr(jHII,4)*HII_xr(jHII,6)
          if(ri.GT.0 .AND. rj.GT.0) then
            ! check weither d12 < R1 + R2
            HII_r2=(ri+rj)**2 ! sum of the radii
            HII_dr2=(HII_xr(iHII,1)-HII_xr(jHII,1))**2 ! distance between the ISs projected on x-axis squared
            if(HII_dr2<HII_r2)then
              HII_dr2=HII_dr2+(HII_xr(iHII,2)-HII_xr(jHII,2))**2
              if(HII_dr2<HII_r2)then
                HII_dr2=HII_dr2+(HII_xr(iHII,3)-HII_xr(jHII,3))**2 ! distance between the ISs projected on z-axis squared
                if(HII_dr2<HII_r2)then ! overlap
                  if(HII_dr2<(ri-rj)**2)then ! one bubble is completely inside the other
                    if(ri>rj)then ! i-th bubble is bigger
if(0==0)then ! NO REMOVAL
                      ! new radius = (HII_xr(iHII,4)**3 + HII_xr(jHII,4)**3 * HII_xr(iHII,4)/(HII_xr(iHII,4)+HII_xr(jHII,4)) )**(1./3.)
                      HII_xr(iHII,6) = (1.0 + rj**3 / ((ri+rj)*ri**2) )**(1./3.)
                      ! new radius = HII_xr(jHII,4) * ( HII_xr(jHII,4)/(HII_xr(iHII,4)+HII_xr(jHII,4)) )**(1./3.)
                      HII_xr(jHII,6) = ( rj/(ri+rj) )**(1./3.)
else ! REMOVAL
                      ! new radius = (HII_xr(iHII,4)**3 + HII_xr(jHII,4)**3)**(1./3.)
                      HII_xr(iHII,6) = (1.0 + ( rj / ri )**3)**(1./3.) 
                      ! new radius = 0
                      HII_xr(jHII,6) = 0.0
endif ! endif removal
                    else ! j-th bubble is bigger
if(0==0)then ! NO REMOVAL       
                      HII_xr(iHII,6) = ( ri/(ri+rj) )**(1./3.)
                      HII_xr(jHII,6) = (1.0 + ri**3 / ((ri+rj)*rj**2) )**(1./3.)
else ! REMOVAL
                      HII_xr(iHII,6) = 0.0
                      HII_xr(jHII,6) = (1.0 + ( ri / rj )**3)**(1./3.) 
endif ! endif REMOVAL
                    endif
                  else                
                    ! r1_new**3 = r1_old**3 + V_intersection * (3/4/pi) * r1_old/(r1_old+r2_old)   with V_intersection = pi/(12*d) * (r1 + r2 - d)**2 * (d**2 + 2*d*r1 + 2*d*r2 - 3*r1*2 - 3*r2**2 + 6*r1*r2)
                    ! overlap = V_intersection * (3/4/pi) * 1/(r1_old+r2_old)
                    HII_dr2 = sqrt(HII_dr2) ! distance between the centers
                    HII_r2= sqrt(HII_r2) ! sum of the radii
                    overlap = 6.25D-2*(HII_r2-HII_dr2)**2*(HII_dr2/HII_r2 + 2.0D0 - 3.0D0*(ri-rj)**2/HII_dr2/HII_r2)
                    ! new radius (HII_xr(iHII,4)**3 + HII_xr(iHII,4)*overlap)**(1./3.)
                    HII_xr(iHII,6) = (1.0 + overlap/ri**2)**(1./3.)
                    ! new radius (HII_xr(jHII,4)**3 + HII_xr(jHII,4)*overlap)**(1./3.)
                    HII_xr(jHII,6) = (1.0 + overlap/rj**2)**(1./3.)
                  endif
                  ri = HII_xr(iHII,4)*HII_xr(iHII,6)
                endif
              endif
            endif
          endif
        end do
        ! End loop over other ISs      
      end do
      ! End loop over ISs
    endif


if(0==0)then ! NO REMOVAL
    ! if the HII region is smaller than the cell which contains it, tag the HII region by setting a negative radius AND a negative scaling factor (so that the effective radius (used in overlap) > 0)
    ilev2=1
    nperlev = nHII_perlev(ilev2)
    dxcell=0.5D0**levelmin*scale/2.0D0 ! half-size of the cell at levelmin
    do iHII=1, nHII
      do while(iHII>nperlev) ! reaching finer level
        ilev2=ilev2+1
        nperlev = nperlev + nHII_perlev(ilev2)
        dxcell=dxcell/2.0D0
      end do
      if(HII_xr(iHII,4)*HII_xr(iHII,6).LT.dxcell) then
        HII_xr(iHII,4)=-1.0D0*ABS(HII_xr(iHII,4))
        HII_xr(iHII,6)=-1.0D0*ABS(HII_xr(iHII,6))
      endif
    end do
else ! REMOVAL
    ! if the HII region is smaller than the cell which contains it, tag the HII region by setting a negative radius AND a negative scaling factor (so that the effective radius (used in overlap) > 0)
    ilev2=1
    nperlev = nHII_perlev(ilev2)
    dxcell=0.5D0**levelmin*scale/2.0D0 ! half-size of the cell at levelmin
    nis = nHII ! temp save
    nHII = 0
    do iHII=1, nis
      do while(iHII>nperlev) ! reaching finer level
        ilev2=ilev2+1
        nperlev = nperlev + nHII_perlev(ilev2)
        dxcell=dxcell/2.0D0
      end do
      if(HII_xr(iHII,4)*HII_xr(iHII,6).GT.0)then ! the bubble exists and should be kept
        nHII = nHII + 1
        HII_xr(nHII,1:6) = HII_xr(iHII,1:6)
        if(HII_xr(nHII,4)*HII_xr(iHII,6).LT.dxcell) then
          HII_xr(nHII,4)=-1.0D0*ABS(HII_xr(nHII,4))
          HII_xr(nHII,6)=-1.0D0*ABS(HII_xr(nHII,6))
        endif
      else
        nHII_perlev(ilev2) = nHII_perlev(ilev2) - 1 ! remove the bubble
      endif
    end do
endif ! endif REMOVAL


#ifndef WITHOUTMPI
  endif

  call MPI_BCAST(nHII,1,MPI_INTEGER,0,MPI_COMM_WORLD,info) ! broadcast the final number of HII regions
  
  if(nHII.GT.0) call MPI_BCAST(HII_xr(1:nHII,1:6),nHII*6,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,info) ! Broadcast the final data array of the HII regions to all CPUs

  if(myid.EQ.1)write(*,'("ISTime=",1pe12.5," nHII=",I6)')t,nHII

#endif
  ! The total number of HII regions (nHII), and their position, size, and radiative pressure (HII_xr), at time t, are known by all CPUs. Ready for treatment in coolfine1.

112 format('   Entering ionizing_sources for level ',i2)

end subroutine ionizing_sources

!###########################################################
!###########################################################
!###########################################################
!###########################################################

subroutine radiative_pressure
  use amr_commons
  use pm_commons
  use hydro_commons
  implicit none
#ifndef WITHOUTMPI
  include 'mpif.h'
#endif
  integer::ilevel,ngrid,igrid,ncache, iHII, ivar
  integer::i,ind,iskip,idim,nleaf,nx_loc,ix,iy,iz
  real(dp)::scale_nH,scale_T2,scale_l,scale_d,scale_t,scale_v  
  real(dp)::dx, radiative, dt_loc, scale, dx_loc, dx2_loc_half
  real(dp)::HII_dr2,HII_r2,rhominHII2
  
  integer,dimension(1:nvector),save::ind_grid,ind_cell,ind_leaf
  logical,dimension(1:nvector),save::ok

  real(dp),dimension(1:ndim)::xx
  real(dp),dimension(1:twotondim,1:3)::xc
  real(dp),dimension(1:3)::skip_loc

  if(nHII == 0) return ! no HII region => exit

  ! Mesh spacing
  nx_loc=(icoarse_max-icoarse_min+1)
  skip_loc=(/0.0d0,0.0d0,0.0d0/)
  if(ndim>0)skip_loc(1)=dble(icoarse_min)
  if(ndim>1)skip_loc(2)=dble(jcoarse_min)
  if(ndim>2)skip_loc(3)=dble(kcoarse_min)
  scale=boxlen/dble(nx_loc)
  
  ! Conversion factor from user units to cgs units
  call units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)
  rhominHII2 = rhominHII/scale_nH
  dt_loc = dtnew(levelmin)

  ! Loop over levels
  do ilevel=levelmin,nlevelmax
    ! Mesh spacing *at level ilevel*
    dx=0.5D0**ilevel 
    dx_loc=dx*scale
    dx2_loc_half=(dx_loc**2)/4.0 ! half size of the cell, squared

    ! Set position of cell centers relative to grid center
    do ind=1,twotondim
     iz=(ind-1)/4
     iy=(ind-1-4*iz)/2
     ix=(ind-1-2*iy-4*iz)
     if(ndim>0)xc(ind,1)=(dble(ix)-0.5D0)*dx
     if(ndim>1)xc(ind,2)=(dble(iy)-0.5D0)*dx
     if(ndim>2)xc(ind,3)=(dble(iz)-0.5D0)*dx
    end do

    ! Loop over grids
    ncache=active(ilevel)%ngrid
    do igrid=1,ncache,nvector
      ngrid=MIN(nvector,ncache-igrid+1)
      do i=1,ngrid
        ind_grid(i)=active(ilevel)%igrid(igrid+i-1)
      end do

      ! Loop over cells
      do ind=1,twotondim  
        iskip=ncoarse+(ind-1)*ngridmax
        do i=1,ngrid
          ind_cell(i)=iskip+ind_grid(i)
        end do

        ! Flag leaf cells
        do i=1,ngrid
          ok(i)=(son(ind_cell(i))==0)
        end do

        nleaf=0
        do i=1,ngrid
          if(ok(i))then ! cell is a leaf
            ! Get gas cell position
            do idim=1,ndim
              xx(idim)=(xg(ind_grid(i),idim)+xc(ind,idim)-skip_loc(idim))*scale
            end do
            
! HII_xr(iHII,1) = center x
! HII_xr(iHII,2) = center y
! HII_xr(iHII,3) = center z
! HII_xr(iHII,4) = intrinsic radius
! HII_xr(iHII,5) = momentum rate = Luminosity(time) * Planck * frequency / lightspeed
! HII_xr(iHII,6) = overlap factor = effective_radius / intrinsic_radius

            ! Loop over ionizing sources
            do iHII=1, nHII   
!              if(uold(ind_cell(i),1)>rhominHII2 .AND. abs(HII_xr(iHII,6)) < 10 .AND. HII_xr(iHII,5)<5e-6 .AND. HII_xr(iHII,5)>1e-9)then ! safety limits
              if(uold(ind_cell(i),1)>rhominHII2 .AND. abs(HII_xr(iHII,6)) < 10 .AND. HII_xr(iHII,5)<1.0 .AND. HII_xr(iHII,5)>1e-9)then ! safety limits
                HII_r2 = ( HII_xr(iHII,4) * HII_xr(iHII,6) ) **2 ! radius of the bubble squared, corrected by the overlap factor

                if(HII_xr(iHII,4) < 0)then ! the bubble is not resolved: find the host cell and inject momentum in it
                  HII_dr2 = (xx(1)-HII_xr(iHII,1))**2 ! bubble-cell distance along x, squared
                  if(HII_dr2<dx2_loc_half)then ! if smaller than the half cell size (also squared), keep going
                    HII_dr2 = (xx(2)-HII_xr(iHII,2))**2 ! bubble-cell distance along y, squared
                    if(HII_dr2<dx2_loc_half)then ! if smaller than the half cell size (also squared), keep going
                      HII_dr2 = (xx(3)-HII_xr(iHII,3))**2 ! bubble-cell distance along z, squared
                      if(HII_dr2<dx2_loc_half)then ! if smaller than the half cell size (also squared), the bubble is in the cell

                        HII_dr2 = ( xx(1)-HII_xr(iHII,1) )**2 + ( xx(2)-HII_xr(iHII,2) )**2 + ( xx(3)-HII_xr(iHII,3) )**2 ! 3D distance squared
                        
          radiative = HII_xr(iHII,5) * dt_loc / dx_loc**3  / sqrt(HII_dr2) * 2**(nlevelmax-levelmin-1)  !!4 subcycle factor   !!real(nHII_skip)
                        ! = momentum_rate * dt / M_cell * rho_cell[because momentum in uold] / norm(radial distance to bubble's center [used for projection])
                        ! = momentum_rate * dt / V_cell / norm(radial distance to bubble's center [used for projection])
                        
!                        if(radiative * sqrt(HII_dr2) / uold(ind_cell(i),1) < 1.25/scale_v) then ! < 10 km/s / dt
                        radiative = min(0.225 * 5000. * dt_loc / sqrt(HII_dr2) * uold(ind_cell(i),1), radiative)  ! 5000 km/s during 1Myr (0.225 = (1 km/s) /(1 Myr) in ramses units)

                          uold(ind_cell(i),2) = uold(ind_cell(i),2) + ( xx(1)-HII_xr(iHII,1) ) * radiative
                          uold(ind_cell(i),3) = uold(ind_cell(i),3) + ( xx(2)-HII_xr(iHII,2) ) * radiative
                          uold(ind_cell(i),4) = uold(ind_cell(i),4) + ( xx(3)-HII_xr(iHII,3) ) * radiative
                          uold(ind_cell(i),5) = uold(ind_cell(i),5) + 0.5/uold(ind_cell(i),1) * HII_dr2 * radiative**2
                          ! 1/uold(i,1) because delta_uold(i,5) = 0.5* rho * delta_v**2    and    delta_uold(i,3) = rho * delta_v     thus,   uold(i,5) = 0.5* rho * delta_uold(i,3)**2 / rho**2
!                        endif
                    
                      endif
                    endif                
                  endif

                else ! the bubble is resolved: find the cells of which centers are in the bubble and inject their share of the momentum (volume-weighted)

                  ! check if the center of the current cell is inside the bubble created by the current source
                  HII_dr2 = (xx(1)-HII_xr(iHII,1))**2 ! bubble-cell 1D-distance, squared
                  if(HII_dr2<HII_r2)then
                    HII_dr2 = HII_dr2 + (xx(2)-HII_xr(iHII,2))**2 ! bubble-cell 2D-distance, squared
                    if(HII_dr2<HII_r2)then
                      HII_dr2 = HII_dr2 + (xx(3)-HII_xr(iHII,3))**2 ! bubble-cell 3D-distance, squared
                      if(HII_dr2<HII_r2)then ! the cell is in one HII region

     radiative = HII_xr(iHII,5) * dt_loc / (4.1887902 * HII_xr(iHII,4)**3 * HII_xr(iHII,6)**3)  / sqrt(HII_dr2) * 2**(nlevelmax-levelmin-1) !!4 subcycle factor !!!real(nHII_skip)
                        ! = momentum_rate * dt / M_cell * V_cell / V_bubble * rho_cell[because momentum in uold] / norm(radial distance to bubble's center [used for projection])
                        ! = momentum_rate * dt / V_bubble / norm(radial distance to bubble's center [used for projection])

!                        if(radiative * sqrt(HII_dr2) / uold(ind_cell(i),1) < 1.25/scale_v) then ! < 10 km/s / dt
        radiative = min(0.225 * 5000. * dt_loc / sqrt(HII_dr2) * uold(ind_cell(i),1), radiative)  ! 5000 km/s during 1Myr (0.225 = (1 km/s) /(1 Myr) in ramses units)
        radiative = max(0.225 * 1. * dt_loc / sqrt(HII_dr2) * uold(ind_cell(i),1), radiative)  ! 1 km/s during 1Myr (0.225 = (1 km/s) /(1 Myr) in ramses units)                



                        !!!!!radiative = 0.225 * 250. * dt_loc / sqrt(HII_dr2) * uold(ind_cell(i),1)
!               if(HII_xr(iHII,4)>.05) then
!                  print *,radiative * sqrt(HII_dr2) / uold(ind_cell(i),1),uold(ind_cell(i),2)/uold(ind_cell(i),1),dt_loc ,'deltaV;Vcell;dt_loc -- rHII',HII_xr(iHII,4)
!               endif

                          uold(ind_cell(i),2) = uold(ind_cell(i),2) + ( xx(1)-HII_xr(iHII,1) ) * radiative
                          uold(ind_cell(i),3) = uold(ind_cell(i),3) + ( xx(2)-HII_xr(iHII,2) ) * radiative
                          uold(ind_cell(i),4) = uold(ind_cell(i),4) + ( xx(3)-HII_xr(iHII,3) ) * radiative
                          uold(ind_cell(i),5) = uold(ind_cell(i),5) + 0.5/uold(ind_cell(i),1) * HII_dr2 * radiative**2
                          ! 1/uold(i,1) because delta_uold(i,5) = 0.5* rho * delta_v**2    and    delta_uold(i,3) = rho * delta_v     thus,   uold(i,5) = 0.5* rho * delta_uold(i,3)**2 / rho**2
!                        endif

                      endif
                    endif
                  endif

                endif ! end bubble resolved
              endif ! end safety limits
            end do ! End loop over ionizing sources    


          endif ! end leaf cell
        end do     
      end do ! End loop over cells
   end do ! End loop over grids
  end do ! End loop over levels

  ! Update hydro quantities for split cells
  do ilevel=nlevelmax,levelmin,-1
    call upload_fine(ilevel)
    do ivar=1,nvar
      call make_virtual_fine_dp(uold(1,ivar),ilevel)
    end do
  end do

end subroutine radiative_pressure
!################################################################
!################################################################
!################################################################
!################################################################

!!! FRenaud

